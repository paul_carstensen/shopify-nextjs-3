## Shopify NextJS

Server Side Rendered pages for [uptodate.de](https://www.uptdodate.de).
It's implemented using [NextJS framework](https://nextjs.org), [React](https://reactjs.org).
We use CloudFront as a reverse proxy to serve Shopify shop pages and pages from this application under the same domain.
Find more information about this [here](https://uptodate-ventures.atlassian.net/wiki/spaces/UM/pages/1407516673/React+pages+-+Technical+Documentation)

## Getting Started

Login to AWS CodeArtifact so that you can pull all npm packages:

```bash
./ca-config.sh
```

Set up a `config/local.json` file, by copying the content from `config/local.example.json`. You can change this configuration with configuration from `config/dev.json` or `config/prod.json` to test features with dev/prod resources.

Set up a `.env.local` file. You have to retrieve tokens from [AWS Systems Manager Parameter Store](https://eu-central-1.console.aws.amazon.com/systems-manager/parameters/shopify-nextjs-storefront-api-token/description?region=eu-central-1&tab=Table)

```
NEXT_TELEMETRY_DISABLED=1
SITE_URL=http://www.uptodate.dev
CONSENT_TRACKER_URL=https://api.uptodate.de/consent-tracker
STOREFRONT_API_TOKEN=<shopify-nextjs-storefront-api-token>
```

After installing all dependencies with `npm install` run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

## Special notes

Due to our [infrastructure setup](https://uptodate-ventures.atlassian.net/wiki/spaces/UM/pages/1628536916/CloudFront+as+reverse+proxy+for+Shopify+and+NextJS),
we are using Webpack to serve the images (by default NextJS serve them from root path `/` which is
not something we can do). Because of that, we shouldn't use `/public/` folder for images
but put them into `/assets/` folder **and** using `import` to use them
in the code. The images will be served under the `/_next/static` path and solve the issue with
paths. More details [here](https://uptodate-ventures.atlassian.net/wiki/spaces/UM/pages/1714520186/Serving+of+images+and+other+static+files+in+Next.js)

## Using Prettier

We use [Prettier](https://prettier.io/) for automated code formating.
Install the recommend Prettier extensions `esbenp.prettier-vscode` and enable `Editor: format on save` in VSCode settings.
Verify that prettier is the default formatter (shown in the VSCode status bar).
Ticking the `Prettier: Require Config` option in VSCode settings will enable code formatting only for projects that have a `.prettierrc` file.

If we change some Prettier settings we can easily format all files with `npm run prettier`.
