// @ts-check

// eslint-disable-next-line @typescript-eslint/no-var-requires
const config = require('./config');

/**
 * @type {import('next').NextConfig}
 * */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['partner-images.uptodate.de', 'cdn.shopify.com'],
  },
  typescript: {
    ignoreBuildErrors: true, // skip type checking because we call tsc inside the npm build script and check all files
  },
  compress: false, // because we offload compression to CloudFront
  async headers() {
    return [
      // Send the x-shopid response header used by Shopify to verify domains
      {
        source: '/:path*',
        locale: false,
        headers: [
          { key: 'X-ShopId', value: config().shop.shopId },
          { key: 'Cache-Control', value: 'public,max-age=3600' },
        ],
      },
    ];
  },
};

module.exports = nextConfig;
