import { ReactElement } from 'react';
import App, { AppContext } from 'next/app';
import Head from 'next/head';
import { ThemeProvider } from 'styled-components';
import { GlobalUserAgentStyleOverwrite, GlobalStyle } from '@u2dv/marketplace_ui_kit/dist/styles';
import Layout from '../components/Layout';
import getAbsoluteUrl from '../helpers/getAbsoluteUrl';
import { getNavigationMenus, NavigationMenusResult } from '../helpers/getNavigationMenus';
import ApiErrorsProvider from '../context/errors/apiErrorsProvider';
import config from '../config';
import faviconImage from '../assets/images/favicon.png';
import '../styles/fonts.css';
import '../styles/theme.shopify.css';

const theme = {
  colors: {
    primary: '#19232d',
  },
};

/* eslint-disable react/no-danger */
const getGoogleTagManagerScript = (containerId: string): ReactElement => (
  <script
    dangerouslySetInnerHTML={{
      __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
     new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
     j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
     'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
     })(window,document,'script','dataLayer','${containerId}');`,
    }}
  />
);

/* eslint-disable @next/next/no-sync-scripts */
const getUsercentricsScript = (settingsId: string): ReactElement => (
  <script
    src="https://app.usercentrics.eu/browser-ui/latest/loader.js"
    id="usercentrics-cmp"
    data-settings-id={settingsId}
    async
  />
);
/* eslint-enable @next/next/no-sync-scripts */

const shopifyThemeScripts = (
  <>
    {/* Note: the cart drawer has been disabled since it requires a Shopify-rendered cart
        template to work. Set "cartType: 'drawer'" in the settings below to re-enable. */}
    <script
      dangerouslySetInnerHTML={{
        __html: `document.documentElement.className = document.documentElement.className.replace('no-js', 'js');

       window.theme = window.theme || {};
       theme.strings = {
         addToCart: "In den Warenkorb legen",
         soldOut: "Ausverkauft",
         unavailable: "Nicht verfügbar",
         waitingForStock: "Inventar auf dem Weg",
         cartEmpty: "Dein Warenkorb ist im Moment leer.",
         cartTermsConfirmation: "Du musst den Verkaufsbedingungen zustimmen, um auszuchecken"
       };
       theme.settings = {
         dynamicVariantsEnable: true,
         dynamicVariantType: "button",
         cartType: "page",
         currenciesEnabled: false,
         nativeMultiCurrency: 1 > 1 ? true : false,
         moneyFormat: "€{{amount_with_comma_separator}}",
         saveType: "percent",
         recentlyViewedEnabled: false,
         predictiveSearch: true,
         predictiveSearchType: "product,article,page",
         inventoryThreshold: 10,
         quickView: false,
         themeName: 'Impulse',
         themeVersion: "2.5.2"
       };`,
      }}
    />

    {/* eslint-disable @next/next/no-sync-scripts */}
    <script src="https://cdn.shopify.com/s/files/1/0268/4819/8771/t/80/assets/vendor-scripts-v5.js" />
    <script src="https://cdn.shopify.com/s/files/1/0268/4819/8771/t/80/assets/theme.js?v=1318120240327998521" />
    {/* eslint-enable @next/next/no-sync-scripts */}
  </>
);
/* eslint-enable react/no-danger */

const title = 'Besser leben und wohnen';
const description =
  'Risiken zuhause minimieren mit uptodate.de ➤ Innovative Produkte, Apps & Services ✓nachhaltiger ✓gesünder ✓sichererer zuhause';
const socialMediaImageUrl = 'https://cdn.shopify.com/s/files/1/0268/4819/8771/files/checkout_logo.png';

export type CustomAppProps = {
  Component: React.FunctionComponent<Record<string, unknown>>;
  pageProps: Record<string, unknown>;
  pageUrl: string;
  navigationItems: NavigationMenusResult;
};

export const CustomApp = ({ Component, pageProps, pageUrl, navigationItems }: CustomAppProps) => (
  <>
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />

      <link rel="preconnect" href="https://cdn.shopify.com" />
      <link rel="shortcut icon" type="image/png" href={faviconImage.src} />

      <meta name="viewport" content="width=device-width,initial-scale=1" />
      <meta name="format-detection" content="telephone=no" />
      <meta name="theme-color" content="#19232d" />

      <meta property="og:site_name" content="uptodate" />
      <meta property="og:locale" content="de_DE" />
      <meta key="og:type" property="og:type" content="website" />
      <meta key="og:title" property="og:title" content={title} />
      <meta key="og:description" property="og:description" content={description} />
      {pageUrl && <meta key="og:url" property="og:url" content={pageUrl} />}
      <meta key="og:image" property="og:image" content={socialMediaImageUrl} />
      <meta key="twitter:card" name="twitter:card" content="summary_large_image" />

      <meta name="google-site-verification" content="i_D6TjMPGqwfcTB8JBrVwnE58_6gG26vQF2jIc-BatE" />
      <meta name="google-site-verification" content="X020T2nFuXBQiv6rUdM-rSB2K-YHti3MCuPDuMjqIxI" />
      <meta name="google-site-verification" content="IjyVS8_94YsbVFArQjnSiCd7kaboZn53y-rEo_v1GLM" />
      <meta name="facebook-domain-verification" content="2i5r21mkkze8ehxbu6uvpxar4n0xgz" />

      {getGoogleTagManagerScript(config().googleTagManager.containerId)}
      {getUsercentricsScript(config().usercentrics.settingsId)}
      {shopifyThemeScripts}
    </Head>

    <GlobalUserAgentStyleOverwrite />
    <GlobalStyle />

    <ThemeProvider theme={theme}>
      <ApiErrorsProvider>
        <Layout
          footerNavigationItems={navigationItems.contentFooter}
          mainNavigationItems={navigationItems.contentMain}
          sideNavigationItems={navigationItems.contentSide}
        >
          <Component {...pageProps} />
        </Layout>
      </ApiErrorsProvider>
    </ThemeProvider>
  </>
);

CustomApp.getInitialProps = async (appContext: AppContext) => {
  const appProps = await App.getInitialProps(appContext);

  const pageUrl = appContext.ctx.req ? getAbsoluteUrl(appContext.ctx.req).toString() : window.location.href;

  return {
    ...appProps,
    pageUrl,
    navigationItems: await getNavigationMenus(),
  };
};

export default CustomApp;
