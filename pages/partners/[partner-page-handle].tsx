import { NextPage, GetServerSideProps, GetServerSidePropsContext } from 'next';
import fetchPageBlocks from '../../helpers/partner_pages/fetchPageBlocks';
import buildComponentBlocks from '../../helpers/partner_pages/buildComponentBlocks';
import { BlockContent } from '../../types/partnerPages';
import generateContent from '../../helpers/partner_pages/generateContent';
import { firstValue } from '../../helpers/firstValue';
import commonStyles from '../../components/partner_pages/blocks/common.module.css';

// TODO: The mapping should be fetched from database. The current solution is temporary.
const pageMap: Record<string, number | undefined> = {
  '8fit': 50608734323,
  runtastic: 50761269363,
};

const renderWithBlocks = (blocks: BlockContent[]) => (
  <div className={`${commonStyles['main-container']} ${commonStyles['no-margin-top']}`}>{generateContent(blocks)}</div>
);

type PartnerPageProps = {
  componentBlocks: BlockContent[];
};

const PartnerPage: NextPage<PartnerPageProps> = ({ componentBlocks }) => renderWithBlocks(componentBlocks);

export const getServerSideProps: GetServerSideProps = async ({ params = {} }: GetServerSidePropsContext) => {
  const handle = firstValue<string | undefined>(params['partner-page-handle']);
  if (!handle) {
    return {
      notFound: true,
    };
  }

  const partnerPageId = pageMap[handle];
  if (!partnerPageId) {
    return {
      notFound: true,
    };
  }

  const blocks = await fetchPageBlocks(partnerPageId);
  if (blocks.length === 0) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      componentBlocks: buildComponentBlocks(blocks),
    },
  };
};

export default PartnerPage;
