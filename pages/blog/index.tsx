import React, { ReactElement } from 'react';
import { NextPage, GetServerSideProps } from 'next';
import Head from 'next/head';
import styled from 'styled-components';
import ErrorPage404 from '../404';
import { fetchMarketplaceGraphQL } from '../../helpers/fetchMarketplaceGraphQL';
import { BlogPostItem } from '../../components/Blogs/Blocks/BlogPostItem';
import { Blog, BlogPost } from '../../types/blogs';
import config from '../../config';
import { BlogPageWidthContainer } from '../../components/Blogs/BlogPageWidthContainer';

type BlogPageProps = {
  blog?: Blog;
};

const BlogTitle = styled.div`
  font-size: 32px;
  font-weight: 500;
  text-transform: uppercase;
  text-align: center;
  padding-bottom: 30px;
  @media screen and (max-width: 768px) {
    font-size: 24px;
    line-height: normal;
  }
`;

const BlogPostItemList = styled.div`
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  div.blog-post-item {
    width: 33.33%;
  }
  @media screen and (max-width: 768px) {
    flex-direction: column;
    div.blog-post-item {
      width: 100%;
    }
  }
`;

const renderBlogPost = ({
  blogPostId,
  title,
  slug,
  imageUrl,
  publishedAt,
  readingTimeMinutes,
}: BlogPost): ReactElement => (
  <div className="blog-post-item" key={blogPostId}>
    <BlogPostItem
      title={title}
      readingTimeMinutes={readingTimeMinutes}
      publishedAt={publishedAt}
      blogPostId={blogPostId}
      imageUrl={imageUrl}
      slug={slug}
      key={blogPostId}
    />
  </div>
);

const BlogPage: NextPage<BlogPageProps> = ({ blog }) => {
  if (!blog) {
    return <ErrorPage404 />;
  }

  const { name, description, imageUrl, posts } = blog;
  return (
    <>
      <Head>
        <title>{name}</title>
        {description && <meta name="description" content={description} />}

        <meta key="og:title" property="og:title" content={name} />
        {description && <meta key="og:description" property="og:description" content={description} />}
        {imageUrl && <meta key="og:image" property="og:image" content={imageUrl} />}
      </Head>

      <BlogPageWidthContainer>
        <header className="section-header">
          {name && <BlogTitle>{name}</BlogTitle>}
          {!posts?.length ? (
            <p>Dieser Blog hat noch keine Beiträge. Versuche es später noch einmal.</p>
          ) : (
            <BlogPostItemList>{posts.map((post) => renderBlogPost(post))}</BlogPostItemList>
          )}
        </header>
      </BlogPageWidthContainer>
    </>
  );
};

const GET_BLOG_QUERY = `
  query getBlog($slug: String!) {
    blog(slug: $slug) {
      name
      slug
      description
      imageUrl
      posts {
        blogPostId
        title
        slug
        imageUrl
        publishedAt
        readingTimeMinutes
      }
    }
  }
`;

const fetchBlog = async (slug: string): Promise<Blog | null> => {
  const { data, errors } = await fetchMarketplaceGraphQL(config(), GET_BLOG_QUERY, { variables: { slug } });

  if (errors?.length) {
    throw new Error(errors[0].message);
  }

  return data.blog;
};

export const getServerSideProps: GetServerSideProps = async () => ({
  props: {
    blog: await fetchBlog(config().blog.defaultBlogSlug),
  },
});

export default BlogPage;
