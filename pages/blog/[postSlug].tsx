import React, { ReactElement } from 'react';
import { NextPage, GetServerSideProps, GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import styled from 'styled-components';
import { stringToArray } from '@u2dv/marketplace_common/dist/stringToArray';
import ErrorPage404 from '../404';
import getAbsoluteUrl from '../../helpers/getAbsoluteUrl';
import { firstValue } from '../../helpers/firstValue';
import { fetchMarketplaceGraphQL } from '../../helpers/fetchMarketplaceGraphQL';
import { SharingBar } from '../../components/SharingBar';
import { TagsBar } from '../../components/TagsBar';
import { Newsletter } from '../../components/Newsletter';
import { BlogBlockRenderer, Element } from '../../components/Blogs/BlogBlockRenderer';
import { BlogPostMeta } from '../../components/Blogs/Blocks/BlogPostMeta';
import { HeroImage } from '../../components/Blogs/Blocks/HeroImage';
import { BlogPost } from '../../types/blogs';
import config from '../../config';
import { MainContainer } from '../../components/common/MainContainer';
import { BlogPageWidthContainer } from '../../components/Blogs/BlogPageWidthContainer';

const renderBlogPostContents = ({ contents }: BlogPost): ReactElement[] => {
  const elements: Element[] = JSON.parse(contents);

  return elements.map((element) => <BlogBlockRenderer key={element.key} element={element} />);
};

const SharingBarContainer = styled.div`
  text-align: center;
`;

type BlogPostPageProps = {
  post?: BlogPost;
  postUrl: string;
};

const BlogPostPage: NextPage<BlogPostPageProps> = ({ post, postUrl }) => {
  if (!post) {
    return <ErrorPage404 />;
  }

  const { title, seoTitle, description, authorName, publishedAt, readingTimeMinutes, imageUrl, tagsString } = post;
  return (
    <>
      <Head>
        <title>{seoTitle || title}</title>
        {description && <meta name="description" content={description} />}

        <meta key="og:title" property="og:title" content={title} />
        {description && <meta key="og:description" property="og:description" content={description} />}
        {imageUrl && <meta key="og:image" property="og:image" content={imageUrl} />}
      </Head>

      <BlogPageWidthContainer>
        <MainContainer>
          {tagsString?.trim() && <TagsBar tags={stringToArray(tagsString, ',')} />}
          <BlogPostMeta date={publishedAt} time={readingTimeMinutes} author={authorName} title={title} />
          <SharingBarContainer>
            <SharingBar
              urls={postUrl}
              message={`${title} - {url}`}
              emailSubject={title}
              emailBody={`${description ?? title} - {url}`}
              ctaText="Jetzt teilen"
              emailIconTrackingId="ga-blog-email-share-click"
              whatsAppIconTrackingId="ga-blog-whatsapp-share-click"
              facebookIconTrackingId="ga-blog-facebook-share-click"
              twitterIconTrackingId="ga-blog-twitter-share-click"
            />
          </SharingBarContainer>
          {imageUrl?.trim() && <HeroImage backgroundImage={imageUrl} />}

          {renderBlogPostContents(post)}

          <Newsletter
            title="Sichere Dir exklusive Vorteile, aktuelle Angebote, Produkt-News und Expertentipps."
            subtitle="Nichts mehr verpassen mit unserem Newsletter."
            smallText="Eine Abmeldung ist jederzeit kostenlos möglich. Informationen zu unseren Datenschutzpraktiken findest Du auf unserer Website."
            imagePosition="left"
            ctaText="JETZT ABONNIEREN & GUTSCHEIN SICHERN!"
            ctaUrl="https://www.uptodate.de/pages/newsletter"
            ctaNewWindow
            trackingId="ga-blog-newsletter-subscribe-click"
          />
        </MainContainer>
      </BlogPageWidthContainer>
    </>
  );
};

const GET_BLOG_POST_QUERY = `
  query getBlogPost($slug: String!, $blogSlug: String) {
    blogPost(slug: $slug, blogSlug: $blogSlug) {
      title
      seoTitle
      description
      published
      publishedAt
      imageUrl
      contents
      authorName
      tagsString
      readingTimeMinutes
    }
  }
`;

type BlogPostResponse = {
  errors?: Error[];
  data: {
    blogPost: BlogPost | null;
  };
};

const fetchBlogPost = async (slug: string, blogSlug: string | null = null): Promise<BlogPost | null> => {
  const { data, errors }: BlogPostResponse = await fetchMarketplaceGraphQL(config(), GET_BLOG_POST_QUERY, {
    variables: { slug, blogSlug },
  });

  if (errors?.length) {
    throw new Error(errors[0].message);
  }

  const post = data.blogPost;

  if (post?.published === false) {
    return null; // ignore unpublished posts (will cause 404 page to show)
  }

  return post;
};

export const getServerSideProps: GetServerSideProps = async ({
  req,
  query: { postSlug },
}: GetServerSidePropsContext) => ({
  props: {
    post: postSlug ? await fetchBlogPost(firstValue<string>(postSlug), config().blog.defaultBlogSlug) : null,
    postUrl: getAbsoluteUrl(req).toString(),
  },
});

export default BlogPostPage;
