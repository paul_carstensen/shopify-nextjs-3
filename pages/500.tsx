import { GetStaticProps } from 'next';
import React from 'react';
import { fetchShopifyCollectionProducts } from '@u2dv/marketplace_common/dist/fetchShopifyCollectionProducts';
import ErrorPage from '../components/error_pages/ErrorPage';
import config from '../config';

type ErrorPage500Props = {
  bestsellers?: any[];
};

export const getStaticProps: GetStaticProps = async () => {
  const { graphqlUrl: url, token } = config().storefront;
  const products = await fetchShopifyCollectionProducts(url, token, 'bestseller');

  return {
    props: {
      // @ts-expect-error TODO: type bestsellers all the way down, ErrorPage component, ....
      bestsellers: products.map(({ title, handle, images, priceRange, compareAtPriceRange }) => ({
        title,
        handle,
        price: priceRange.minVariantPrice.amount,
        originalPrice: compareAtPriceRange.minVariantPrice.amount,
        imageUrl: images[0]?.src,
      })),
    },
  };
};

const ErrorPage500: React.FC<ErrorPage500Props> = ({ bestsellers }) => (
  <ErrorPage
    errorText="BEIM LADEN DER SEITE IST EIN FEHLER AUFGETRETEN."
    bestsellers={bestsellers}
    bestsellersCollectionHandle="bestsellers"
  />
);

export default ErrorPage500;
