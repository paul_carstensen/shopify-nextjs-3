import React, { ReactElement, useRef, useState, RefObject } from 'react';
import { NextPage, GetServerSideProps, GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';
import { ResultSlideFirst, RIGHT_SLIDE_FIRST_CONTAINER_ID } from '../../components/sicherheit/crime/ResultSlideFirst';
import {
  ResultSlideSecond,
  RIGHT_SLIDE_SECOND_CONTAINER_ID,
} from '../../components/sicherheit/crime/ResultSlideSecond';
import { ResultSlideThird } from '../../components/sicherheit/crime/ResultSlideThird';
import { CrimeInfo } from '../../types/sicherheit/wie-sicher-ist-dein-wohnort';
import { LeftScreen, LEFT_CONTAINER_ID } from '../../components/sicherheit/crime/LeftScreen';
import { RightLandingScreen } from '../../components/sicherheit/crime/RightLandingScreen';
import getAbsoluteUrl from '../../helpers/getAbsoluteUrl';
import { SharingBar } from '../../components/SharingBar';
import { ContentSubtitle } from '../../components/sicherheit/crime/ContentSubtitle';
import { ContentDescription } from '../../components/sicherheit/crime/ContentDescription';
import burglarImage from '../../assets/images/crime-check/burglar.jpg';

const MainContainer = styled.div`
  height: calc(100vh - 178px);
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  margin-top: 16px;

  @media screen and (max-width: 768px) {
    height: 100%;
    padding-top: 16px;
  }
`;

const FixedContainer = styled.div`
  height: calc(100vh - 178px);
  width: 50%;

  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;

const ScrollContainers = styled.div`
  height: 100%;
  overflow-y: scroll;
  width: 50%;
  scroll-snap-type: y proximity;

  ::-webkit-scrollbar {
    width: 0 !important;
    height: 0 !important;
  }

  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;

const renderSharingBar = (): ReactElement => {
  const url = 'https://www.uptodate.de/sicherheit/wie-sicher-ist-dein-wohnort';
  const message = 'Wie sicher ist Dein Wohnort? Informiere Dich hier: {url}';
  const emailSubject = 'Wie sicher ist Dein Wohnort? Informiere Dich jetzt';
  const emailBody = `Jedes Jahr werden die Kriminalitätsstatistiken für Städte und Gemeinden in Deutschland veröffentlicht.
  
Obwohl Deutschland allgemein als sicheres Land gilt, gibt es erhebliche Unterschied abhängig vom eigenen Wohnort. Teile von Deutschland sind wahre Hochburgen der Kriminalität, während andere Teile als sehr sicher anzusehen sind. 
  
Prüfe jetzt Dein persönliches Risikopotential ausgehend von dem Ort an dem Du wohnst: {url}`;

  return (
    <SharingBar
      variant="white"
      urls={url}
      message={message}
      emailSubject={emailSubject}
      emailBody={emailBody}
      emailIconTrackingId="ga-crime-check-email-share-click"
      whatsAppIconTrackingId="ga-crime-check-whatsapp-share-click"
      facebookIconTrackingId="ga-crime-check-facebook-share-click"
      twitterIconTrackingId="ga-crime-check-twitter-share-click"
    />
  );
};

type CrimeCheckPageProps = {
  socialMediaImageUrl: string;
};

const CrimeCheckPage: NextPage<CrimeCheckPageProps> = ({ socialMediaImageUrl }) => {
  const title = 'Kriminalitätscheck';
  const description =
    'Obwohl Deutschland allgemein als sicheres Land gilt, sind Teile des Landes wahre Hochburgen der Kriminalität. Was Sicherheit angeht, gibt es erhebliche lokale Unterschiede für Städte und Gemeinden, wie die jährlichen Kriminalitätsstatistiken des Bundeskriminalamts bestätigen. Überprüfe jetzt wie das Gefahrenpotential in Deiner Stadt aussieht.';
  const scrollContainerRef = useRef<HTMLDivElement>(null);
  const isMobile = useMediaQuery({ query: '(max-width: 768px)' });
  const [crimeCheckInfo, setCrimeCheckInfo] = useState<CrimeInfo | undefined>(undefined);

  const goToNextSlide = (id: string, ref: RefObject<HTMLDivElement>) => {
    const el = document.getElementById(id);
    if (!el) {
      return;
    }

    const header = document.querySelector('.header-section');
    if (!header) {
      return;
    }

    if (!isMobile) {
      if (!ref.current) {
        return;
      }

      const top = el.getBoundingClientRect().top + el.getBoundingClientRect().height + ref.current.scrollTop;
      ref.current.scrollTo({ top, behavior: 'smooth' });
    } else {
      const top =
        el.getBoundingClientRect().top +
        window.pageYOffset +
        el.getBoundingClientRect().height -
        header.getBoundingClientRect().height;
      window.scrollTo({ top, behavior: 'smooth' });
    }
  };

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />

        <meta key="og:title" property="og:title" content={title} />
        <meta key="og:description" property="og:description" content={description} />
        <meta key="og:image" property="og:image" content={socialMediaImageUrl} />
      </Head>

      <MainContainer>
        <FixedContainer>
          <LeftScreen
            renderSharingBar={renderSharingBar}
            onArrowButtonClick={() => goToNextSlide(LEFT_CONTAINER_ID, scrollContainerRef)}
          />
        </FixedContainer>
        <ScrollContainers ref={scrollContainerRef}>
          {!crimeCheckInfo && <RightLandingScreen setCrimeCheckInfo={setCrimeCheckInfo} />}
          {crimeCheckInfo && (
            <>
              <ResultSlideFirst
                backgroundColor="#c4d6e6"
                counter="1"
                onArrowButtonClick={() => goToNextSlide(RIGHT_SLIDE_FIRST_CONTAINER_ID, scrollContainerRef)}
              >
                <ContentSubtitle>Erfahre hier wie Du Dich schützen kannst!</ContentSubtitle>
                <ContentDescription>
                  Das Bundeskriminalamt veröffentlicht jedes Jahr die Kriminalstatistik für Städte und Gemeinden
                  (Quelle: PKS Bundeskriminalamt, Berichtszeitraum: 01.01.2020 bis 31.12.2020, V1.0). Während Berlin die
                  Stadt mit den meisten Verbrechen pro Kopf ist, hat Regensburg, proportional bezogen auf die
                  Einwohnerzahl, die wenigsten kriminellen Handlungen in 2020 zu verzeichnen.
                </ContentDescription>
                <ContentDescription>
                  Der Blick auf die Gesamtzahlen kann allerdings täuschen. Auch vermeintlich sichere Städte können
                  Hochburgen für bestimmte kriminelle Aktivitäten sein. Zum Beispiel ist Leipzig mit über 9000 Fällen
                  die Stadt mit den meisten Fahrraddiebstählen pro Kopf in Deutschland. Darüber hinaus könnte sich in
                  Krefeld eine Alarmanlage besonders lohnen. Hier werden die meisten Wohnungseinbrüche pro Kopf verübt.
                </ContentDescription>
              </ResultSlideFirst>
              <ResultSlideSecond
                backgroundColor="#19232d"
                counter="2"
                crimeCheckInfo={crimeCheckInfo}
                onArrowButtonClick={() => goToNextSlide(RIGHT_SLIDE_SECOND_CONTAINER_ID, scrollContainerRef)}
                buttonUrl="https://www.uptodate.de/pages/sicherung-zuhause"
                buttonTrackingId="ga-crime-check-more-info-click"
              >
                <ContentSubtitle fontColor="white">Wie sicher ist Dein Wohnort?</ContentSubtitle>
                <ContentDescription fontColor="white">
                  Relatives Gefahrenpotential bezogen auf die Einwohnerzahl:
                </ContentDescription>
              </ResultSlideSecond>
              <ResultSlideThird
                backgroundColor="#c4d6e6"
                counter="3"
                buttonUrl="https://www.uptodate.de/pages/sicherung-zuhause"
                buttonTrackingId="ga-crime-check-more-info-click"
              >
                <ContentDescription>
                  Wichtig ist auch die Aufklärungsquote bei verübten Verbrechen. Bei Raubüberfällen ist die
                  Aufklärungsquote in der Regel sehr hoch. Die Mehrheit der Raubüberfälle wird aufgeklärt und der Täter
                  geschnappt. Demgegenüber stehen Tatbestände wie Wohnungseinbrüche. Nur ein Bruchteil aller Einbrüche
                  wird aufgeklärt und auch das Diebesgut ist in der Regel verloren.
                </ContentDescription>
                <ContentDescription>
                  Grund dafür ist, dass das Hab und Gut meist nicht ausreichend geschützt ist. 96% aller Haushalte
                  verfügen über keinerlei Vorkehrungen zum Schutz gegen Einbrüche. Dabei ist die gute Wirksamkeit von
                  Alarmanlagen und anderen Vorrichtungen erwiesen: laut BKA könnten mehrere tausend Einbruchsversuche
                  jedes Jahr durch Vorrichtungen verhindert werden. Prävention galt lange als teuer und aufwendig. Das
                  ist heutzutage dank Smart Home-Produkten anders. Mit wenig Aufwand und Budget ist es möglich, das
                  Eigenheim vor Einbrüchen und Diebstählen rund ums Haus zu schützen. In diesem Zusammenhang haben wir
                  Dir alle nötigen Informationen zusammengestellt.
                </ContentDescription>
              </ResultSlideThird>
            </>
          )}
        </ScrollContainers>
      </MainContainer>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ req }: GetServerSidePropsContext) => ({
  props: {
    socialMediaImageUrl: getAbsoluteUrl(req, burglarImage.src).toString(),
  },
});

export default CrimeCheckPage;
