import React, { useState, useEffect } from 'react';
import { NextPage, GetServerSideProps } from 'next';
import Head from 'next/head';
import styled from 'styled-components';
import { Slider } from '@u2dv/marketplace_ui_kit/dist/components';
import cache, { keys } from '../../cache';
import { getPolicyText } from '../../helpers/consent_tracker/api';
import { QuestionCTA } from '../../components/QuestionCTA';
import { KeepMeUpdatedBody, Lifestyle, QuestionRisk } from '../../types/risk/lifestyle';
import riskQuestions from '../../helpers/lifeStyle/riskQuestions';
import { submitLifeStyles } from '../../helpers/lifeStyle/api';
import { PickYourLifestyle } from '../../components/Risk/Lifestyle/PickYourLifestyle';
import { RiskDataConsent } from '../../components/Risk/Lifestyle/RiskDataConsent';

const SliderContainer = styled.div`
  margin-bottom: 30px;
  max-width: 300px;
  margin: 0 auto 30px auto;

  @media screen and (min-width: 768px) {
    min-height: 700px;
    display: flex;
    align-items: center;
  }
`;

const Container = styled.div`
  margin: auto;
  max-width: 950px;
  margin-bottom: 40px;
`;

const QuestionContainer = styled.div`
  max-width: 400px;
  margin: auto;
`;

const userRiskData = {
  email: null,
  submittedQuestions: [],
  selectedLifestyleKey: '',
};

const CONSENT_GTM_NAME = 'Google Analytics';

const usercentricsLoaded = (): boolean =>
  // Usercentrics SDK will not be loaded during SSR or in development mode
  typeof window?.UC_UI?.getServicesBaseInfo === 'function';

type ConsentObject = {
  name: string;
  consent: {
    status: boolean;
  };
  id: string;
};

const getConsentObject = (vendorName: string): ConsentObject | undefined => {
  if (!usercentricsLoaded()) {
    return undefined;
  }

  const consents = window.UC_UI.getServicesBaseInfo() as ConsentObject[];

  return consents.find(({ name }) => name === vendorName);
};

const getConsentStatus = (vendorName: string): boolean => {
  const { consent } = getConsentObject(vendorName) || { consent: { status: false } };

  return consent.status;
};

const getPolicyTextHtml = async (): Promise<string> => {
  let policyTextHtml: string | undefined = cache.get(keys.LIFESTYLE_POLICY_TEXT);

  if (!policyTextHtml) {
    policyTextHtml = await getPolicyText('lifestyle');

    cache.set(keys.LIFESTYLE_POLICY_TEXT, policyTextHtml, 3600);
  }

  return policyTextHtml;
};

type LifestylePageProps = {
  policyTextHtml: string;
};

const LifestylePage: NextPage<LifestylePageProps> = ({ policyTextHtml }) => {
  const title = 'Welcher Lifestyle-Typ bist Du? Finde es raus – uptodate';
  const description =
    'Weißt Du, welche Risiken Du im Alltag hast? ➤ Mach den Test und finde Deinen Lifestyle-Typ inkl. Risikobewertung. ✓Gesundheit ✓Psyche ✓Beziehungen';

  const [riskData, setRiskData] = useState<KeepMeUpdatedBody>(userRiskData);
  const [hasConsent, setHasConsent] = useState(false);
  const [top3Lifestyles, setTop3Lifestyles] = useState<Lifestyle[]>([]);

  const handleUserCentricsInitialized = () => {
    setHasConsent(getConsentStatus(CONSENT_GTM_NAME));
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [riskData.selectedLifestyleKey, top3Lifestyles.length, riskData.submittedQuestions]);

  useEffect(() => {
    handleUserCentricsInitialized();

    window?.addEventListener('UC_UI_INITIALIZED', handleUserCentricsInitialized);

    return () => {
      window?.removeEventListener('UC_UI_INITIALIZED', handleUserCentricsInitialized);
    };
  }, []);

  const answerQuestion = async (
    question: string,
    answerId: number,
    answer: string[],
    i: number,
    totalRiskQuestions: number
  ) => {
    const newAnswer: QuestionRisk = {
      question,
      answerId,
      answer,
    };

    const copyRiskData: KeepMeUpdatedBody = JSON.parse(JSON.stringify(riskData));
    copyRiskData.submittedQuestions.push(newAnswer);

    setRiskData(copyRiskData);

    if (i + 1 === totalRiskQuestions) {
      const userConsent = hasConsent ? 1 : 0;
      const lifestyleAnswers = copyRiskData.submittedQuestions.map((x) => x.answer);
      const lifeStyles = await submitLifeStyles(lifestyleAnswers, userConsent);
      setTop3Lifestyles(lifeStyles);
    }
  };

  const BuildHead: NextPage = () => (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
      <meta name="robots" content="none" />
      <meta key="og:title" property="og:title" content={title} />
      <meta key="og:description" property="og:description" content={description} />
    </Head>
  );

  if (top3Lifestyles.length === 0) {
    return (
      <>
        <BuildHead />
        <Container>
          <QuestionContainer>
            <SliderContainer>
              <Slider
                visibleSlidesBreakpoints={1}
                enableArrows={false}
                enableMouseWheel={false}
                disableHorizontalScroll
              >
                {riskQuestions.map((riskQuestion, i) => (
                  <QuestionCTA
                    key={`question-${riskQuestion.questions}`}
                    title={riskQuestion.questions}
                    options={riskQuestion.answers.map((option) => ({
                      title: option.answer,
                      dataTrackingId: option.dataTrackingId,
                      lifeStyle: option.lifeStyle,
                      answerId: option.answerId,
                      onClick: () =>
                        answerQuestion(
                          riskQuestion.questions,
                          option.answerId,
                          option.lifeStyle,
                          i,
                          riskQuestions.length
                        ),
                      dataTestId: `risk-question-${i + 1}-${option.answerId}`,
                    }))}
                    minHeight="calc(100vh - 140px)"
                    minHeightDesktop="500px"
                    pagination={i + 1}
                    totalPagination={riskQuestions.length}
                    bgImage={riskQuestion.bgImage}
                  />
                ))}
              </Slider>
            </SliderContainer>
          </QuestionContainer>
        </Container>
      </>
    );
  }

  return (
    <>
      <BuildHead />
      <Container>
        {!riskData.selectedLifestyleKey ? (
          <PickYourLifestyle
            pageTitle="Erkennst Du Dich wieder?"
            pageDescription="Basierend auf Deinen Antworten haben wir 3 Lebensstile für Dich gefunden"
            lifestyles={top3Lifestyles}
            riskData={riskData}
            setRiskData={setRiskData}
          />
        ) : (
          <RiskDataConsent
            title="Deine Ergebnisse"
            policyTextHtml={policyTextHtml}
            riskData={riskData}
            setRiskData={setRiskData}
            hasConsent={hasConsent}
          />
        )}
      </Container>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async () => ({
  props: {
    policyTextHtml: await getPolicyTextHtml(),
  },
});

export default LifestylePage;
