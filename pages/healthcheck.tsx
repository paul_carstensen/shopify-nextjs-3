import { NextPage, NextPageContext } from 'next';

const HealthcheckPage: NextPage = () => <></>;

export const getServerSideProps = async ({ res }: NextPageContext) => {
  if (res) {
    res.setHeader('X-Robots-Tag', 'noindex');
    res.write('OK');
    res.end();
  }

  return {
    props: {},
  };
};

export default HealthcheckPage;
