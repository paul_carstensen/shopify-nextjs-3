if (!process.env.SITE_URL) {
  throw new Error('SITE_URL is not set properly');
}

module.exports = {
  siteUrl: process.env.SITE_URL,
  sitemapBaseFileName: 'sitemap-nextjs',
  exclude: [
    '/500',
    '/healthcheck',
    '/sitemap-nextjs-dynamic.xml',
    '/t/*',
    '/sicherheit/lifestyle-checker',
  ],
};
