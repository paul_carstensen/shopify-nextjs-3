function asg_instance_count {
    if [[ "$1" == 'B' ]] || [[ "$1" == 'G' ]]; then
        local bg="${1}"
    else
        echo "First parameter needs to be either 'B' or 'G'"
        return 1
    fi
    local stack="$2"

    local instances=$(aws autoscaling describe-auto-scaling-groups \
        --region $AWS_REGION \
        --auto-scaling-group-names "${stack}-${bg}" \
        --output text \
        --query 'AutoScalingGroups[*].Instances[?LifecycleState == `InService`].InstanceId')

    echo $instances | wc -w | awk {'print $1'}
}

function get_deployment_parameters {
    local stack="$1"

    #find B/G ASG and get the number of instances in it
    local instance_count_b=$(asg_instance_count B $stack)

    local current_stack_bg="B"
    local new_stack_bg="G"
    local instance_count="$instance_count_b"
    if [[ "$instance_count_b" == "0" ]]; then
        current_stack_bg="G"
        new_stack_bg="B"
        instance_count=$(asg_instance_count G $stack)
    fi

    #if there are no instances, use default instance count
    if [[ "$instance_count" == "0" ]]; then
        instance_count=2
    fi

    echo "$current_stack_bg $new_stack_bg $instance_count"
}

function switch_bg {
    local stack="$1"
    local listener="$2"
    local target_group="$3"
    local old_stack_bg="$4"

    #switch targets
    aws elbv2 modify-listener \
        --region $AWS_REGION \
        --listener-arn $listener \
        --default-actions Type=forward,TargetGroupArn=$target_group > /dev/null

    #remove old stack
    aws cloudformation delete-stack \
        --region $AWS_REGION \
        --stack-name "${stack}-${old_stack_bg}"
}


