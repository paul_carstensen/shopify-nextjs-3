#!/bin/bash

set -Eeuo pipefail

source ./bash_utils/stack.sh
source ./bash_utils/params.sh

AWS_REGION=eu-central-1

check_environment_var

STACK="shopify-nextjs-alb-$ENVIRONMENT"

vpc_output=$(stack_output VPC-$ENVIRONMENT)
private_subnets=$(param_from_stack_output "$vpc_output" PublicSubnets)
vpc_id=$(param_from_stack_output "$vpc_output" VPC)

app_zone_output=$(stack_output shopify-nextjs-route53)
app_zone_id=$(param_from_stack_output "$app_zone_output" ZoneId)

uptodate_zone_output=$(stack_output route53)
uptodate_zone_id=$(param_from_stack_output "$uptodate_zone_output" UptodateDeZoneId)

alarming_output=$(stack_output error-notifier-sns-$ENVIRONMENT)
alarming_sns=$(param_from_stack_output "$alarming_output" AlarmingSNS)

aws cloudformation deploy \
  --region $AWS_REGION \
  --stack-name $STACK \
  --template-file stacks/shopify-nextjs-alb-stack.yaml \
  --no-fail-on-empty-changeset \
  --parameter-overrides EnvironmentName=$ENVIRONMENT \
                        Subnets=$private_subnets \
                        VpcId=$vpc_id \
                        AlarmingSNSTopic=$alarming_sns \
                        AppUptodateDeZoneId=$app_zone_id \
                        UptodateDeZoneId=$uptodate_zone_id
