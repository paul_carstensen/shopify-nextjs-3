#!/bin/bash

set -Eeuox pipefail

./shopify-nextjs-role.sh
./shopify-nextjs-logs.sh
./shopify-nextjs-route53.sh
./shopify-nextjs-alb.sh
./shopify-nextjs-cloudfront.sh
./shopify-nextjs-ec2.sh
