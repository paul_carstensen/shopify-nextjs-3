// using CommonJS module instead of ES6 module since next.config.js doesn't support ES6 modules it requires this file

/* eslint-disable @typescript-eslint/no-var-requires */
const base = require('./config/default.json');
const dev = require('./config/dev.json');
const prod = require('./config/prod.json');

let local;
try {
  local = require('./config/local.json');
} catch (err) {
  local = {};
}

const isObject = (o) => typeof o === 'object' && !Array.isArray(o);

const setEnvironmentValues = (json) => {
  const doIt = (o) => {
    if (isObject(o)) {
      return Object.fromEntries(Object.entries(o).map(([key, value]) => [key, doIt(value)]));
    }
    if (Array.isArray(o)) {
      return (o).map(doIt);
    }
    if (typeof o === 'string') {
      return `${o}`.replace(
        /^__ENV__(.+)/g,
        (_str, match) => process.env[match] || `missing process.env[${match}]`,
      );
    }
    return o;
  };
  return doIt(json);
};

const deepMerge = (source, override) => {
  if (isObject(source) && isObject(override)) {
    return Object.keys(source).reduce((output, key) => {
      // eslint-disable-next-line no-param-reassign
      output[key] = override[key] === undefined ? source[key] : deepMerge(source[key], override[key]);
      return output;
    }, {});
  }
  return override;
};

let cached;

const config = () => {
  if (cached !== undefined) {
    return cached;
  }
  let override;
  switch (process.env.NEXT_PUBLIC_CONFIG_ENV?.toLowerCase()) {
    case 'dev':
      override = dev;
      break;
    case 'prod':
      override = prod;
      break;
    default:
      override = local;
  }
  cached = setEnvironmentValues(deepMerge(base, override));
  return cached;
};

module.exports = config;
