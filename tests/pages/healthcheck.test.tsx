/** @jest-environment jsdom */
import { NextPageContext } from 'next';
import { render } from '@testing-library/react';
import HealthcheckPage, { getServerSideProps } from '../../pages/healthcheck';

const makeMockResponse = () => {
  const res = jest.fn() as any;
  res.setHeader = jest.fn();
  res.write = jest.fn();
  res.end = jest.fn();

  return res;
};

describe('Health check page', () => {
  it('renders without crashing', () => {
    expect(() => {
      render(<HealthcheckPage />);
    }).not.toThrowError();
  });

  it('sends "OK" as the response', async () => {
    const res = makeMockResponse();

    await getServerSideProps({ res } as NextPageContext);

    expect(res.write).toHaveBeenCalledWith('OK');
    expect(res.end).toHaveBeenCalled();
  });

  it('sends "noindex" HTTP header', async () => {
    const res = makeMockResponse();

    await getServerSideProps({ res } as NextPageContext);

    expect(res.setHeader).toHaveBeenCalledWith('X-Robots-Tag', 'noindex');
  });
});
