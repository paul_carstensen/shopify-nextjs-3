/** @jest-environment jsdom */
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { getPage } from 'next-page-tester';
import { mocked } from 'ts-jest/utils';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { getNavigationMenus } from '../../../helpers/getNavigationMenus';
import { getPolicyText } from '../../../helpers/consent_tracker/api';
import config from '../../../config';

window.HTMLElement.prototype.scrollTo = jest.fn();

const handlers = [
  rest.post(`${config().risk.apiUrl}/submit-lifestyles/0`, (_, res, ctx) => res(
    ctx.status(200),
    ctx.body(JSON.stringify([
      {
        lifestyleKey: 'luxury_lover',
        title: 'Luxury Lover',
        kpiAllPercent: 9.7,
        kpiAllAmount: 6.8,
        kpiAllPrediction: 'down',
        kpiCoregroupAmount: 3,
        kpiCoregroupAge: '14-34',
        kpiIncome: 1706,
        statementBeliefs: 'Lorem ipsum',
        statementMotto: 'Lorem ipsum',
        statementLifegoal: 'Lorem ipsum',
        shortDescription: 'Lorem ipsum',
        description: 'Lorem ipsum',
        score: 2,
      },
      {
        lifestyleKey: 'autopilot',
        title: 'Autopilot',
        kpiAllPercent: 9.7,
        kpiAllAmount: 6.8,
        kpiAllPrediction: 'down',
        kpiCoregroupAmount: 3,
        kpiCoregroupAge: '14-34',
        kpiIncome: 1706,
        statementBeliefs: 'Lorem ipsum',
        statementMotto: 'Lorem ipsum',
        statementLifegoal: 'Lorem ipsum',
        shortDescription: 'Lorem ipsum',
        description: 'Lorem ipsum',
        score: 2,
      },
      {
        lifestyleKey: 'multitasker',
        title: 'Multitasker',
        kpiAllPercent: 9.7,
        kpiAllAmount: 6.8,
        kpiAllPrediction: 'down',
        kpiCoregroupAmount: 3,
        kpiCoregroupAge: '14-34',
        kpiIncome: 1706,
        statementBeliefs: 'Lorem ipsum',
        statementMotto: 'Lorem ipsum',
        statementLifegoal: 'Lorem ipsum',
        shortDescription: 'Lorem ipsum',
        description: 'Lorem ipsum',
        score: 2,
      },
    ])),
  )),
];

const server = setupServer(...handlers);

jest.mock('next/image', () => ({
  __esModule: true,
  default: () => <></>, // eslint-disable-line react/display-name
}));

jest.mock('../../../helpers/getNavigationMenus');
jest.mock('../../../helpers/consent_tracker/api');

const renderPage = async () => {
  const page = await getPage({
    route: '/sicherheit/lifestyle-checker',
  });

  return page.render();
};

describe('Lifestyle risk page, question iteration', () => {
  beforeAll(() => {
    mocked(getNavigationMenus).mockResolvedValue({
      contentMain: [],
      contentSide: [],
      contentFooter: [],
    });
  });

  it('should go through all questions', async () => {
    mocked(getPolicyText).mockResolvedValueOnce('Policy text');

    await renderPage();

    // q => question, a => answer

    // First question
    expect(screen.getByText('Ich bin ein Pionier des Wandels')).toBeInTheDocument();

    const q1a2 = screen.getByTestId('risk-question-1-2');

    userEvent.click(q1a2);

    // Second question
    expect(screen.getByText('Ich stehe für Achtsamkeit und Werte der Entschleunigung')).toBeInTheDocument();

    const q2a1 = screen.getByTestId('risk-question-2-1');

    userEvent.click(q2a1);

    // Third question
    expect(screen.getByText('Ich bin ein Trendsetter')).toBeInTheDocument();

    const q3a5 = screen.getByTestId('risk-question-3-5');

    userEvent.click(q3a5);

    // Fourth question
    expect(screen.getByText('Ich stehe für nachhaltigen Wachstum')).toBeInTheDocument();

    const q4a2 = screen.getByTestId('risk-question-4-2');

    userEvent.click(q4a2);

    // Fifth question
    expect(screen.getByText('Ich interessiere mich für zukünftige Wohnkonzepte')).toBeInTheDocument();

    const q5a6 = screen.getByTestId('risk-question-5-6');

    userEvent.click(q5a6);

    // Sixth question
    expect(screen.getByText('Ich begebe mich auf kulinarische Abenteuer')).toBeInTheDocument();

    const q6a6 = screen.getByTestId('risk-question-6-6');

    userEvent.click(q6a6);

    // Seventh question
    expect(screen.getByText('Ich bin digital affin')).toBeInTheDocument();

    const q7a3 = screen.getByTestId('risk-question-7-3');

    userEvent.click(q7a3);

    // Eightth question
    expect(screen.getByText('Mein Gesundheitsbewusstsein prägt meinen Alltag')).toBeInTheDocument();

    const q8a1 = screen.getByTestId('risk-question-8-1');

    userEvent.click(q8a1);

    // Ninth question
    expect(screen.getByText('Familie geht über alles')).toBeInTheDocument();

    const q9a1 = screen.getByTestId('risk-question-9-1');

    userEvent.click(q9a1);

    // Tenth question
    expect(screen.getByText('Alter')).toBeInTheDocument();
  });
});

describe('Lifestyle risk page, lifestyle submission', () => {
  beforeAll(() => {
    mocked(getNavigationMenus).mockResolvedValue({
      contentMain: [],
      contentSide: [],
      contentFooter: [],
    });
    server.listen();
  });

  afterEach(() => server.resetHandlers());

  afterAll(() => server.close());

  it('should submit the user answers and go to the top 3 lifestyles', async () => {
    mocked(getPolicyText).mockResolvedValueOnce('Policy text');

    await renderPage();

    const q10a1 = screen.getByTestId('risk-question-10-1');

    // Click on one of the final anwsers
    userEvent.click(q10a1);

    expect(await screen.findByText('Erkennst Du Dich wieder?')).toBeInTheDocument();

    expect(await screen.findByText('Luxury Lover')).toBeInTheDocument();
    expect(await screen.findByText('Autopilot')).toBeInTheDocument();
    expect(await screen.findByText('Multitasker')).toBeInTheDocument();

    const autopilotTab = screen.getByTestId('lifestyle-tab-autopilot');

    userEvent.click(autopilotTab);

    const submitLifestyleBtn = screen.getByText('Passt am besten zu mir');

    userEvent.click(submitLifestyleBtn);

    expect(submitLifestyleBtn).not.toBeInTheDocument();

    const keepMeUpdatedBtn = screen.getByText('bleib uptodate');

    expect(keepMeUpdatedBtn).toBeInTheDocument();
  });
});
