import styled from 'styled-components';

export const ButtonLink = styled.a`
  background-color: white;
  font-family: Quicksand;
  font-size: 12px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 3px;
  padding: 20px;
  width: fit-content;
  text-transform: uppercase;
  text-decoration: none;
  text-align: center;
  color: #19232d;
  border: 1px solid #e6e6e6;
  border-radius: 0px;
  cursor: pointer;
`;

export const ButtonLinkDark = styled(ButtonLink)`
  background-color: #19232d;
  color: white;
  border: 1px solid #19232d;

  :hover {
    background-color: #19232d;
    color: white;
  }
`;

export const ButtonLinkLight = styled(ButtonLink)`
  background-color: white;
  color: #19232d;
  border: 1px solid #e6e6e6;

  :hover {
    background-color: white;
    color: #19232d;
  }
`;

export const RoundedButtonLinkDark = styled(ButtonLinkDark)`
  border-radius: 20px;
  padding: 5px 30px;
  font-family: Poppins;
  font-size: 18px;
  font-weight: bold;

  @media screen and (max-width: 768px) {
    font-size: 12px;
    width: 100%;
  }
`;

export const CtaButton = styled(ButtonLinkDark)`
  text-transform: uppercase;
  letter-spacing: 3px;
  font-size: 13px;
  font-weight: 500;
  display: block;
  margin-bottom: 30px;
  padding: 12px 21.4px 12px 22px !important;

  @media screen and (max-width: 768px) {
    font-size: 10px;
    line-height: 2;
    padding: 10px !important;
  }
`;

export const CenteredCtaButton = styled(CtaButton)`
  margin-left: auto;
  margin-right: auto;
`;
