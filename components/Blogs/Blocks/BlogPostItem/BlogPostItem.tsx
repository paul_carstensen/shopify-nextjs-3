import React from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import { BlogPost } from '../../../../types/blogs';
import { BlogTime } from '../BlogTime';
import { BlogDate } from '../BlogDate';

const BlogPostItemContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 20px;

  @media screen and (max-width: 768px) {
    padding: 0px;
  }
`;

const BlogPostImage = styled(Image)`
  object-fit: cover;
`;

const BlogMetaDetails = styled.div`
  display: flex;
  padding-top: 20px;
  justify-content: center;
  margin: auto;

  div {
    font-size: 14px;
    line-height: 1.58;
  }

  @media screen and (max-width: 768px) {
    flex-wrap: wrap;
  }
`;

const BlogTitle = styled.div`
  font-size: 18px;
  text-transform: uppercase;
  font-weight: 500;
  line-height: 1.33;
  padding-top: 14px;
  text-align: center;
  padding-bottom: 30px;
`;

export type BlogPostItemFromGqlPrefetch = Pick<
  BlogPost,
  'blogPostId' | 'title' | 'publishedAt' | 'readingTimeMinutes' | 'imageUrl'
>;
export type BlogPostItemProps = Pick<
  BlogPost,
  'blogPostId' | 'title' | 'publishedAt' | 'readingTimeMinutes' | 'imageUrl' | 'slug'
>;

export const BlogPostItem: React.FC<BlogPostItemProps> = ({
  blogPostId,
  title,
  publishedAt,
  readingTimeMinutes,
  imageUrl,
  slug,
}) => (
  <BlogPostItemContainer key={blogPostId}>
    <a href={`/blog/${slug}`}>
      {imageUrl && <BlogPostImage src={imageUrl} width="410" height="200" />}
      <BlogMetaDetails>
        {publishedAt && <BlogDate date={publishedAt} />}
        {readingTimeMinutes && <BlogTime time={readingTimeMinutes} />}
      </BlogMetaDetails>
      <BlogTitle>{title}</BlogTitle>
    </a>
  </BlogPostItemContainer>
);
