import React from 'react';
import styled from 'styled-components';

interface ImageWrapperProps {
  backgroundImageUrl: string;
}

export interface HeroImageProps {
  backgroundImage: string;
}

const ImageWrapper = styled.div<ImageWrapperProps>`
  background-image: url(${(props) => props.backgroundImageUrl});
  background-size: cover;
  background-position: center;
  min-height: 452px;
  width: 100%;
  margin-bottom: 40px;
  @media screen and (max-width: 768px) {
    min-height: 415px;
  }
`;

const HeroImageContainer = styled.div`
  margin-bottom: 40px;
  @media screen and (max-width: 768px) {
    margin: 20px -20px 0px -20px;
  }
`;

export const HeroImage: React.FC<HeroImageProps> = (props: HeroImageProps) => {
  const { backgroundImage } = props;

  return (
    <HeroImageContainer>
      <ImageWrapper backgroundImageUrl={backgroundImage} />
    </HeroImageContainer>
  );
};
