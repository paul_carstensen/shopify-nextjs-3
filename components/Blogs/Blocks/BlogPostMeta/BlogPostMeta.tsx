import React from 'react';
import styled from 'styled-components';
import { BlogTime } from '../BlogTime';
import { BlogDate } from '../BlogDate';
import { PageWidthContainer } from '../../../common/PageWidthContainer';
import { HeaderAnchorOffset } from '../../../common/HeaderAnchorOffset';

interface AuthorItemProps {
  author?: string;
}

const BlogPostMetaContainer = styled.div`
  margin-bottom: -20px;
  @media screen and (max-width: 768px) {
    margin-top: 20px;
    margin-bottom: 10px;
  }
`;

const BlogMetaDetails = styled.div`
  display: flex;
  padding-bottom: 12px;
  justify-content: center;
  margin: auto;
  @media screen and (max-width: 768px) {
    flex-wrap: wrap;
  }
`;

const BlogTitle = styled.div`
  font-size: 32px;
  font-weight: 500;
  text-transform: uppercase;
  text-align: center;
  @media screen and (max-width: 768px) {
    font-size: 24px;
    line-height: normal;
  }
`;

const Author = styled.div`
  font-size: 18px;
  line-height: 1.33;
`;

const AuthorItem: React.FC<AuthorItemProps> = ({ author }) => <Author>| von {author}</Author>;

export type BlogPostMetaProps = {
  date?: string;
  time?: number;
  author?: string;
  title?: string;
};

export const BlogPostMeta: React.FC<BlogPostMetaProps> = ({ date, time, author, title }) => (
  <HeaderAnchorOffset id="blog_post_meta">
    <PageWidthContainer>
      <BlogPostMetaContainer>
        <BlogMetaDetails>
          {date && <BlogDate date={date} />}
          {time && <BlogTime time={time} />}
          {author && <AuthorItem author={author} />}
        </BlogMetaDetails>
        {title && <BlogTitle>{title}</BlogTitle>}
      </BlogPostMetaContainer>
    </PageWidthContainer>
  </HeaderAnchorOffset>
);
