import React from 'react';
import styled from 'styled-components';

const TextInner = styled.div<{ textAlign: string }>`
  margin-bottom: 10px;
  @media screen and (min-width: 769px) {
    margin: auto;
    margin-bottom: 40px;
    width: 100%;
    font-size: 16px;
    line-height: 1.56;
    ${(props) => `text-align: ${props.textAlign};`}
  }
`;

export interface TextProps {
  textAlign?: string;
  children: React.ReactNode;
}

export const Text: React.FC<TextProps> = ({ children, textAlign = 'left' }: TextProps) => (
  <TextInner textAlign={textAlign}>{children}</TextInner>
);
