import React from 'react';
import styled from 'styled-components';

const BlogImageContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 40px;

  @media screen and (max-width: 768px) {
    margin: 20px -20px 20px -20px;
  }

  img {
    max-width: 100%;
    @media screen and (max-width: 768px) {
      // This is a fix for Safari on iOS so the image display as expected
      width: 100%;
      height: 100%;
    }
  }
`;

type BlogImageProps = {
  src: string;
  alt: string;
};

export const BlogImage: React.FC<BlogImageProps> = ({ src, alt }) => (
  <BlogImageContainer>
    <img src={src} alt={alt} />
  </BlogImageContainer>
);

type BlogBannerImageProps = {
  linkUrl: string;
  dataTrackingId: string;
} & BlogImageProps;

export const BlogBannerImage: React.FC<BlogBannerImageProps> = ({ src, alt, linkUrl, dataTrackingId }) => (
  <BlogImageContainer>
    <a href={linkUrl} target="_blank" rel="noopener noreferrer" data-tracking-id={dataTrackingId || undefined}>
      <img src={src} alt={alt} />
    </a>
  </BlogImageContainer>
);
