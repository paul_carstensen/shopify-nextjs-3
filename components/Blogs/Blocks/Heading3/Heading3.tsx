import styled from 'styled-components';

export const Heading3 = styled.h3`
  margin: 0 0 7.5px;
  font-family: 'Quicksand', sans-serif;
  font-weight: 400;
  letter-spacing: 0em;
  line-height: 1;
  text-transform: uppercase;
  font-size: 1.4em;
  @media only screen and (min-width: 769px) {
    margin: 0 0 15px;
  }
`;
