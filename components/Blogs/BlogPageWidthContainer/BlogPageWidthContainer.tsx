import styled from 'styled-components';
import { PageWidthContainer } from '../../common/PageWidthContainer';

export const BlogPageWidthContainer = styled(PageWidthContainer)`
  padding-top: 75px;
  padding-bottom: 75px;

  @media only screen and (max-width: 768px) {
    padding-top: 40px;
    padding-bottom: 40px;
  }
`;
