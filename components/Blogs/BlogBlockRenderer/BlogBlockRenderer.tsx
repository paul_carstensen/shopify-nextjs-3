import React, { ReactElement } from 'react';
import { CenteredCtaButton } from '../../common/Buttons';
import { Text } from '../Blocks/Text';
import { QuotedText } from '../Blocks/QuotedText';
import { Video } from '../../Video';
import ProductHighlights from '../../products/ProductHighlights';
import { BulletPoints } from '../../BulletPoints';
import { BlogPostItemFromGqlPrefetch } from '../Blocks/BlogPostItem';
import { BlogSlider } from '../Blocks/BlogSlider';
import { Heading2 as H2 } from '../Blocks/Heading2';
import { Heading3 as H3 } from '../Blocks/Heading3';
import { TextLink } from '../../TextLink';
import { BlogBannerImage, BlogImage } from '../Blocks/BlogImage';

type CustomText = {
  text: string;
  bold?: true;
  italic?: true;
  key: string;
};

type ParagraphElement = {
  type: 'paragraph';
  children: CustomText[];
  key: string;
};

type QuoteElement = {
  type: 'block-quote';
  children: ParagraphElement[];
  key: string;
};

type QuoteParagraphElement = {
  type: 'block-quote-paragraph';
  children: CustomText[];
  key: string;
};

type LinkElement = {
  type: 'link';
  url: string;
  children: CustomText[];
  key: string;
};

type ButtonElement = {
  type: 'button';
  url: string;
  trackingId?: string;
  children: CustomText[];
  key: string;
};

type ListElement = {
  type: 'bulleted-list';
  children: ListItemElement[];
  key: string;
};

type ListItemElement = {
  type: 'list-item';
  children: CustomText[];
  key: string;
};

type ImageElement = {
  type: 'image';
  src: string;
  alt?: string;
  children: CustomText[];
  key: string;
};

type BannerImageElement = {
  type: 'banner-image';
  src: string;
  alt: string;
  link: string;
  dataTrackingId: string;
  children: CustomText[];
  key: string;
};

type VideoElement = {
  type: 'video';
  title: string;
  embedId: string;
  children: CustomText[];
  key: string;
};

type FeaturedProductSliderElement = {
  type: 'featured-products';
  title?: string;
  collectionHandle: string;
  products: any[]; // fetched by GraphQL API
  children: CustomText[];
  key: string;
};

type MagazineElement = {
  type: 'magazine';
  blogAndPostSlugs: string[];
  children: CustomText[];
  posts: BlogPostItemFromGqlPrefetch[]; // fetched by GraphQL API
  key: string;
};

export type Heading2 = {
  type: 'h2';
  children: CustomText[];
  key: string;
};

export type Heading3 = {
  type: 'h3';
  children: CustomText[];
  key: string;
};

export type Element =
  | ParagraphElement
  | LinkElement
  | ButtonElement
  | ListElement
  | ListItemElement
  | QuoteElement
  | QuoteParagraphElement
  | ImageElement
  | BannerImageElement
  | VideoElement
  | FeaturedProductSliderElement
  | MagazineElement
  | Heading2
  | Heading3;

const renderChildren = (element: Element): ReactElement[] => {
  switch (element.type) {
    case 'bulleted-list':
    case 'block-quote':
      return element.children.map((child) => <BlogBlockRenderer key={child.key} element={child} />); // eslint-disable-line @typescript-eslint/no-use-before-define
    default:
      return element.children?.map((child) => <BlogBlockLeafRenderer key={child?.key} leaf={child} />); // eslint-disable-line @typescript-eslint/no-use-before-define
  }
};

interface BlogBlockLeafRendererProps {
  leaf: CustomText | LinkElement;
}

const isExternalUrl = (url: string): boolean => url.startsWith('http') && !url.includes('uptodate.de');

const BlogBlockLeafRenderer: React.FC<BlogBlockLeafRendererProps> = ({ leaf }) => {
  if ('type' in leaf) {
    return (
      <TextLink href={leaf.url} {...(isExternalUrl(leaf.url) ? { target: '_blank', rel: 'noopener noreferrer' } : {})}>
        {renderChildren(leaf)}
      </TextLink>
    );
  }
  if (leaf.bold && leaf.italic) {
    return (
      <strong>
        <em>{leaf.text}</em>
      </strong>
    );
  }
  if (leaf.bold) {
    return <strong>{leaf.text}</strong>;
  }
  if (leaf.italic) {
    return <em>{leaf.text}</em>;
  }
  return <>{leaf.text}</>;
};

interface BlogBlockRendererProps {
  element: Element;
}

export const BlogBlockRenderer: React.FC<BlogBlockRendererProps> = ({ element }) => {
  switch (element.type) {
    case 'paragraph':
      return <Text>{renderChildren(element)}</Text>;
    case 'h2':
      return <H2>{renderChildren(element)}</H2>;
    case 'h3':
      return <H3>{renderChildren(element)}</H3>;
    case 'image':
      return <BlogImage src={element.src} alt={element.alt ?? ''} />;
    case 'banner-image':
      return (
        <BlogBannerImage
          src={element.src}
          alt={element.alt}
          linkUrl={element.link}
          dataTrackingId={element.dataTrackingId}
        />
      );
    case 'link':
      return <BlogBlockLeafRenderer leaf={element} />;
    case 'button':
      return (
        <CenteredCtaButton role="button" href={element.url} data-tracking-id={element.trackingId}>
          {renderChildren(element)}
        </CenteredCtaButton>
      );
    case 'bulleted-list':
      return (
        <BulletPoints>
          <ul>{renderChildren(element)}</ul>
        </BulletPoints>
      );
    case 'list-item':
      return <li>{renderChildren(element)}</li>;
    case 'block-quote':
      return <QuotedText>{renderChildren(element)}</QuotedText>;
    case 'video':
      return <Video embedId={element.embedId} title={element.title} identifier={element.embedId} />;
    case 'featured-products':
      return (
        <ProductHighlights
          title={element.title}
          products={element.products}
          collectionHandle={element.collectionHandle}
        />
      );
    case 'magazine':
      return <BlogSlider sliderTitle="Magazine" blogAndPostSlugs={element.blogAndPostSlugs} posts={element.posts} />;
    default:
      return <div>{renderChildren(element)}</div>;
  }
};
