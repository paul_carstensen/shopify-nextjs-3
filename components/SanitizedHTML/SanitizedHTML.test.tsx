/** @jest-environment jsdom */
import React from 'react';
import { render } from '@testing-library/react';
import { SanitizedHTML } from './SanitizedHTML';

describe('Validation/SanitizedHTML', () => {
  it('should sanitize innerHTML', () => {
    const { container } = render(
      <SanitizedHTML innerHTML="this should not render an iframe<iframe src='javascript:alert(1)'></iframe>" />
    );

    expect(container.innerHTML).toEqual('<div>this should not render an iframe</div>');
  });

  it('should sanitize innerHTML and render as span', () => {
    const { container } = render(
      <SanitizedHTML
        innerHTML="this should not render an iframe<iframe src='javascript:alert(1)'></iframe>"
        as="span"
      />
    );

    expect(container.innerHTML).toEqual('<span>this should not render an iframe</span>');
  });
});
