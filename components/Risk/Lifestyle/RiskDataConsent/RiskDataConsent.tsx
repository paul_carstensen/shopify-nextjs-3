import React, { useState } from 'react';
import styled from 'styled-components';
import { Button } from '@u2dv/marketplace_ui_kit/dist/components';
import { SanitizedHTML } from '../../../SanitizedHTML';
import { PolicyModal } from '../../../PolicyModal';
import { FormErrorText } from '../../../FormErrorText';
import { KeepMeUpdatedBody } from '../../../../types/risk/lifestyle';
import { keepMeUpdated } from '../../../../helpers/lifeStyle/api';

type RiskDataConsentProps = {
  title: string;
  policyTextHtml: string;
  riskData: KeepMeUpdatedBody;
  setRiskData: (riskData: KeepMeUpdatedBody) => void;
  hasConsent: boolean;
};

const RiskDataConsentContainer = styled.div`
  max-width: 400px;
  width: 100%;
  margin: 0 auto;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-family: Poppins;
  align-items: center;
  padding: 0px 44px;
`;

const Title = styled.div`
  font-size: 20px;
  line-height: 24px;
  font-weight: 600;
  text-align: center;
  padding-bottom: 15px;
  font-weight: bold;
`;

const SubTitle = styled.div`
  text-align: center;
  font-size: 14px;
  padding-bottom: 10px;
`;

const ConsentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 25px;
  margin-bottom: 18px;
  padding-top: 25px;
`;

const ConsentComponent = styled.div`
  grid-area: consent;
  & label div {
    display: inline-block;
    width: 200px;
    vertical-align: text-top;
    letter-spacing: 0em;
    text-transform: none;
    overflow: visible;
    text-overflow: ellipsis;
    white-space: normal;
    text-align: left;
  }
  & label div :first-child {
    display: inline;
  }
  & label div :not(:first-child) {
    display: none;
  }
  & button {
    margin: -8px 0 10px 0px;
    position: absolute;
    font-weight: bold;
    font-size: 12px;
  }
`;

const Label = styled.label`
  text-transform: uppercase;
  letter-spacing: 0.3em;
  font-size: 0.75em;
  display: block;
  margin-bottom: 10px;

  &[for] {
    cursor: pointer;
  }

  &.error {
    color: #d02e2e;
  }
`;

const InputField = styled.input`
  border: 1px solid #e6e6e6;
  max-width: 100%;
  padding: 8px 10px;
  margin-bottom: 25px;
  border-radius: 25px;
  border: none;
  box-shadow: 5px 10px 30px 0 rgba(178, 178, 178, 0.25);
  font-family: 'Quicksand';
  color: #19232d;
  font-size: 12px;

  &.error {
    border-color: #d02e2e;
    background-color: #fff6f6;
    color: #d02e2e;
  }
`;

const UpdatedButton = styled(Button)<{ disabled: boolean }>`
  cursor: ${(props) => (props.disabled ? 'default' : 'pointer')};
  pointer-events: ${(props) => (props.disabled ? 'none' : 'all')};
`;

export const RiskDataConsent: React.FC<RiskDataConsentProps> = ({ title, policyTextHtml, riskData, hasConsent }) => {
  const [email, setEmail] = useState('');
  const [hasPolicyConsent, setHasPolicyConsent] = useState(false);
  const [policyModalOpen, setPolicyModalOpen] = useState(false);
  const [emailValidation, setEmailValidation] = useState('');

  const submitRiskData = async (data: KeepMeUpdatedBody) => {
    await keepMeUpdated(data);
  };

  const onButtonClick = () => {
    if (hasConsent) {
      const riskDataUser = riskData;
      const riskDataJson: KeepMeUpdatedBody = riskDataUser;
      if (hasPolicyConsent) {
        if (email && emailValidation.length === 0) {
          const updatedRiskDataJson = {
            ...riskDataJson,
            email,
          };
          submitRiskData(updatedRiskDataJson);
        } else if (email.length === 0) {
          submitRiskData(riskDataJson);
        }
      }
    }
  };

  const onBlur = () => {
    const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (emailPattern.test(email) || email.length === 0) {
      setEmailValidation('');
    } else {
      setEmailValidation('Ungültige E-Mail-Adresse');
    }
  };

  return (
    <RiskDataConsentContainer>
      <Title>{title}</Title>
      <SubTitle>
        Tut uns leid! Um fundierte Empfehlungen für ein besseres Leben geben zu können, arbeiten wir jeden Lebensstil
        gründlich aus. Dieser Lebensstil ist leider noch in Arbeit.
      </SubTitle>
      <SubTitle>Wir geben Dir gerne per Email Bescheid, sobald wir Dir Empfehlungen geben können.</SubTitle>

      <ConsentWrapper>
        <PolicyModal
          isOpen={policyModalOpen}
          closeModal={() => setPolicyModalOpen(false)}
          policyTextHtml={policyTextHtml}
        />
        <InputField
          id="email"
          name="email"
          type="text"
          placeholder="Email"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          onBlur={onBlur}
        />
        {emailValidation && <FormErrorText>{emailValidation}</FormErrorText>}
        <ConsentComponent>
          <Label htmlFor="consent">
            <input
              id="consent"
              type="checkbox"
              name="consent"
              onChange={(e) => setHasPolicyConsent(e.target.checked)}
            />
            <SanitizedHTML innerHTML={policyTextHtml} />
          </Label>
          <button type="button" onClick={() => setPolicyModalOpen(true)}>
            Mehr anzeigen
          </button>
        </ConsentComponent>
      </ConsentWrapper>
      <UpdatedButton
        trackingId="turbulanz-risktip-subscribe"
        appearance="primary"
        onClick={onButtonClick}
        disabled={!hasPolicyConsent && emailValidation.length > 0}
        roundedBorder
      >
        bleib uptodate
      </UpdatedButton>
    </RiskDataConsentContainer>
  );
};
