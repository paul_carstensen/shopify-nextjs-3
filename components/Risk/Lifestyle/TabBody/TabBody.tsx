import React, { useState } from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import { Button, Slider } from '@u2dv/marketplace_ui_kit/dist/components';
import IncomeImage from '../../../../assets/images/lifestyles/income.webp';
import GroupImage from '../../../../assets/images/lifestyles/group.webp';
import { Lifestyle, KeepMeUpdatedBody } from '../../../../types/risk/lifestyle';

type TabBodyProps = {
  lifestyle: Lifestyle | undefined;
  riskData: KeepMeUpdatedBody;
  setRiskData: (riskData: KeepMeUpdatedBody) => void;
};

const TabBodyWrapper = styled.div`
  margin: 12px 0;
  #slide-0,
  #slide-1,
  #slide-2 {
    width: 85% !important;
  }
`;

const CoreGroupWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  margin-bottom: 29px;
`;

const CoreGroup = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  img {
    height: 40px !important;
    width: 40px !important;
  }
`;

const CoreGroupAmountWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  margin-right: 12px;
  font-weight: bold;
`;

const CoreGroupAmount = styled.span`
  font-size: 24px;
  line-height: normal;
  margin-right: 8px;
`;

const CoreGroupDataWrap = styled.span`
  display: flex;
  flex-direction: column;
  margin-left: 12px;
`;

const CoreGroupAgeText = styled.span`
  font-size: 12px;
  font-weight: 600;
  font-family: QuickSand;
`;

const SliderItemTitle = styled.span`
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 10px;
`;

const SliderItemDescription = styled.span`
  font-size: 14px;
  font-family: Quicksand;
`;

const Card = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 8px;
  padding: 30px 15px;
  box-shadow: 5px 10px 30px 0 rgba(178, 178, 178, 0.25);
  margin: 10px;
  margin-bottom: 29px;
`;

const ExplanantionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 25px;
  margin-bottom: 18px;
  padding-top: 25px;
`;

const ExplanationTitle = styled.span`
  font-size: 14px;
  font-weight: bold;
  text-align: left;
`;

const ExplanationDescription = styled.span`
  font-size: 14px;
  font-family: Quicksand;
  text-align: left;
`;

const ShowMore = styled.span`
  font-size: 12px;
  font-weight: bold;
  cursor: pointer;
  font-family: Poppins;
`;

export const TabBody: React.FC<TabBodyProps> = ({ lifestyle, riskData, setRiskData }) => {
  const [showMore, setShowMore] = useState(false);
  if (!lifestyle) {
    return null;
  }

  const onButtonClick = () => {
    const selectedLifestyleKey = lifestyle.lifestyleKey;
    const riskDataUser = riskData;
    const riskDataJson: KeepMeUpdatedBody = riskDataUser;
    const updatedRiskDataJson = {
      ...riskDataJson,
      selectedLifestyleKey,
    };
    setRiskData(updatedRiskDataJson);
  };

  return (
    <TabBodyWrapper>
      <CoreGroupWrapper>
        <CoreGroup>
          <Image src={GroupImage} alt="group-icon" />
          <CoreGroupDataWrap>
            <CoreGroupAmountWrapper>
              <CoreGroupAmount>{`${lifestyle.kpiCoregroupAmount}`.replace('.', ',')}</CoreGroupAmount>
              <span>Mio.</span>
            </CoreGroupAmountWrapper>
            <CoreGroupAgeText>in DE (Ø{lifestyle.kpiCoregroupAge} J.)</CoreGroupAgeText>
          </CoreGroupDataWrap>
        </CoreGroup>
        <CoreGroup>
          <Image src={IncomeImage} alt="income-icon" />
          <CoreGroupDataWrap>
            <CoreGroupAmountWrapper>
              <CoreGroupAmount>
                {`${lifestyle.kpiIncome}`.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
              </CoreGroupAmount>
              <span>€</span>
            </CoreGroupAmountWrapper>
            <CoreGroupAgeText>
              Ø Einkommen
              <br />
            </CoreGroupAgeText>
          </CoreGroupDataWrap>
        </CoreGroup>
      </CoreGroupWrapper>
      <Slider
        visibleSlidesBreakpoints={[
          { breakpoint: '(max-width: 560px)', value: 1 },
          { breakpoint: '(min-width: 561px)', value: 1 },
        ]}
        enableArrows={false}
        enableMouseWheel
      >
        <Card>
          <SliderItemTitle>Grundüberzeugung</SliderItemTitle>
          <SliderItemDescription>“{lifestyle.statementBeliefs}„</SliderItemDescription>
        </Card>
        <Card>
          <SliderItemTitle>Lebensmotto im Alltag</SliderItemTitle>
          <SliderItemDescription>“{lifestyle.statementMotto}„</SliderItemDescription>
        </Card>
        <Card>
          <SliderItemTitle>Lebensziel</SliderItemTitle>
          <SliderItemDescription>“{lifestyle.statementLifegoal}„</SliderItemDescription>
        </Card>
      </Slider>
      <ExplanantionWrapper>
        <ExplanationTitle>Was macht diesen Lebensstil aus?:</ExplanationTitle>
        <ExplanationDescription>
          {showMore ? lifestyle.description : `${lifestyle.description.substring(0, 280)}`}
          {!showMore ? '...' : ''}
          <ShowMore data-tracking-id="turbulanz-picklifestyle-more" onClick={() => setShowMore(!showMore)}>
            {showMore ? 'Weniger' : 'Mehr'}
          </ShowMore>
        </ExplanationDescription>
      </ExplanantionWrapper>
      <Button
        trackingId={`turbulanz-picklifestyle-confirm-${lifestyle.lifestyleKey}`}
        appearance="primary"
        onClick={onButtonClick}
        roundedBorder
      >
        Passt am besten zu mir
      </Button>
    </TabBodyWrapper>
  );
};
