import React from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import lifestyleImages from '../lifestyleImages';

import { Lifestyle } from '../../../../types/risk/lifestyle';

type TabProps = {
  onClick: () => void;
  item: Lifestyle;
  selected: boolean;
  firstTab: boolean;
  lastTab: boolean;
  dataTrackingId?: string;
  dataTestId?: string;
};

type TabWrapperProps = {
  selected: boolean;
  firstTab: boolean;
  lastTab: boolean;
};

const TabWrapper = styled.div<TabWrapperProps>`
  display: flex;
  flex-direction: column;
  flex: 1;
  height: fit-content;
  padding: 4px;
  border-top-left-radius: ${(props) => (props.firstTab || props.selected ? '8px' : '0')};
  border-top-right-radius: ${(props) => (props.lastTab || props.selected ? '8px' : '0')};
  background-color: ${(props) => (props.selected ? '#c4d6e6' : '#f7f7f7')};
  color: #19232d;
  cursor: pointer;

  img {
    min-width: ${(props) => (props.selected ? '60px !important' : '35px !important')};
    min-height: ${(props) => (props.selected ? '60px !important' : '35px !important')};
    margin-top: ${(props) => (props.selected ? '7px !important' : '0px')};
  }
`;

const TabTitle = styled.span`
  text-align: center;
  font-family: Poppins;
  font-size: 12px;
  font-weight: bold;
  min-height: 38px;
`;

export const Tab: React.FC<TabProps> = ({ item, firstTab, lastTab, selected, onClick, dataTrackingId, dataTestId }) => (
  <TabWrapper
    data-tracking-id={dataTrackingId}
    firstTab={firstTab}
    lastTab={lastTab}
    selected={selected}
    onClick={onClick}
    data-testid={dataTestId}
  >
    <Image
      src={lifestyleImages[item.lifestyleKey]}
      width={selected ? 50 : 40}
      height={selected ? 50 : 40}
      alt={item.title}
    />
    <TabTitle>{item.title}</TabTitle>
  </TabWrapper>
);
