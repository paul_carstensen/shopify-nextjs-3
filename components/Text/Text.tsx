import React from 'react';
import styled from 'styled-components';
import { PageWidthContainer } from '../common/PageWidthContainer';

const TextInner = styled.div<{ textAlign?: string }>`
  @media screen and (min-width: 769px) {
    margin: auto;
    width: 80%;
    ${(props) => `text-align: ${props.textAlign || 'center'};`}
  }
`;

export interface TextProps {
  textAlign?: string;
  children: React.ReactNode;
}

export const Text: React.FC<TextProps> = ({ children, textAlign }: TextProps) => (
  <PageWidthContainer>
    <TextInner textAlign={textAlign}>{children}</TextInner>
  </PageWidthContainer>
);
