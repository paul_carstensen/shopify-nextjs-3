/** @jest-environment jsdom */
import { render } from '@testing-library/react';
import { Text, TextProps } from '.';

const renderText = ({ children }: TextProps) => render(<Text>{children}</Text>);

describe('Text component', () => {
  const text = 'This text needs to be tested.';

  it('should render without crashing', () => {
    expect(() => {
      renderText({ children: text });
    }).not.toThrowError();
  });

  it('should render the text', () => {
    const textBar = renderText({ children: text });

    expect(textBar.getByText(text)).toBeInTheDocument();
  });
});
