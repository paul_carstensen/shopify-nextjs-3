/** @jest-environment jsdom */
import { render, within } from '@testing-library/react';
import { SharingBar, SharingBarProps } from '.';

jest.mock('next/image', () => ({
  __esModule: true,
  // eslint-disable-next-line react/display-name
  default: (props: any) => {
    const Image = jest.requireActual('next/image').default;

    return <Image alt="Dummy" {...props} src="/dummy.png" />;
  },
}));

const renderSharingBar = ({ message, urls, emailSubject, emailBody, ctaText }: SharingBarProps) =>
  render(
    <SharingBar message={message} urls={urls} emailBody={emailBody} emailSubject={emailSubject} ctaText={ctaText} />
  );

const assertTargetBlank = (link: HTMLAnchorElement) => {
  expect(link.target).toBe('_blank');
  expect(link.rel).toBe('noreferrer');
};

describe('Sharing bar', () => {
  const url = 'http://www.milliondollarhomepage.com';
  const message = 'Own a piece of Internet history! {url}';
  const emailSubject = 'The million dollar homepage';
  const emailBody =
    'The Million Dollar Homepage - 1,000,000 pixels - $1 per pixel - Own a piece of internet history!\n{url}';

  it('should render without crashing', () => {
    expect(() => {
      renderSharingBar({ urls: url, message, emailSubject, emailBody });
    }).not.toThrowError();
  });

  it('should render when we pass multiple links', () => {
    const multipleUrls = 'http://www.milliondollarhomepage.com,http://www.billiondollarhomepage.com';
    const messageWithMultipleLinks = 'Own a piece of Internet history! {url}, {url1}';

    const sharingBar = renderSharingBar({ urls: multipleUrls, message: messageWithMultipleLinks, emailSubject });
    const [emailLink, whatsAppLink, facebookLink, twitterLink] = sharingBar.getAllByRole('link') as HTMLAnchorElement[];

    expect(emailLink.href).toMatchInlineSnapshot(
      '"mailto:?subject=The%20million%20dollar%20homepage&body=Own%20a%20piece%20of%20Internet%20history!%20http%3A%2F%2Fwww.milliondollarhomepage.com%2F%3Futm_source%3Demail%2C%20http%3A%2F%2Fwww.billiondollarhomepage.com%2F%3Futm_source%3Demail"'
    );
    expect(within(emailLink).getByAltText('Per E-Mail teilen')).toBeTruthy();

    expect(whatsAppLink.href).toMatchInlineSnapshot(
      '"https://api.whatsapp.com/send?text=Own%20a%20piece%20of%20Internet%20history!%20http%3A%2F%2Fwww.milliondollarhomepage.com%2F%3Futm_source%3Dwhatsapp%2C%20http%3A%2F%2Fwww.billiondollarhomepage.com%2F%3Futm_source%3Dwhatsapp"'
    );
    expect(within(whatsAppLink).getByAltText('Per WhatsApp teilen')).toBeTruthy();
    assertTargetBlank(whatsAppLink);

    expect(facebookLink.href).toMatchInlineSnapshot(
      '"https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.milliondollarhomepage.com%2F%3Futm_source%3Dfacebook&quote=Own%20a%20piece%20of%20Internet%20history!%20http%3A%2F%2Fwww.milliondollarhomepage.com%2F%3Futm_source%3Dfacebook%2C%20http%3A%2F%2Fwww.billiondollarhomepage.com%2F%3Futm_source%3Dfacebook"'
    );
    expect(within(facebookLink).getByAltText('Auf Facebook teilen')).toBeTruthy();
    assertTargetBlank(facebookLink);

    expect(twitterLink.href).toMatchInlineSnapshot(
      '"https://twitter.com/intent/tweet?text=Own%20a%20piece%20of%20Internet%20history!%20http%3A%2F%2Fwww.milliondollarhomepage.com%2F%3Futm_source%3Dtwitter%2C%20http%3A%2F%2Fwww.billiondollarhomepage.com%2F%3Futm_source%3Dtwitter"'
    );
    expect(within(twitterLink).getByAltText('Auf Twitter teilen')).toBeTruthy();
    assertTargetBlank(twitterLink);
  });

  it('should render the expected sharing links', () => {
    const sharingBar = renderSharingBar({ urls: url, message, emailSubject, emailBody });
    const [emailLink, whatsAppLink, facebookLink, twitterLink] = sharingBar.getAllByRole('link') as HTMLAnchorElement[];

    expect(emailLink.href).toMatchInlineSnapshot(
      '"mailto:?subject=The%20million%20dollar%20homepage&body=The%20Million%20Dollar%20Homepage%20-%201%2C000%2C000%20pixels%20-%20%241%20per%20pixel%20-%20Own%20a%20piece%20of%20internet%20history!%0Ahttp%3A%2F%2Fwww.milliondollarhomepage.com%2F%3Futm_source%3Demail"'
    );
    expect(within(emailLink).getByAltText('Per E-Mail teilen')).toBeTruthy();

    expect(whatsAppLink.href).toMatchInlineSnapshot(
      '"https://api.whatsapp.com/send?text=Own%20a%20piece%20of%20Internet%20history!%20http%3A%2F%2Fwww.milliondollarhomepage.com%2F%3Futm_source%3Dwhatsapp"'
    );
    expect(within(whatsAppLink).getByAltText('Per WhatsApp teilen')).toBeTruthy();
    assertTargetBlank(whatsAppLink);

    expect(facebookLink.href).toMatchInlineSnapshot(
      '"https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.milliondollarhomepage.com%2F%3Futm_source%3Dfacebook&quote=Own%20a%20piece%20of%20Internet%20history!%20http%3A%2F%2Fwww.milliondollarhomepage.com%2F%3Futm_source%3Dfacebook"'
    );
    expect(within(facebookLink).getByAltText('Auf Facebook teilen')).toBeTruthy();
    assertTargetBlank(facebookLink);

    expect(twitterLink.href).toMatchInlineSnapshot(
      '"https://twitter.com/intent/tweet?text=Own%20a%20piece%20of%20Internet%20history!%20http%3A%2F%2Fwww.milliondollarhomepage.com%2F%3Futm_source%3Dtwitter"'
    );
    expect(within(twitterLink).getByAltText('Auf Twitter teilen')).toBeTruthy();
    assertTargetBlank(twitterLink);
  });

  it('falls back to the given message if no email body is provided', () => {
    const sharingBar = renderSharingBar({ urls: url, message, emailSubject });
    const [emailLink] = sharingBar.getAllByRole('link') as HTMLAnchorElement[];

    expect(emailLink.href).toMatchInlineSnapshot(
      '"mailto:?subject=The%20million%20dollar%20homepage&body=Own%20a%20piece%20of%20Internet%20history!%20http%3A%2F%2Fwww.milliondollarhomepage.com%2F%3Futm_source%3Demail"'
    );
  });

  it('should render the CTA text if provided', () => {
    const sharingBar = renderSharingBar({ urls: url, message, emailSubject, emailBody, ctaText: 'Sharing is caring!' });

    expect(sharingBar.getByText('Sharing is caring!')).toBeTruthy();
  });
});
