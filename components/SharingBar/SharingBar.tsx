/**
 * Sharing bar component.
 *
 * This component renders a number of sharing links in a horizontal row.
 *
 * Usage notes:
 *   - The provided URL must be absolute.
 *   - If you want the shared message to include the URL, then you must include
 *     the URL in the message string using the `{url}` placeholder. This is necessary because most platforms do
 *     not support specifying a URL explicitly.
 *   - The URL and the `{url}` placeholders will be appended with the `utm_source` query parameter.
 *   - URLs that are already in the message will not be appended with any query parameter.
 *
 */
import React from 'react';
import Image from 'next/image';
import styled from 'styled-components';
import { sharingBarImageByVariant, StaticImageData } from './sharingBarImagesByVariant';
import { PageWidthContainer } from '../common/PageWidthContainer';
import { HeaderAnchorOffset } from '../common/HeaderAnchorOffset';

/**
 * To pass multiple urls, pass it as a string separated with commas:
 * Ex: 'https://url1.com,https://url2.com'
 *
 * In the facebook scenario the main url will be the first one.
 */

const formatURL = (url: string, utmSource: string): string => {
  try {
    const urlObject = new URL(url);
    urlObject.searchParams.set('utm_source', utmSource);
    return urlObject.toString();
  } catch {
    return url;
  }
};

const injectURLs = (message: string, urls: string, utmSource: string) => {
  let result = message;

  urls.split(',').forEach((url, i) => {
    const keyToBeReplaced = `{url${i || ''}}`;
    const regexp = new RegExp(keyToBeReplaced, 'g');

    result = result.replace(regexp, formatURL(url, utmSource));
  });

  return result;
};

const getEmailShareLink = (subject: string, message: string, urls: string): string =>
  `mailto:?subject=${encodeURIComponent(subject)}&body=${encodeURIComponent(injectURLs(message, urls, 'email'))}`;

const getWhatsAppShareLink = (message: string, urls: string): string =>
  `https://api.whatsapp.com/send?text=${encodeURIComponent(injectURLs(message, urls, 'whatsapp'))}`;

const getFacebookShareLink = (message: string, mainUrl: string, urls: string): string => {
  const utmUrl = formatURL(mainUrl, 'facebook');
  return `https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(utmUrl)}&quote=${encodeURIComponent(
    injectURLs(message, urls, 'facebook')
  )}`;
};

const getTwitterShareLink = (message: string, urls: string): string =>
  `https://twitter.com/intent/tweet?text=${encodeURIComponent(injectURLs(message, urls, 'twitter'))}`;

const ServiceLink = styled.a`
  margin-right: 8px;
  width: 30px;
  height: 30px;

  &:last-child {
    margin-right: 0;
  }
`;

type SharingBarItemProps = {
  url: string;
  image: StaticImageData;
  imageAltText: string;
  openNewWindow?: boolean;
  trackingId?: string;
};

const SharingBarItem: React.FC<SharingBarItemProps> = ({
  url,
  image,
  imageAltText,
  openNewWindow = false,
  trackingId,
}) => (
  <ServiceLink href={url} data-tracking-id={trackingId} {...(openNewWindow && { target: '_blank', rel: 'noreferrer' })}>
    <Image src={image} alt={imageAltText} width="30" height="30" />
  </ServiceLink>
);

const Wrapper = styled.div`
  display: inline-block;
  text-align: center;
`;

const Items = styled.div`
  display: flex;
`;

const SmallText = styled.span`
  text-transform: uppercase;
  letter-spacing: 3px;
  font-size: 10px;
`;

type CtaTextProps = {
  variant?: 'white';
};

const CtaText = styled(SmallText)<CtaTextProps>`
  padding-top: 9px;
  color: ${(props) => (props.variant === 'white' ? 'white' : 'inherit')};
`;

export type SharingBarProps = {
  message: string;
  urls: string;
  emailSubject: string;
  emailBody?: string;
  ctaText?: string;
  ctaTextTop?: string;
  emailIconTrackingId?: string;
  whatsAppIconTrackingId?: string;
  facebookIconTrackingId?: string;
  twitterIconTrackingId?: string;
  variant?: 'white';
  id?: string;
};

export const SharingBar: React.FC<SharingBarProps> = ({
  message,
  urls,
  emailSubject,
  emailBody,
  ctaText,
  ctaTextTop,

  emailIconTrackingId,
  whatsAppIconTrackingId,
  facebookIconTrackingId,
  twitterIconTrackingId,
  variant,
  id,
}) => {
  const images = sharingBarImageByVariant(variant);
  return (
    <HeaderAnchorOffset id={id ?? 'sharing_bar'}>
      <PageWidthContainer>
        <Wrapper>
          {ctaTextTop && <CtaText variant={variant}>{ctaTextTop}</CtaText>}
          <Items>
            <SharingBarItem
              url={getEmailShareLink(emailSubject, emailBody ?? message, urls)}
              image={images.emailImage}
              imageAltText="Per E-Mail teilen"
              trackingId={emailIconTrackingId}
            />
            <SharingBarItem
              url={getWhatsAppShareLink(message, urls)}
              image={images.whatsAppImage}
              imageAltText="Per WhatsApp teilen"
              openNewWindow
              trackingId={whatsAppIconTrackingId}
            />
            <SharingBarItem
              url={getFacebookShareLink(message, urls.split(',')[0], urls)}
              image={images.facebookImage}
              imageAltText="Auf Facebook teilen"
              openNewWindow
              trackingId={facebookIconTrackingId}
            />
            <SharingBarItem
              url={getTwitterShareLink(message, urls)}
              image={images.twitterImage}
              imageAltText="Auf Twitter teilen"
              openNewWindow
              trackingId={twitterIconTrackingId}
            />
          </Items>

          {ctaText && <CtaText variant={variant}>{ctaText}</CtaText>}
        </Wrapper>
      </PageWidthContainer>
    </HeaderAnchorOffset>
  );
};
