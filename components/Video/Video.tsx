import React from 'react';
import ReactPlayer from 'react-player';
import styled from 'styled-components';
import { ConsentWrapper } from '../ConsentWrapper/ConsentWrapper';

const VideoContainer = styled.div`
  padding-top: 35px;
  padding-bottom: 35px;
  background-color: white;
  .player {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 0;
    height: 0 !important;
    overflow: hidden;
    iframe,
    object,
    embed {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
  }
`;

const Title = styled.div`
  font-size: 32px;
  font-weight: 500;
  text-align: center;
  @media screen and (max-width: 768px) {
    font-size: 24px;
    line-height: normal;
  }
`;

export interface VideoProps {
  title?: string;
  embedId: string;
  identifier: string;
}

export const Video: React.FC<VideoProps> = (props: VideoProps) => {
  const { title, embedId, identifier } = props;

  return (
    <div id={identifier}>
      <VideoContainer data-tracking-id="ga-blog-video-click">
        <Title>{title}</Title>
        {embedId && (
          <ConsentWrapper key={embedId}>
            <ReactPlayer
              className="player"
              width="100%"
              height="100%"
              url={`https://www.youtube.com/watch?v=${embedId}-U`}
            />
          </ConsentWrapper>
        )}
      </VideoContainer>
    </div>
  );
};
