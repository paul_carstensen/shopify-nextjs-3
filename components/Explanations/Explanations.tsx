import React from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import { colors } from '@u2dv/marketplace_ui_kit/dist/styles';
import iconClose from '../../assets/images/homepage/closeIcon.png';

export type Explanation = {
  title?: string;
  description: React.ReactNode;
};

export type ExplanationsProps = {
  explanations: Explanation[];
  onClose: () => void;
};

const ExplanationContainer = styled.div`
  padding: 40px;
  background-color: ${colors.lightLilac};
  text-align: center;
`;

const ExplanationTitle = styled.p`
  font-family: Poppins;
  font-size: 24px;
  font-weight: bold;
`;

const ExplanationDescription = styled.p`
  text-align: center;
  margin-bottom: 60px;
`;

const ExplanationButtonClose = styled.button`
  position: relative;
  top: -10px;
  right: -45%;
`;

export const Explanations: React.FC<ExplanationsProps> = ({ explanations, onClose }: ExplanationsProps) => (
  <ExplanationContainer>
    <ExplanationButtonClose type="button" onClick={onClose}>
      <Image src={iconClose} alt="Schließen-Symbol" width="28" height="28" />
    </ExplanationButtonClose>
    {explanations.map(({ title, description }: Explanation) => (
      <div key={`explanation-${title}`}>
        <ExplanationTitle>{title}</ExplanationTitle>
        <ExplanationDescription>{description}</ExplanationDescription>
      </div>
    ))}
  </ExplanationContainer>
);
