import React from 'react';
import { NavigationItem } from '../../../../../types';

type SubMegaMenuItemProps = {
  link: NavigationItem;
};

const SubMegaMenuItem: React.FC<SubMegaMenuItemProps> = ({ link }) => (
  <div>
    <a href={link.url} className="site-nav__dropdown-link">
      {link.title}
    </a>
  </div>
);

type MegaMenuItemProps = {
  link: NavigationItem;
  delay: number;
};

export const MegaMenuItem: React.FC<MegaMenuItemProps> = ({ link, delay }) => (
  <div className={`grid__item medium-up--one-fifth appear-animation appear-delay-${delay}`} key={link.title}>
    <div className="h5">
      <a href={link.url} className="site-nav__dropdown-link site-nav__dropdown-link--top-level">
        {link.title}
      </a>
    </div>
    {link.items.map((subLink) => (
      <SubMegaMenuItem link={subLink} key={`${link.title}-${subLink.title}`} />
    ))}
  </div>
);
