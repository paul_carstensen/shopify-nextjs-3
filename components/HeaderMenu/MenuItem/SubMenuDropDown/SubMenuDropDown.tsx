import React from 'react';
import { NavigationItem } from '../../../../types';

type DropDownItemProps = {
  link: NavigationItem;
};

const DropDownItem: React.FC<DropDownItemProps> = ({ link }) => (
  <li>
    <a href={link.url} className="site-nav__dropdown-link site-nav__dropdown-link--second-level">
      {link.title}
    </a>
  </li>
);

type SubMenuDropDownProps = {
  links: NavigationItem[];
};

export const SubMenuDropDown: React.FC<SubMenuDropDownProps> = ({ links }) => (
  <ul className="site-nav__dropdown text-left">
    {links.map((link) => (
      <DropDownItem link={link} key={`header-menu-dropdown-${link.title}`} />
    ))}
  </ul>
);
