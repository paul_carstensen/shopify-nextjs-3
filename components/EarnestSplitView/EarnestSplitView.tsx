import React from 'react';
import Image from 'next/image';
import styled from 'styled-components';
import earnestIconImage from '../../assets/images/earnest/earnest-icon.png';
import appStoreImage from '../../assets/images/earnest/app-store.png';
import playStoreImage from '../../assets/images/earnest/play-store.png';
import { PageWidthContainer } from '../common/PageWidthContainer';
import { HeaderAnchorOffset } from '../common/HeaderAnchorOffset';

interface EarnestSplitViewContainerProps {
  imagePosition: 'left' | 'right';
}
interface ImageWrapperProps {
  backgroundImageUrl: string;
}
interface SubTitleProps {
  paddingLeft?: string;
}

const EarnestSplitViewContainer = styled.div<EarnestSplitViewContainerProps>`
  display: flex;
  align-items: center;
  min-height: 260px;
  flex-direction: ${(props) => (props.imagePosition === 'right' ? 'row' : 'row-reverse')};
  background-color: #e5e6e5;
  font-family: Quicksand;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  @media screen and (max-width: 768px) {
    flex-direction: column;
    background-color: #c4d6e6;
    margin: 0px -20px;
  }
`;

const Content = styled.div`
  padding: 0px 40px;
  flex: 1;
  @media screen and (max-width: 768px) {
    min-height: 415px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
  }
`;

const ImageWrapper = styled.div<ImageWrapperProps>`
  flex: 1;
  background-image: url(${(props) => props.backgroundImageUrl});
  background-size: cover;
  background-position: center;
  min-height: 260px;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  @media screen and (max-width: 768px) {
    min-height: 585px;
  }
`;

const Title = styled.div`
  font-size: 22px;
  line-height: 24px;
  font-weight: 600;
  text-align: left;
  padding-bottom: 15px;
`;

const SubTitle = styled.div<SubTitleProps>`
  text-align: left;
  font-size: 14px;
  padding-left: ${(props) => props.paddingLeft || '0px'};
`;

const EarnestIconWrap = styled.div`
  color: #fff;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StoreIconWrap = styled.div`
  padding-top: 7px;
`;

const StoreLink = styled.a`
  &:first-child {
    padding-right: 20px;
  }
`;
export interface EarnestSplitViewProps {
  title: string;
  subtitle: string;
  earnestText?: string;
  appStoreCtaUrl?: string;
  playStoreCtaUrl?: string;
  imagePosition?: 'left' | 'right';
  backgroundImageUrl: string;
}

export const EarnestSplitView: React.FC<EarnestSplitViewProps> = (props: EarnestSplitViewProps) => {
  const { title, subtitle, earnestText, appStoreCtaUrl, playStoreCtaUrl, imagePosition, backgroundImageUrl } = props;

  return (
    <HeaderAnchorOffset id="earnest_split_view">
      <PageWidthContainer>
        <EarnestSplitViewContainer imagePosition={imagePosition || 'right'}>
          <Content>
            <Title>{title}</Title>
            <SubTitle>{subtitle}</SubTitle>
          </Content>
          <ImageWrapper backgroundImageUrl={backgroundImageUrl}>
            {earnestText && (
              <EarnestIconWrap>
                <Image src={earnestIconImage} alt="Earnest App-Icon" width="42" height="42" />
                <SubTitle paddingLeft="9px">{earnestText}</SubTitle>
              </EarnestIconWrap>
            )}
            <StoreIconWrap>
              {appStoreCtaUrl && (
                <StoreLink href={appStoreCtaUrl} target="_blank" data-tracking-id="ga-nextjs-earnest-appstore-cta">
                  <Image src={appStoreImage} alt="App Store" width="108" height="35" />
                </StoreLink>
              )}
              {playStoreCtaUrl && (
                <StoreLink href={playStoreCtaUrl} target="_blank" data-tracking-id="ga-nextjs-earnest-playstore-cta">
                  <Image src={playStoreImage} alt="Play Store" width="108" height="35" />
                </StoreLink>
              )}
            </StoreIconWrap>
          </ImageWrapper>
        </EarnestSplitViewContainer>
      </PageWidthContainer>
    </HeaderAnchorOffset>
  );
};
