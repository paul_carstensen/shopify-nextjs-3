import React from 'react';
import styled from 'styled-components';
import { AnchorHeaderHeroOffset } from '../common/AnchorHeaderHeroOffset';
import { SliderContainer, SliderContainerProps } from '../SliderContainer';

const MainContainer = styled.div`
  height: calc(100vh - 178px);
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;

  @media screen and (max-width: 768px) {
    height: 100%;
    padding-top: 16px;
  }
`;

const FixedContainer = styled.div`
  height: calc(100vh - 178px);
  width: 50%;

  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;

const ScrollContainer = styled.div`
  height: 100%;
  overflow-y: scroll;
  width: 50%;
  scroll-snap-type: y proximity;

  ::-webkit-scrollbar {
    width: 0 !important;
    height: 0 !important;
  }

  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;

export const SLIDER_PAGE_SCROLL_CONTAINER_ID = 'slider-page-scroll-container';

type SliderPageProps = Pick<SliderContainerProps, 'backgroundColor' | 'title' | 'subtitle' | 'text'>;

export const SliderPage: React.FC<SliderPageProps> = ({ backgroundColor, title, subtitle, text, children }) => (
  <AnchorHeaderHeroOffset>
    <MainContainer>
      <FixedContainer>
        <SliderContainer backgroundColor={backgroundColor} title={title} subtitle={subtitle} text={text} />
      </FixedContainer>

      <ScrollContainer id={SLIDER_PAGE_SCROLL_CONTAINER_ID}>{children}</ScrollContainer>
    </MainContainer>
  </AnchorHeaderHeroOffset>
);
