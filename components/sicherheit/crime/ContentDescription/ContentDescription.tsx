import React from 'react';
import styled from 'styled-components';

const ContentDescriptionInner = styled.p<{ fontColor?: string }>`
  font-size: 14px;
  ${(props) => `color: ${props.fontColor || '#19232d'};`}
`;

export interface ContentDescriptionProps {
  children: React.ReactNode;
  fontColor?: string;
}

export const ContentDescription: React.FC<ContentDescriptionProps> = ({ children, fontColor }) => (
  <ContentDescriptionInner fontColor={fontColor}>{children}</ContentDescriptionInner>
);
