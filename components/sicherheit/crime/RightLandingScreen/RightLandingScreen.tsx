import React, { useState } from 'react';
import styled from 'styled-components';
import { useFormik } from 'formik';
import { CrimeInfo } from '../../../../types/sicherheit/wie-sicher-ist-dein-wohnort';
import { postalCodeSchema } from './postalCodeSchema';
import { FormErrorText } from '../../../FormErrorText';
import { Input } from '../../../Input';
import { getCrimecheckInfo } from './getCrimeCheckInfo';
import { ContentSubtitle } from '../ContentSubtitle';
import { ContentDescription } from '../ContentDescription';

const FixedContainer = styled.div`
  height: calc(100vh - 178px);
  width: 100%;
  display: grid;
  grid-template-rows: 1fr auto 1fr;
  grid-template-areas: 'top' 'contents' 'postalCode';
  background-color: #19232d;
  background-blend-mode: multiply;
  background-size: cover;
  padding: 30px 50px;

  @media screen and (max-width: 768px) {
    height: calc(100vh - 119px);
    width: 100%;
    padding: 55px 30px 55px 25px;
  }
`;

const FixedContainerContents = styled.div`
  grid-area: contents;
`;

const FormContainer = styled.form`
  z-index: 1;
  grid-area: form;
  width: 75%;
  & label {
    font-size: 12px;
    font-weight: 500;
    color: #fff;
    text-transform: uppercase;
    line-height: normal;
    letter-spacing: 3px;
  }
`;

const InputsContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 18px;
  grid-template-areas: 'postalCode';
  & [for='postalCode'] {
    grid-area: postalCode;
  }
`;

const FullLineInput = styled(Input)`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 37px;
  color: #19232d;
  &:last-child {
    margin: 10px 0 0 0;
    width: calc(100%);
    background-color: white;
  }
`;

const SubmitButtonContainer = styled.div`
  display: flex;
  align-items: flex-end;
  margin-bottom: 10px;
`;

const SubmitButton = styled.button.attrs({
  type: 'submit',
  'data-tracking-id': 'ga-crime-check-submit',
})`
  background-color: #c4d6e6;
  color: #19232d;
  border: 1px solid #c4d6e6;
  font-family: Quicksand;
  font-size: 12px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 3px;
  width: calc(100% - 2px);
  padding: 10px 0;
  text-transform: uppercase;
  text-align: center;
`;

type RightLandingScreenProps = {
  setCrimeCheckInfo: (info: CrimeInfo) => void;
};

export const RightLandingScreen: React.FC<RightLandingScreenProps> = ({ setCrimeCheckInfo }) => {
  const [crimeAPIError, setcrimeAPIError] = useState<string | undefined>(undefined);

  const onSubmit = async ({ postalCode }: { [key: string]: string }) => {
    try {
      const result = await getCrimecheckInfo(postalCode);
      setCrimeCheckInfo(result);
    } catch (err: unknown) {
      setcrimeAPIError(err instanceof Error ? err.message : 'Unbekannter Fehler');
    }
  };

  const { values, errors, handleSubmit, handleChange } = useFormik({
    initialValues: {
      postalCode: '',
    },
    validationSchema: postalCodeSchema,
    validateOnChange: false,
    onSubmit,
  });

  return (
    <FixedContainer>
      <FixedContainerContents>
        <ContentSubtitle fontColor="white">
          Prüfe jetzt Dein persönliches Risikopotential ausgehend von dem Ort an dem Du wohnst.
        </ContentSubtitle>
        <ContentDescription fontColor="white">
          Eine Stadt oder Gemeinde kann generell als sicher gelten und trotzdem ein Hotspot für eine spezielle Form der
          Kriminalität sein. Könntest Du Dir vorstellen, welche deutsche Stadt das El Dorado für Handtaschendiebstahl
          ist? Oder in welcher Stadt man sein Fahrrad am besten nicht unangekettet lässt?
        </ContentDescription>
        <ContentDescription fontColor="white">
          Gegen viele Formen der Kriminalität kann man als Einzelperson wenig ausrichten. Nichtsdestotrotz ist man alles
          andere als machtlos. Gerade der technische Fortschritt der letzten Jahre hat gezeigt, dass man auch als
          Privatperson der Kriminalität den Kampf ansagen kann.
        </ContentDescription>
        <FormContainer id="crime-check-form" onSubmit={handleSubmit}>
          <InputsContainer>
            <FullLineInput id="postalCode" label="PLZ" value={values.postalCode} onChange={handleChange} required />
            <SubmitButtonContainer>
              <SubmitButton>Weiter</SubmitButton>
            </SubmitButtonContainer>
          </InputsContainer>
          {errors.postalCode && <FormErrorText>{errors.postalCode}</FormErrorText>}
          {crimeAPIError && <FormErrorText>{crimeAPIError}</FormErrorText>}
        </FormContainer>
      </FixedContainerContents>
    </FixedContainer>
  );
};
