import React from 'react';
import styled from 'styled-components';
import { SliderContainer } from '../SliderContainer';

export interface ResultSlideThirdProps {
  counter?: string;
  backgroundColor?: string;
  children?: React.ReactNode;
  buttonUrl: string;
  buttonTrackingId?: string;
}

const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 10px;
`;

const Button = styled.button`
  background-color: #19232d;
  color: white;
  border: 1px solid #19232d;
  font-size: 12px;
  font-weight: 500;
  line-height: normal;
  letter-spacing: 3px;
  padding: 10px;
  text-transform: uppercase;
  text-align: center;
`;

export const ResultSlideThird: React.FC<ResultSlideThirdProps> = ({
  backgroundColor,
  counter,
  children,
  buttonUrl,
  buttonTrackingId,
}) => (
  <>
    <SliderContainer counter={counter} backgroundColor={backgroundColor}>
      {children}
      <a href={buttonUrl} target="_blank" rel="noreferrer">
        <ButtonContainer>
          <Button data-tracking-id={buttonTrackingId}>Weitere Informationen</Button>
        </ButtonContainer>
      </a>
    </SliderContainer>
  </>
);
