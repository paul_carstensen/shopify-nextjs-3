import React from 'react';
import styled from 'styled-components';

const ContentTitleInner = styled.h1<{ fontColor?: string }>`
  font-size: 46px;
  font-weight: bold;
  ${(props) => `color: ${props.fontColor || '#19232d'};`}
  margin: 0 0 20px 0;
  text-transform: none;

  @media screen and (max-width: 768px) {
    font-size: 26px;
  }
`;

export interface ContentTitleProps {
  fontColor?: string;
  children: React.ReactNode;
}

export const ContentTitle: React.FC<ContentTitleProps> = ({ children, fontColor }) => (
  <ContentTitleInner fontColor={fontColor}>{children}</ContentTitleInner>
);
