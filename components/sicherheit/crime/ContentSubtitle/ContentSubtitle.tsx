import React from 'react';
import styled from 'styled-components';

const ContentSubtitleInner = styled.h2<{ fontColor?: string }>`
  font-size: 22px;
  font-weight: 600;
  ${(props) => `color: ${props.fontColor || '#19232d'};`}
  text-transform: none;

  @media screen and (max-width: 768px) {
    font-size: 16px;
  }
`;

export interface ContentSubtitleProps {
  fontColor?: string;
  children: React.ReactNode;
}

export const ContentSubtitle: React.FC<ContentSubtitleProps> = ({ children, fontColor }) => (
  <ContentSubtitleInner fontColor={fontColor}>{children}</ContentSubtitleInner>
);
