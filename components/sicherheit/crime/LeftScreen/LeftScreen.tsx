import React, { ReactElement } from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import { useMediaQuery } from 'react-responsive';
import burglarImage from '../../../../assets/images/crime-check/burglar.jpg';
import arrowDown from '../../../../assets/images/crime-check/arrow-down.svg';
import { ContentTitle } from '../ContentTitle';
import { ContentSubtitle } from '../ContentSubtitle';
import { ContentDescription } from '../ContentDescription';

export const LEFT_CONTAINER_ID = 'crime-check-left-container';

type FixedContainerProps = {
  backgroundImageUrl: string;
};

const FixedContainer = styled.div<FixedContainerProps>`
  height: calc(100vh - 178px);
  width: 100%;
  display: grid;
  grid-template-rows: 1fr auto 1fr;
  grid-template-areas: 'top' 'contents' 'sharingbar';
  background-image: linear-gradient(#999, #999), url('${(props) => props.backgroundImageUrl}');
  background-blend-mode: multiply;
  background-size: cover;
  padding: 30px 90px 30px 50px;

  @media screen and (max-width: 768px) {
    width: 100%;
    padding: 55px 30px 55px 25px;
  }
`;

const FixedContainerContents = styled.div`
  grid-area: contents;
`;

const FixedContainerSharingBar = styled.div`
  grid-area: sharingbar;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;

type ArrowButtonProps = {
  height: number;
};

const ArrowButton = styled.div<ArrowButtonProps>`
  display: none;

  @media screen and (max-width: 768px) {
    display: block;
    text-align: center;
    height: ${(props) => props.height}px;
  }
`;

type LeftScreenProps = {
  renderSharingBar: () => ReactElement;
  onArrowButtonClick: () => void;
};

export const LeftScreen: React.FC<LeftScreenProps> = ({ renderSharingBar, onArrowButtonClick }) => {
  const isMobile = useMediaQuery({ query: '(max-width: 768px)' });

  return (
    <FixedContainer backgroundImageUrl={burglarImage.src} id={LEFT_CONTAINER_ID}>
      <FixedContainerContents>
        <ContentTitle fontColor="white">Wie sicher ist Dein Wohnort?</ContentTitle>
        <ContentSubtitle fontColor="white">Wir brauchen nur Deine Postleitzahl.</ContentSubtitle>
        <ContentDescription fontColor="white">
          Obwohl Deutschland allgemein als sicheres Land gilt, sind Teile des Landes wahre Hochburgen der Kriminalität.
          Was Sicherheit angeht, gibt es erhebliche lokale Unterschiede für Städte und Gemeinden, wie die jährlichen
          Kriminalitätsstatistiken des Bundeskriminalamts bestätigen.
        </ContentDescription>
      </FixedContainerContents>
      <FixedContainerSharingBar>
        <ArrowButton height={arrowDown.height} onClick={onArrowButtonClick}>
          <Image src={arrowDown} alt="Weiter" />
        </ArrowButton>
        {isMobile ? '' : renderSharingBar()}
      </FixedContainerSharingBar>
    </FixedContainer>
  );
};
