import React from 'react';
import styled from 'styled-components';

export interface ResultSlideFirstProps {
  counter?: string;
  backgroundColor?: string;
  children?: React.ReactNode;
  counterFontColor?: string;
}

const SliderContainerInner = styled.div<{ backgroundColor?: string; counterFontColor?: string }>`
  min-height: calc(100vh - 178px);
  scroll-snap-align: start;
  ${(props) => `background: ${props.backgroundColor}`};
  & div.slider-container {
    min-height: calc(100vh - 178px);
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 10%;
    position: relative;

    & .slider-container-head {
      text-align: right;
      position: absolute;
      right: 10px;
      top: 0px;
      ${(props) => `color: ${props.counterFontColor || '#19232d'};`}
    }

    & .slider-container-head span {
      font-size: 46px;
      font-weight: bold;
    }
  }

  @media screen and (max-width: 768px) {
    width: 100%;
    background-size: 90%;
    background-position-x: 40vw;
    min-height: calc(100vh - 178px);
    height: 100%;

    & div.slider-container {
      min-height: calc(100vh - 178px);
      padding: 25px;
      max-width: inherit;

      & .slider-container-head span {
        font-size: 26px;
      }
    }
  }
`;

export const SliderContainer: React.FC<ResultSlideFirstProps> = ({
  backgroundColor,
  counter,
  children,
  counterFontColor,
}) => (
  <>
    <SliderContainerInner
      id={`tipp-slide-${counter}`}
      backgroundColor={backgroundColor}
      counterFontColor={counterFontColor}
    >
      <div className="slider-container">
        <div className="slider-container-head">{counter ? <span className="count"> {counter}/3</span> : false}</div>
        <div className="slider-container-main">{children}</div>
        <div className="slider-container-footer" />
      </div>
    </SliderContainerInner>
  </>
);
