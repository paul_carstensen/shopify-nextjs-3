import React from 'react';
import { BlockContent as Props } from 'types/partnerPages';
import isInsideLinkAnchor from '../../../../helpers/partner_pages/isInsideLinkAnchor';
import styles from './style.module.css';
import commonStyles from '../common.module.css';
import emptyBigImage from '../../../../assets/images/partner-pages/empty-big.png';

interface Content {
  title: string;
  description: string;
  imageUrl: string;
  imageLocation: 'left' | 'right';
  buttonText: string;
  buttonUrl: string;
}

export interface BlockProps {
  title: string;
  description: string;
  content: Content[];
  identifier: string;
}

const btnClass = `${commonStyles.btn} ${commonStyles['btn--primary']} ${commonStyles.button} ${commonStyles.light} ${commonStyles['ga-partner-page-use-case']}`;

const renderWithImageLeft = (t: Content) => (
  <>
    <div className={styles['use-case-image-section']}>
      <img src={t.imageUrl.length > 0 ? t.imageUrl : emptyBigImage.src} className={styles.image} alt={t.description} />
    </div>
    <div className={styles['use-case-text-section']}>
      {t.title ? <h2 className={styles['content-title']}>{t.title}</h2> : null}
      <p className={styles['content-description']}>{t.description}</p>
      <div>
        {isInsideLinkAnchor(t.buttonUrl) ? (
          <a href={t.buttonUrl} className={btnClass} role="button">
            {t.buttonText}
          </a>
        ) : (
          <a href={t.buttonUrl} target="_blank" rel="noopener noreferrer" className={btnClass} role="button">
            {t.buttonText}
          </a>
        )}
      </div>
    </div>
  </>
);

const renderWithImageRight = (t: Content) => (
  <>
    <div className={styles['use-case-text-section']}>
      {t.title ? <h2 className={styles['content-title']}>{t.title}</h2> : null}
      <p className={styles['content-description']}>{t.description}</p>
      <div>
        {isInsideLinkAnchor(t.buttonUrl) ? (
          <a href={t.buttonUrl} className={btnClass} role="button">
            {t.buttonText}
          </a>
        ) : (
          <a href={t.buttonUrl} target="_blank" rel="noopener noreferrer" className={btnClass} role="button">
            {t.buttonText}
          </a>
        )}
      </div>
    </div>
    <div className={styles['use-case-image-section']}>
      <img src={t.imageUrl.length > 0 ? t.imageUrl : emptyBigImage.src} className={styles.image} alt={t.description} />
    </div>
  </>
);

const Block: React.FC<Props> = ({ data }: Props) => {
  const { title, description, content, identifier } = data as BlockProps;

  return (
    <div className={commonStyles['anchor-header-offset']} id={identifier}>
      <div className={styles['use-case']}>
        <div className={commonStyles['page-width']}>
          <div className={styles['use-case-header']}>
            <h2 className={commonStyles.dark}>{title}</h2>
            {description ? <p className={styles['use-case-description']}>{description}</p> : <></>}
          </div>
          {content?.map((t, i) => (
            <div key={`content-${i.toString()}`} className={styles['content-container']}>
              {t.imageLocation === 'left' ? renderWithImageLeft(t) : renderWithImageRight(t)}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Block;
