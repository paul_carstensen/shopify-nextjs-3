import React from 'react';
import { BlockContent as Props } from 'types/partnerPages';
import isInsideLinkAnchor from '../../../../helpers/partner_pages/isInsideLinkAnchor';
import styles from './style.module.css';
import commonStyles from '../common.module.css';

export interface BlockProps {
  title: string;
  buttonText: string;
  buttonUrl: string;
  backgroundImage: string;
  identifier: string;
}

const Block: React.FC<Props> = ({ data }: Props) => {
  const { title, buttonText, buttonUrl, backgroundImage, identifier } = data as BlockProps;

  return (
    <div className={commonStyles['anchor-header-offset']} id={identifier}>
      <div
        className={`${commonStyles.container} ${styles.cta}`}
        style={{
          backgroundImage: `linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5)), url(${backgroundImage})`,
        }}
        id={identifier}
      >
        <h2 className={commonStyles.light}>{title}</h2>
        {isInsideLinkAnchor(buttonUrl) ? (
          <a
            href={buttonUrl}
            className={`${commonStyles.btn} ${styles['btn-outline-primary']} ${commonStyles.button} ${commonStyles.light} ${styles['ga-partner-page-cta']}`}
            role="button"
          >
            {buttonText}
          </a>
        ) : (
          <a
            href={buttonUrl}
            target="_blank"
            rel="noopener noreferrer"
            className={`${commonStyles.btn} ${styles['btn-outline-primary']} ${commonStyles.button} ${commonStyles.light} ${styles['ga-partner-page-cta']}`}
            role="button"
          >
            {buttonText}
          </a>
        )}
      </div>
    </div>
  );
};

export default Block;
