import React, { useState } from 'react';
import { BlockContent as Props } from 'types/partnerPages';
import styles from './style.module.css';
import commonStyles from '../common.module.css';
import cartIcon from '../../../../assets/images/partner-pages/cart-icon.png';
import plusIcon from '../../../../assets/images/partner-pages/plus.png';
import minusIcon from '../../../../assets/images/partner-pages/minus.png';

interface Content {
  title: string;
  price: string;
  compareAtPrice?: string;
  variantID: string;
}

export interface BlockProps {
  title: string;
  hideVariants: boolean;
  preChoosenVariant: number;
  images: string[];
  content: Content[];
  identifier: string;
  isSKU?: boolean;
}

const preProcessVariantElements = (variant: Content) => {
  const compareAtPrice = Number(variant.compareAtPrice || variant.price);
  const price = Number(variant.price);
  const priceSale = compareAtPrice - price;
  const savingPercent = compareAtPrice > 0 ? (compareAtPrice - price) / compareAtPrice : 0;
  const onPriceSale = priceSale >= 5 && savingPercent < 0.05;
  const onSale = savingPercent >= 0.05 || priceSale >= 5;
  const euroSign = onPriceSale ? '€' : '';
  const percentSign = onPriceSale ? '' : ' %';

  const renderedOnSalePrice = onSale ? (
    <span className={styles['original-price']}>€{compareAtPrice.toLocaleString('DE-de')}</span>
  ) : (
    <></>
  );
  const renderedProductPrice = (
    <span className={onSale ? styles['reduced-price'] : styles['normal-price']}>€{price.toLocaleString('DE-de')}</span>
  );
  const renderedSaving = onSale ? (
    <span className={styles.savings}>
      Spare {euroSign}
      {(onPriceSale ? priceSale : Math.round(savingPercent * 100)).toLocaleString('DE-de')}
      {percentSign}
    </span>
  ) : (
    <></>
  );

  return {
    renderedOnSalePrice,
    renderedProductPrice,
    renderedSaving,
  };
};

const Block: React.FC<Props> = ({ data }: Props) => {
  const { title, content, images: blockImages, identifier, isSKU } = data as BlockProps;
  const [imageIndex, setImageIndex] = useState(0);
  const [variantIndex, setVariantIndex] = useState(
    data.preChoosenVariant && data.preChoosenVariant >= 0 && data.preChoosenVariant < content.length
      ? data.preChoosenVariant
      : 0
  );
  const [quantity, setQuantity] = useState(1);
  const currentVariant = content[variantIndex] ||
    content[0] || { title: 'Please set a variant', price: '0', variantID: '0' };
  const { renderedOnSalePrice, renderedProductPrice, renderedSaving } = preProcessVariantElements(currentVariant);
  const showVariant = !data.hideVariants && content.length > 1;

  return (
    <div className={commonStyles['anchor-header-offset']} id={identifier}>
      <div className={`${commonStyles.container} ${styles.buying}`}>
        <div className={commonStyles['page-width']}>
          <div className={styles['inner-container']}>
            <div className={styles['image-section']}>
              <div className={styles['current-image']}>
                <img src={blockImages[imageIndex]} alt="Main" />
              </div>
              <div className={styles['image-picker']}>
                {blockImages.map((src, index) => (
                  <div
                    key={`${src}-${src.length}`}
                    className={imageIndex === index ? styles.current : ''}
                    onClick={() => setImageIndex(index)}
                    aria-hidden="true"
                    style={{
                      backgroundImage: `url(${src})`,
                    }}
                  />
                ))}
              </div>
            </div>
            <div className={styles['bubble-picker']}>
              {blockImages.map((src, index) => (
                <div
                  key={`${src}-${src.length}`}
                  className={imageIndex === index ? styles.current : ''}
                  onClick={() => setImageIndex(index)}
                  aria-hidden="true"
                />
              ))}
            </div>
            <div className={styles['form-section']}>
              <h2 className={commonStyles.dark}>{title}</h2>
              <div className={styles['product-price']}>
                {renderedOnSalePrice}
                {renderedProductPrice}
                {renderedSaving}
              </div>
              <div className={styles.vat}>inkl. MwSt.</div>
              <div className={styles['variant-picker']} style={showVariant ? {} : { display: 'none' }}>
                <div className={styles['variant-text']}>VARIANTE</div>
                {/* eslint-disable-next-line jsx-a11y/no-onchange */}
                <select
                  value={variantIndex}
                  className={`ga-partner-page-buying${isSKU ? '-sku' : ''}-select`}
                  onChange={(e) => setVariantIndex(Number(e.target.value))}
                >
                  {content.map((variant, index) => (
                    <option key={`${variant.title}-${variant.title.length}`} value={index}>
                      {variant.title}
                    </option>
                  ))}
                </select>
              </div>
              <div className={styles['quantity-text']}>MENGE</div>
              <div className={styles.quantity}>
                <div
                  className={`${styles.minus} ga-partner-page-buying${isSKU ? '-sku' : ''}-minus`}
                  onClick={() => setQuantity(quantity > 1 ? quantity - 1 : 1)}
                  aria-hidden="true"
                >
                  <img src={minusIcon.src} alt="Weniger" />
                </div>
                <div className={styles.quantity}>{quantity}</div>
                <div
                  className={`${styles.plus} ga-partner-page-buying${isSKU ? '-sku' : ''}-plus`}
                  onClick={() => setQuantity(quantity < 3 ? quantity + 1 : 3)}
                  aria-hidden="true"
                >
                  <img src={plusIcon.src} alt="Mehr" />
                </div>
              </div>
              <form method="post" action="/cart/add" encType="multipart/form-data">
                <input type="hidden" name="form_type" value="product" />
                <input type="hidden" name="utf8" value="✓" />
                <input type="hidden" name="id" value={content[variantIndex].variantID} />
                <input type="hidden" name="quantity" value={quantity} />

                <button
                  type="submit"
                  name="add"
                  className={`${styles.btn} btn--full ${styles['add-to-cart']} ga-partner-page-buying${
                    isSKU ? '-sku' : ''
                  }-submit`}
                >
                  <span>In den Warenkorb legen</span>
                  <img src={cartIcon.src} alt="Cart" />
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Block;
