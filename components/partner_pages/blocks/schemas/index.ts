import ADVANTAGES from './advantages.json';
import CTA from './cta.json';
import FAQ from './faq.json';
import GALLERY from './gallery.json';
import HERO from './hero.json';
import OFFERS from './offers.json';
import PRICING from './pricing.json';
import PROCESS from './process.json';
import TESTIMONIAL from './testimonial.json';
import USE_CASE from './usecase.json';
import VIDEO from './video.json';
import REGISTRATION from './registration.json';
import BUYING from './buying.json';
import BUYING_SKU from './buying_sku.json';
import CUSTOM_HTML from './custom_html.json';
import CARESHIP_WIDGET from './careship_widget.json';

export default {
  ADVANTAGES,
  CTA,
  FAQ,
  GALLERY,
  HERO,
  OFFERS,
  PRICING,
  PROCESS,
  TESTIMONIAL,
  USE_CASE,
  VIDEO,
  REGISTRATION,
  BUYING,
  BUYING_SKU,
  CUSTOM_HTML,
  CARESHIP_WIDGET,
};
