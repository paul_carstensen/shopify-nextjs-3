import { useFormik } from 'formik';
import styled from 'styled-components';
import React from 'react';
import arabeskFormSchema from './arabeskFormSchema';
import { SelectQuestion } from '../../../SelectQuestion';
import { FormErrorText } from '../../../FormErrorText';
import { useApiErrors } from '../../../../context/errors/apiErrorsProvider';
import { PageWidthContainer } from '../../../common/PageWidthContainer';
import { HeaderAnchorOffset } from '../../../common/HeaderAnchorOffset';

const selects = [
  {
    question: 'Ist hinter der Wasseruhr ein Feinfilter installiert?',
    options: [
      { disabled: true, title: '', value: '' },
      { title: 'ja', value: 'true' },
      { title: 'nein', value: 'false' },
    ],
    value: '',
    questionMaxWidth: '600px',
    selectWidth: '235px',
    statePath: 'arabeskWaterFilter',
  },
  {
    question:
      'Wird der Feinfilter regelmäßig gereinigt, rückgespült bzw. die Filterkerze ausgewechselt? Die Intervalle gibt der Hersteller vor, mindestens aber zweimal jährlich.',
    options: [
      { disabled: true, title: '', value: '' },
      { title: 'ja', value: 'true' },
      { title: 'nein', value: 'false' },
    ],
    value: '',
    questionMaxWidth: '660px',
    selectWidth: '235px',
    statePath: 'arabeskWaterFilterMaintenance',
  },
  {
    question:
      'Gibt es nicht oder selten genutzte Teile der Wasserversorgung (z.B. Sanitäreinrichtung im Keller oder in einem Nebengebäude, Einrichtung zur Gartenbewässerung)?',
    options: [
      { disabled: true, title: '', value: '' },
      { title: 'ja', value: 'true' },
      { title: 'nein', value: 'false' },
    ],
    value: '',
    questionMaxWidth: '864px',
    selectWidth: '235px',
    statePath: 'arabeskWaterRarelyUsedSources',
  },
  {
    question: 'Sind in den Nassbereichen die Silikonfugen in Ordnung (Badewanne, Fliesen, Duschtassen etc.)?',
    options: [
      { disabled: true, title: '', value: '' },
      { title: 'in Ordnung', value: 'true' },
      { title: 'defekt oder teilweise defekt', value: 'false' },
    ],
    value: '',
    questionMaxWidth: '864px',
    selectWidth: '235px',
    statePath: 'arabeskWaterSiliconeGroutOk',
  },
  {
    question: 'Ist in jedem Wohn- und Schlafraum sowie im Flur ein Rauchmelder installiert?',
    options: [
      { disabled: true, title: '', value: '' },
      { title: 'ja', value: 'true' },
      { title: 'nein', value: 'false' },
    ],
    value: '',
    questionMaxWidth: '600px',
    selectWidth: '235px',
    statePath: 'arabeskSmokeAlarms',
  },
  {
    question: 'Sind Fehlerstromschutzschalter (RCD- bzw. FI-Schutzschalter) im Sicherungskasten vorhanden?',
    options: [
      { disabled: true, title: '', value: '' },
      { title: 'ja', value: 'true' },
      { title: 'nein', value: 'false' },
    ],
    value: '',
    questionMaxWidth: '864px',
    selectWidth: '235px',
    statePath: 'arabeskCircuitBreakers',
  },
  {
    question: 'Bist Du der/die Eigentümer:in dieser Immobilie?',
    options: [
      { disabled: true, title: '', value: '' },
      { title: 'ja', value: 'true' },
      { title: 'nein', value: 'false' },
    ],
    value: '',
    questionMaxWidth: '600px',
    selectWidth: '235px',
    statePath: 'arabeskPropertyOwner',
  },
  {
    question: 'Planst Du in den nächsten 12 Monaten umzuziehen?',
    options: [
      { disabled: true, title: '', value: '' },
      { title: 'ja', value: 'true' },
      { title: 'nein', value: 'false' },
    ],
    value: '',
    questionMaxWidth: '600px',
    selectWidth: '235px',
    statePath: 'arabeskMovingOutSoon',
  },
];

const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  @media screen and (max-width: 768px) {
    padding: 0 12px;
  }
`;

const SubmitButtonContainer = styled.div``;

const SubmitButton = styled.button.attrs({
  type: 'submit',
  'data-tracking-id': 'ga-radon-arabesk-submit',
})`
  background-color: #19232d;
  color: white;
  border: 1px solid #19232d;
  font-family: Quicksand;
  font-size: 12px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 3px;
  width: 254px;
  padding: 10px 0;
  text-transform: uppercase;
  text-decoration: none;
  text-align: center;
  border-radius: 0px;
  margin-top: 5px;
  @media screen and (max-width: 768px) {
    margin-top: 15px;
    width: 100%;
  }
`;

export type ArabeskFormProps = {
  onSubmit: (arabeskData: any) => void; // TODO: aktually we are leaking here Pick<FormikConfig<FormikValues>, 'onSubmit'> will need a better type for arabeskData
};

export const ArabeskForm = ({ onSubmit }: ArabeskFormProps) => {
  const { errors: apiErrors } = useApiErrors();
  const { errors, values, handleSubmit, setFieldValue } = useFormik({
    initialValues: {
      arabeskWaterFilter: undefined,
      arabeskWaterFilterMaintenance: undefined,
      arabeskWaterRarelyUsedSources: undefined,
      arabeskWaterSiliconeGroutOk: undefined,
      arabeskSmokeAlarms: undefined,
      arabeskCircuitBreakers: undefined,
      arabeskPropertyOwner: undefined,
      arabeskMovingOutSoon: undefined,
    },
    validationSchema: arabeskFormSchema,
    validateOnChange: false,
    onSubmit,
  });

  return (
    <HeaderAnchorOffset id="arabesk_form">
      <PageWidthContainer>
        <FormContainer onSubmit={handleSubmit}>
          {selects.map((item) => (
            <SelectQuestion
              {...item}
              key={item.statePath}
              disabled={item.statePath === 'arabeskWaterFilterMaintenance' && !values.arabeskWaterFilter}
              onSelectChange={(e) => setFieldValue(item.statePath, e.target.value === 'true')}
            />
          ))}
          <SubmitButtonContainer>
            <SubmitButton>Zur Auswertung</SubmitButton>
          </SubmitButtonContainer>

          {apiErrors.arabesk && <FormErrorText>{apiErrors.arabesk}</FormErrorText>}
          {Object.values(errors).length ? (
            <FormErrorText>Bitte beantworte alle Fragen, um fortfahren zu können.</FormErrorText>
          ) : null}
        </FormContainer>
      </PageWidthContainer>
    </HeaderAnchorOffset>
  );
};
