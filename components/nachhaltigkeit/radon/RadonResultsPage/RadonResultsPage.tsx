import React from 'react';
import styled from 'styled-components';
import Hero from '../../../Hero';
import { RadonSharingBar } from '../RadonSharingBar';
import { RadonResultsSplitView } from '../RadonResultsSplitView';
import { Text } from '../../../Text';
import hero360x from '../../../../assets/images/radon/gimmick-hero_360x.jpg';
import hero540x from '../../../../assets/images/radon/gimmick-hero_540x.jpg';
import hero1420x from '../../../../assets/images/radon/gimmick-hero_1420x.jpg';
import hero2840x from '../../../../assets/images/radon/gimmick-hero_2840x.jpg';
import { MainContainerWithElementStyles } from '../../../common/MainContainer';

const SharingBarContainer = styled.div`
  text-align: center;
`;

const radonResultsPageText =
  'Wie stark Radon aus dem Boden entweichen und potenziell in Innenräume von Häusern gelangen kann, wird als "Radon-Potenzial" bezeichnet. Seine Höhe hängt davon ab, wie viel Radon im Boden konzentriert ist und wie (gas-)durchlässig der Boden ist. Das "Radon-Potenzial" berücksichtigt daher neben dem Radon-Vorkommen im Boden auch die Durchlässigkeit des Bodens. Die für die Prognose verwendeten Parameter können lokal stark variieren. Die Prognose bildet den aktuellen Stand der Erkenntnisse ab. Sie unterliegt einer permanenten Validierung und Weiterentwicklung, basierend auf neuen Daten und neuen Verfahren durch das Bundesamt für Strahlenschutz. Prognosen sind immer mit Unsicherheiten verbunden. In Regionen, in denen keine oder nur wenige Messwerte vorliegen, kann es zu deutlichen Abweichungen zwischen der Prognose und der wirklichen Situation kommen. Quelle: Bundesamt für Strahlenschutz Unten findest Du die Prognose des Radonpotezials Deiner unmittelbaren Umgebung.';

export type RadonResultsPageProps = {
  radonLevel: number;
  onSplitViewCtaClick: () => void;
};

export const RadonResultsPage: React.FC<RadonResultsPageProps> = ({ radonLevel, onSplitViewCtaClick }) => (
  <MainContainerWithElementStyles>
    <Hero
      title="Radioaktives Radon in Deutschland"
      subtitle="Wie hoch ist die Gefahr in Deiner Wohngegend? Unten findest Du Dein persönliches Ergebnis!"
      backgroundImages={[hero360x.src, hero540x.src, hero1420x.src, hero2840x.src]}
      resolutions={[360, 540, 1420, 2840]}
    />
    <SharingBarContainer>
      <RadonSharingBar />
    </SharingBarContainer>
    <Text>
      <span id="lead_tracking">{radonResultsPageText}</span>
    </Text>
    <RadonResultsSplitView radonLevel={radonLevel} onCtaClick={onSplitViewCtaClick} />
  </MainContainerWithElementStyles>
);
