/** @jest-environment jsdom */
import { render } from '@testing-library/react';
import { RadonResultText } from '.';

describe('Radon result text component', () => {
  it('should render without crashing', () => {
    expect(() => {
      render(<RadonResultText radonLevel={1.0} />);
    }).not.toThrowError();
  });

  const testCases = [
    { radonLevel: -1, expectedText: /Das Radonpotenzial in Deiner Gegend ist als gering einzustufen/ },
    { radonLevel: 0, expectedText: /Das Radonpotenzial in Deiner Gegend ist als gering einzustufen/ },
    { radonLevel: 39.5, expectedText: /Das Radonpotenzial in Deiner Gegend ist als gering einzustufen/ },
    { radonLevel: 40.0, expectedText: /erhöhtes und seltener hohes Radonpotenzial/ },
    { radonLevel: 41.0, expectedText: /erhöhtes und seltener hohes Radonpotenzial/ },
    { radonLevel: 100.0, expectedText: /erhöhtes und seltener hohes Radonpotenzial/ },
    { radonLevel: 101.0, expectedText: /erhöhtes bis hohes Radonpotenzial/ },
    { radonLevel: 999, expectedText: /erhöhtes bis hohes Radonpotenzial/ },
  ];

  it.each(testCases)(
    'should render the expected result for radon level $radonLevel',
    ({ radonLevel, expectedText }) => {
      const radonResultText = render(<RadonResultText radonLevel={radonLevel} />);

      expect(radonResultText.getByText(expectedText)).toBeTruthy();
      expect(radonResultText.getByText(/Quelle: Bundesamt für Strahlenschutz/)).toBeTruthy();
    }
  );

  it('should render a fallback text if the radon level is out of bounds', () => {
    // @ts-expect-error TODO: radonLevel must be a number, figure out if we can delete this test or change the type to number | undefined
    const radonResultText = render(<RadonResultText radonLevel={undefined} />);

    expect(radonResultText.getByText(/Wir konnten Dein Ergebnis nicht ermitteln/)).toBeTruthy();
    expect(radonResultText.queryByText(/Quelle: Bundesamt für Strahlenschutz/)).toBeFalsy();
  });
});
