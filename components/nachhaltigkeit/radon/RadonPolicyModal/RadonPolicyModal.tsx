import { useState, useEffect } from 'react';
import Modal from 'react-modal';
import styled from 'styled-components';
import { SanitizedHTML } from '../../../SanitizedHTML';
import { SubmitButton } from '../../../SubmitButton';

const customStyles = {
  content: {
    top: '20%',
    boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
  },
  overlay: {
    zIndex: 100,
  },
};

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export type RadonPolicyModalProps = {
  policyTextHtml: string;
  isOpen: boolean;
  closeModal: (event: React.MouseEvent | React.KeyboardEvent) => void;
};

export const RadonPolicyModal = ({ policyTextHtml, isOpen, closeModal }: RadonPolicyModalProps) => {
  const [shouldRender, setShouldRender] = useState(false);

  useEffect(() => {
    if (!shouldRender) {
      setShouldRender(true);
    }
  }, [shouldRender]);

  if (!shouldRender) return null;

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={closeModal}
      style={customStyles}
      contentLabel="Deine Einwilligung"
      ariaHideApp={false}
    >
      <SanitizedHTML innerHTML={policyTextHtml} />
      <ButtonContainer>
        <SubmitButton onClick={closeModal}>Schließen</SubmitButton>
      </ButtonContainer>
    </Modal>
  );
};
