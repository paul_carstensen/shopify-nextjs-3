import * as Yup from 'yup';

export default Yup.object().shape({
  email: Yup.string().email('Ungültige E-Mail-Adresse').required('E-Mail-Adresse wird benötigt'),
  houseNumber: Yup.string().required('Hausnummer wird benötigt'),
  streetName: Yup.string().required('Straßenname wird benötigt'),
  postalCode: Yup.string()
    .required('Postleitzahl wird benötigt')
    .matches(/^\d{5}$/, 'Ungültige Postleitzahl'),
  consentCheckbox: Yup.boolean()
    .isTrue('Deine Einwilligung wird benötigt')
    .required('Deine Einwilligung wird benötigt'),
});
