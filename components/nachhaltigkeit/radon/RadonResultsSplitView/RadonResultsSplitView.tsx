import React from 'react';
import { SplitView } from '../../../SplitView';
import { RadonResultText } from '../RadonResultText';
import resultSplitViewImage from '../../../../assets/images/radon/result-split-view.jpg';

export type RadonResultsSplitViewProps = {
  radonLevel: number;
  onCtaClick: () => void;
};

export const RadonResultsSplitView: React.FC<RadonResultsSplitViewProps> = ({ radonLevel, onCtaClick }) => (
  <SplitView
    title="Dein persönliches Ergebnis"
    imageUrl={resultSplitViewImage}
    imagePosition="right"
    ctaTitle="Für individuelle Empfehlungen, was weitere Risiken in Deinem Haushalt betrifft, unterstützt Dich die folgende Checkliste."
    ctaText="Weiter zur Checkliste"
    onCtaButtonClick={onCtaClick}
    dataTrackingId="ga-radon-goto-arabesk-form"
  >
    <RadonResultText radonLevel={radonLevel} />
  </SplitView>
);
