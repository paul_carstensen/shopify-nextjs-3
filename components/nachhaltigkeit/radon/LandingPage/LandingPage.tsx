import React from 'react';
import styled from 'styled-components';
import Hero from '../../../Hero';
import { RadonSharingBar } from '../RadonSharingBar';
import { AddressForm } from '../AddressForm';
import { RadonSplitView } from '../RadonSplitView';
import { Text } from '../../../Text';
import hero360x from '../../../../assets/images/radon/gimmick-hero_360x.jpg';
import hero540x from '../../../../assets/images/radon/gimmick-hero_540x.jpg';
import hero1420x from '../../../../assets/images/radon/gimmick-hero_1420x.jpg';
import hero2840x from '../../../../assets/images/radon/gimmick-hero_2840x.jpg';
import { MainContainerWithElementStyles } from '../../../common/MainContainer';

const SharingBarContainer = styled.div`
  text-align: center;
`;

const HeroMobileOvewrite = styled.div`
  @media screen and (max-width: 768px) {
    & .hero-content h1 {
      padding: 10px 40px;
      margin: 0;
      font-size: 18px !important;
      font-weight: 500 !important;
      color: #19232d !important;
      text-transform: uppercase !important;
      text-align: center;
      width: calc(100% - 80px);
    }
    & .hero-content h2 {
      padding: 0 40px 10px 40px;
      margin: 0;
      margin-top: 0 !important;
      font-size: 14px !important;
      font-weight: 500 !important;
      color: #19232d !important;
    }
    & .hero {
      display: block !important;
    }
    @media screen and (max-width: 768px) {
      & .hero-content {
        height: 112px;
        width: calc(100% - 48px);
        max-height: 112px;
        margin: 35px 24px 0 24px;
      }
    }
    & .hero-content {
      padding: 0;
      backdrop-filter: blur(10px);
      background-color: rgba(255, 255, 255, 0.75);
    }
  }
`;

const HeroWrapper = styled.div`
  grid-template-areas: 'hero form';
  margin: auto;
  display: grid;
  grid-template-columns: 100% 0px;
  @media screen and (max-width: 768px) {
    grid-template-areas: 'hero' 'form';
    height: 565px;
  }
`;

const landingPageText =
  'Jede/r Hausbesitzer:in sollte sich der potenziellen Gefahr einer Radon-Belastung im eigenen Keller bewusst sein. Ob Privat-, Firmen- oder öffentlicher Grund – das geruchlose Gas ist in manchen Gegenden Deutschlands stark konzentriert. Doch was ist Radon? Es handelt sich um ein natürliches radioaktives Element, das in den Zerfallsreihen des Urans und Thoriums vorkommt. Dieses Gas, das meistens aus dem Untergrund in Häuser eindringt, kann unter ungünstigen Bedingungen die Aktivität der Raumluft so stark steigen lassen, dass das Risiko, an Lungenkrebs zu erkranken, nennenswert steigt. Aufgrund der Geruchlosigkeit kann die konkrete Belastung nur durch eine spezielle Messung im Haus festgestellt werden. Ob Deine Gegend einem erhöhten Risiko unterliegt, kannst Du vorab hier prüfen.';

export type LandingPageProps = {
  addressFormIsFocused: boolean;
  onAddressFormSubmit: (addressData: any) => void;
  onSplitViewCtaClick: () => void;
  clearFocus: () => void;
  policyTextHtml: string;
};

export const LandingPage: React.FC<LandingPageProps> = ({
  onSplitViewCtaClick,
  onAddressFormSubmit,
  addressFormIsFocused,
  clearFocus,
  policyTextHtml,
}) => (
  <MainContainerWithElementStyles>
    <HeroWrapper>
      <AddressForm
        onSubmit={onAddressFormSubmit}
        isFocused={addressFormIsFocused}
        clearFocus={clearFocus}
        policyTextHtml={policyTextHtml}
      />
      <HeroMobileOvewrite>
        <Hero
          title="Radioaktives Radon in Deutschland"
          subtitle="Wie hoch ist die Gefahr in Deiner Wohngegend? Jetzt prüfen!"
          backgroundImages={[hero360x.src, hero540x.src, hero1420x.src, hero2840x.src]}
          resolutions={[360, 540, 1420, 2840]}
        />
      </HeroMobileOvewrite>
    </HeroWrapper>
    <SharingBarContainer>
      <RadonSharingBar />
    </SharingBarContainer>
    <Text>{landingPageText}</Text>

    <RadonSplitView onCtaClick={onSplitViewCtaClick} />
  </MainContainerWithElementStyles>
);
