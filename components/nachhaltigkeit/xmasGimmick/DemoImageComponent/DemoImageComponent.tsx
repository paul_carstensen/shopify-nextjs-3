import React from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import { useMediaQuery } from 'react-responsive';
import faceSwapBefore from '../../../../assets/images/xmas-gimmick/faceswap-before.png';
import faceSwapAfter from '../../../../assets/images/xmas-gimmick/faceswap-after.png';
import faceSwapBeforeAfter from '../../../../assets/images/xmas-gimmick/faceswap-after-before.png';

const DemoImageContent = styled.div`
  @media screen and (max-width: 768px) {
  }
`;

const ImageContainer = styled.div`
  display: flex;
  justify-content: center;

  div {
    margin: 0px 10px !important;
  }
  @media screen and (max-width: 768px) {
    div {
      margin: 0px !important;
    }
  }
`;

const Title = styled.div`
  font-family: Poppins;
  font-size: 28px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: normal;
  font-weight: 600;
  text-align: center;
  padding-bottom: 15px;
  text-transform: uppercase;
`;

export const DemoImageComponent: React.FC = () => {
  const isMobile = useMediaQuery({ query: '(max-width: 768px)' });
  return (
    <DemoImageContent>
      <Title>Dein Elfen-Ich</Title>
      {isMobile ? (
        <ImageContainer>
          <Image src={faceSwapBeforeAfter} alt="Vorher-Nachher" width="420" height="380" />
        </ImageContainer>
      ) : (
        <ImageContainer>
          <Image src={faceSwapBefore} alt="Vorher" width="420" height="380" />
          <Image src={faceSwapAfter} alt="Nachher" width="420" height="380" />
        </ImageContainer>
      )}
    </DemoImageContent>
  );
};
