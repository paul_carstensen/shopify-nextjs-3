import styled from 'styled-components';

export const ImageUploadContent = styled.div`
  width: 50%;
  margin: auto;
  text-align: center;
  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;

export const Title = styled.div`
  font-family: Poppins;
  font-size: 28px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: normal;
  font-weight: 600;
  text-align: center;
  padding-top: 29px;
  padding-bottom: 15px;
  text-transform: uppercase;
`;

export const SubTitle = styled.div`
  font-family: Poppins;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.36;
  letter-spacing: normal;
  text-align: center;
  padding-bottom: 24px;
`;

export const UploadInputWrapper = styled.div<{ backgroundImage?: string }>`
  border-radius: 26px;
  height: 200px;
  width: 100%;
  margin-bottom: 20px;
  ${({ backgroundImage }) =>
    backgroundImage
      ? `
      background-image: url(${backgroundImage});
      background-size: cover;
      background-position: center;
      background-repeat: no-repeat;
      opacity: 0.8;
      border: none;
    `
      : 'border: 2px dashed #979797;'}

  @media screen and (max-width: 768px) {
    border: none;
  }
`;

export const UploadInput = styled.input`
  display: none;
`;

export const UploadLabel = styled.div`
  padding: 15px;
  border-radius: 26px;
  height: 100%;
  display: flex;
  cursor: pointer;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const UnderlinedText = styled.p`
  border-bottom: 1px solid black;
  width: fit-content;
  font-family: Poppins;
  font-size: 18px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  margin-bottom: 10px;
`;

export const GenderRadioButtonsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;

  @media screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

export const RadioButtonsGroup = styled.div`
  display: flex;
  flex: 1;
  justify-content: space-between;

  @media screen and (max-width: 768px) {
    flex-direction: column;
    align-self: center;
    flex-direction: column-reverse;
  }
`;
