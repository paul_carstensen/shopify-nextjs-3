import React from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import radioChecked from '../../../../assets/images/xmas-gimmick/radio-checked.png';
import radioDefault from '../../../../assets/images/xmas-gimmick/radio-default.png';

type RadioInputProps = {
  checked: boolean;
  value: string | null;
  label: string;
  onChange: <T>(value: string | null) => T | void;
};

const RadioInputContainer = styled.div`
  cursor: pointer;
  display: flex;
  height: 20px;
  align-items: center;
  margin: 0 0 5px 0;
`;

const Label = styled.p`
  font-family: Poppins;
  font-size: 18px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  margin: 0 0 0 10px;

  @media screen and (max-width: 768px) {
    font-size: 14px;
  }
`;

const RadioInput: React.FC<RadioInputProps> = ({ checked, value, label, onChange }) => (
  <RadioInputContainer onClick={() => onChange(value)}>
    <Image src={checked ? radioChecked : radioDefault} alt="Radio-Button" height={20} width={20} />
    <Label>{label}</Label>
  </RadioInputContainer>
);

export default RadioInput;
