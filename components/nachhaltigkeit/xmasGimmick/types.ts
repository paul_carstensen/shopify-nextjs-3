import React from 'react';

export interface FaceSwapObject {
  image: string;
  imageKey: string;
}

export type XmasFirstScreenProps = {
  setFaceSwapResult: React.Dispatch<React.SetStateAction<FaceSwapObject | null>>;
};
