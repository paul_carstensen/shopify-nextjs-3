import React from 'react';
import styled from 'styled-components';
import { ContentComponent } from '../ContentComponent';
import { DemoImageComponent } from '../DemoImageComponent';
import { ImageUploadComponent } from '../ImageUploadComponent';
import { XmasFirstScreenProps } from '../types';

const FirstScreenContainer = styled.div``;

export const XmasFirstScreen: React.FC<XmasFirstScreenProps> = ({ setFaceSwapResult }) => (
  <FirstScreenContainer>
    <ContentComponent
      title="Was kannst Du als Elf tun, um dem Weihnachtsmann dabei zu helfen, sein Zuhause zu retten?"
      subTitle="Besonders zur Weihnachtszeit ist das Zuhause ein Ort, an dem Zufriedenheit, Licht und der Duft von Glühwein einkehren. Ein bestimmtes Zuhause ist jedoch in Gefahr – und zwar das weichnachtlichste von allen – das Zuhause des Weihnachtsmanns. Allein letztes Jahr ist eine Fläche von den Dimensionen Spaniens geschmolzen. Damit der großzügige Geschenkbote nicht bald von Obdachlosigkeit bedroht ist, braucht er Deine Hilfe. Schlüpfe in die Rolle Deines persönlichen Elfs und hilf mit, langfristig etwas zu ändern. Denn wie wir wissen, gibt es ohne Nordpol auch künftig keine Geschenke mehr."
    />
    <DemoImageComponent />
    <ImageUploadComponent setFaceSwapResult={setFaceSwapResult} />
  </FirstScreenContainer>
);
