import React from 'react';
import { NavigationItem } from '../../../types';
import { EarnestAppPromotion } from './EarnestAppPromotion';
import { MenuItem } from './MenuItem';
import { SecondaryMenu } from './SecondaryMenu';

type MobileDrawerProps = {
  mainNavigationItems: NavigationItem[];
  sideNavigationItems: NavigationItem[];
};

export const MobileDrawer: React.FC<MobileDrawerProps> = ({ mainNavigationItems, sideNavigationItems }) => (
  <ul className="only-mobile mobile-nav mobile-nav--heading-style" role="navigation" aria-label="Primary">
    {mainNavigationItems.map((link, i) => (
      <MenuItem link={link} delay={i + 1} key={`mobile-mega-headerlinks-${link.title}`} />
    ))}
    <EarnestAppPromotion delay={mainNavigationItems.length + 1} />
    <SecondaryMenu delayOffset={mainNavigationItems.length + 1} sideNavigationItems={sideNavigationItems} />
  </ul>
);
