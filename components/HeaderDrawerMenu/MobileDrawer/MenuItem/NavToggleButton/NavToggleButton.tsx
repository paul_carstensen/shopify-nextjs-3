import React, { ReactNode } from 'react';

type NavToggleButtonProps = {
  buttonControlId: string;
  className: string;
  buttonClassName: string;
  children?: ReactNode;
};

export const NavToggleButton: React.FC<NavToggleButtonProps> = ({
  buttonControlId,
  className,
  buttonClassName,
  children,
}) => (
  <div className={className}>
    <button type="button" aria-controls={buttonControlId} className={buttonClassName}>
      {children}
      <span
        className={`collapsible-trigger__icon${
          children ? ' collapsible-trigger__icon--circle' : ''
        } collapsible-trigger__icon--open`}
        role="presentation"
      >
        <svg
          aria-hidden="true"
          focusable="false"
          role="presentation"
          className="icon icon--wide icon-chevron-down pointer-none"
          viewBox="0 0 28 16"
        >
          <path d="M1.57 1.59l12.76 12.77L27.1 1.59" strokeWidth="2" stroke="#000" fill="none" fillRule="evenodd" />
        </svg>
      </span>
    </button>
  </div>
);
