import React from 'react';

export const EarnestAppPromotion: React.FC<{ delay: number }> = ({ delay }) => (
  <li className={`mobile-nav__item appear-animation appear-delay-${delay} earnest-link`}>
    <div className="earnest-wrap mobile-nav__link mobile-nav__link--top-level" data-active="true">
      <img
        src="//cdn.shopify.com/s/files/1/0268/4819/8771/t/80/assets/app-icon.svg?v=10617531138236453087"
        width="50"
        height="40"
        alt="Earnest App"
      />
      Earnest App
    </div>
    <div className="badge-wrap">
      <a
        href="https://apps.apple.com/de/app/earnest-dein-verbrauchs-guide/id1515761747"
        target="_blank"
        rel="noreferrer"
      >
        <img
          src="//cdn.shopify.com/s/files/1/0268/4819/8771/t/80/assets/app-store-badge-small.svg?v=10766649635902542294"
          width="35"
          height="34"
          alt="App store"
        />
      </a>
      <a
        href="https://play.google.com/store/apps/details?id=de.uptodate.earnest.app&hl=de"
        target="_blank"
        rel="noreferrer"
      >
        <img
          src="//cdn.shopify.com/s/files/1/0268/4819/8771/t/80/assets/google-play-badge-small.svg?v=9307046514505579589"
          width="35"
          height="34"
          alt="Google pay"
        />
      </a>
    </div>
  </li>
);
