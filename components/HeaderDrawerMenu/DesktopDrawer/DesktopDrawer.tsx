import React from 'react';
import { NavigationItem } from '../../../types';
import { EarnestAppPromotion } from './EarnestAppPromotion';

type MenuItemProps = {
  link: NavigationItem;
  delay: number;
};

export const MenuItem: React.FC<MenuItemProps> = ({ link, delay }) => {
  if (link.title === 'Earnest App') {
    return <EarnestAppPromotion delay={delay} />;
  }

  return (
    <li className={`mobile-nav__item appear-animation appear-delay-${delay}`}>
      <a href={link.url} className="mobile-nav__link mobile-nav__link--top-level">
        {link.title}
      </a>
    </li>
  );
};

type DesktopDrawerProps = {
  sideNavigationItems: NavigationItem[];
};

export const DesktopDrawer: React.FC<DesktopDrawerProps> = ({ sideNavigationItems }) => (
  <ul className="only-desktop mobile-nav mobile-nav--heading-style" role="navigation" aria-label="Primary">
    {sideNavigationItems.map((item, i) => (
      <MenuItem key={item.title} link={item} delay={i + 1} />
    ))}
  </ul>
);
