import React from 'react';

export const EarnestAppPromotion: React.FC<{ delay: number }> = ({ delay }) => (
  <li className={`mobile-nav__item appear-animation appear-delay-${delay} earnest-link`}>
    <a
      href="https://www.meetearnest.de/"
      target="_blank"
      className="mobile-nav__link mobile-nav__link--top-level"
      data-active="true"
      rel="noreferrer"
    >
      <img
        src="//cdn.shopify.com/s/files/1/0268/4819/8771/t/80/assets/app-icon.svg?v=10617531138236453087"
        width="50"
        height="40"
        alt="Earnest logo"
      />
      Earnest App
    </a>
  </li>
);
