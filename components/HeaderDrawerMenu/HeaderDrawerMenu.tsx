import React from 'react';
import { NavigationItem } from 'types';
import { DesktopDrawer } from './DesktopDrawer';
import { MobileDrawer } from './MobileDrawer';
import { MobileSocialSharing } from './MobileSocialSharing';

type HeaderDrawerMenuProps = {
  mainNavigationItems: NavigationItem[];
  sideNavigationItems: NavigationItem[];
};

export const HeaderDrawerMenu: React.FC<HeaderDrawerMenuProps> = ({ mainNavigationItems, sideNavigationItems }) => (
  <div className="drawer__inner">
    <DesktopDrawer sideNavigationItems={sideNavigationItems} />
    <MobileDrawer mainNavigationItems={mainNavigationItems} sideNavigationItems={sideNavigationItems} />
    <MobileSocialSharing delay={mainNavigationItems.length + sideNavigationItems.length + 1} />
  </div>
);
