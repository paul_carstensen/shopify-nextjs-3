import { createGlobalStyle } from 'styled-components';

/**
 * This is all the style the header & footer requires to display properly
 */
export const ShopifyGlobalStyle = createGlobalStyle`
  .h1, .h2, .h3, .h4, .h5, .h6 {
    display: block;
    margin: 0 0 7.5px;
  }
  @media only screen and (min-width: 769px) {
    .h1, .h2, .h3, .h4, .h5, .h6 {
      margin: 0 0 15px;
    }
  }

  .h1, .h2, .h3 {
    font-family: "Quicksand", sans-serif;
    font-weight: 400;
    letter-spacing: 0em;
    line-height: 1;
    text-transform: uppercase;
  }

  .h1 {
    font-size: 2em;
  }

  .h2 {
    font-size: 1.72em;
  }

  h3,
  .h3 {
    font-size: 1.4em;
  }

  .h4 {
    text-transform: uppercase;
    letter-spacing: 0.3em;
    font-size: 0.75em;
  }

  .h5 {
    text-transform: uppercase;
    letter-spacing: 0.3em;
    font-size: 0.75em;
    margin-bottom: 10px;
  }
  @media only screen and (max-width: 768px) {
    .h5 {
      margin-bottom: 5px;
    }
  }

  @media only screen and (max-width: 768px) {
    .small--hide {
      display: none !important;
    }
  }

  @media only screen and (min-width: 769px) {
    .medium-up--hide {
      display: none !important;
    }
  }

  .is-transitioning {
    display: block !important;
    visibility: visible !important;
  }

  .visually-hidden {
    clip: rect(0 0 0 0);
    clip: rect(0, 0, 0, 0);
    overflow: hidden;
    position: absolute;
    height: 1px;
    width: 1px;
  }

  .grid {
    list-style: none;
    margin: 0;
    padding: 0;
    margin-left: -22px;
  }
  .grid::after {
    content: "";
    display: table;
    clear: both;
  }
  @media only screen and (max-width: 768px) {
    .grid {
      margin-left: -17px;
    }
  }

  .grid__item {
    float: left;
    padding-left: 22px;
    width: 100%;
    min-height: 1px;
  }

  @media only screen and (max-width: 768px) {
    .grid__item {
      padding-left: 17px;
    }
  }
  .grid__item[class*="--push"] {
    position: relative;
  }

  .grid--flush-bottom {
    margin-bottom: -22px;
    overflow: auto;
  }
  .grid--flush-bottom > .grid__item {
    margin-bottom: 22px;
  }

  .grid--center {
    text-align: center;
  }

  .grid--center .grid__item {
    float: none;
    display: inline-block;
    vertical-align: top;
    text-align: left;
  }

  .one-half {
    width: 50%;
  }

  .one-third {
    width: 33.33333%;
  }

  .one-fifth {
    width: 20%;
  }

  .show {
    display: block !important;
  }

  .hide {
    display: none !important;
  }

  .text-left {
    text-align: left !important;
  }

  .text-center {
    text-align: center !important;
  }

  @media only screen and (min-width: 769px) {
    .medium-up--one-third {
      width: 33.33333%;
    }

    .medium-up--one-fifth {
      width: 20%;
    }

    .grid--uniform .medium-up--one-third:nth-of-type(3n + 1),
    .grid--uniform .medium-up--one-fifth:nth-of-type(5n + 1) {
      clear: both;
    }
  }

  .page-width {
    max-width: 1500px;
    margin: 0 auto;
  }
  @media only screen and (max-width: 959px) {
    .page-width.header-width {
      padding: 0px;
    }
  }

  .page-width {
    padding: 0 17px;
  }
  @media only screen and (min-width: 769px) {
    .page-width {
      padding: 0 40px;
    }
  }

  .text-link {
    display: inline;
    border: 0 none;
    background: none;
    padding: 0;
    margin: 0;
    color: #19232d;
    text-decoration: none;
    background: transparent;
  }
  @media only screen and (min-width: 769px) {
    .text-link:hover {
      color: #19232d;
    }
  }

  .btn--small {
    padding: 8px 14px;
    background-position: 150% 45%;
    min-width: 90px;
    font-size: 12px;
  }
  @media only screen and (max-width: 768px) {
    .btn--small {
      font-size: 10px;
    }
  }

  .btn--secondary.btn--small {
    font-weight: normal;
  }

  .btn--full {
    width: 100%;
    padding: 11px 20px;
    transition: none;
    padding: 13px 20px;
  }
  .btn--full.btn-no-side-padding {
    padding-left: 0px;
    padding-right: 0px;
  }

  
  .collapsible-trigger-btn {
    text-align: left;
    text-align: center;
    text-transform: uppercase;
    letter-spacing: 0.3em;
    font-size: 0.75em;
    display: block;
    width: 100%;
    padding: 17.14286px 0;
  }
  @media only screen and (max-width: 768px) {
    .collapsible-trigger-btn {
      padding: 15px 0;
    }
  }

  .collapsible-trigger-btn--borders {
    border: 1px solid #e6e6e6;
    border-bottom: 0;
    padding: 12px;
  }
  .collapsible-trigger-btn--borders .collapsible-trigger__icon {
    right: 12px;
  }
  @media only screen and (min-width: 769px) {
    .collapsible-trigger-btn--borders {
      padding: 15px;
    }
    .collapsible-trigger-btn--borders .collapsible-trigger__icon {
      right: 15px;
    }
  }
  .collapsible-content + .collapsible-trigger-btn--borders {
    margin-top: -1px;
  }
  .collapsible-trigger-btn--borders
    + .collapsible-content
    .collapsible-content__inner {
    font-size: 12px;
    border: 1px solid #e6e6e6;
    border-top: 0;
    padding: 0 20px 20px;
  }
  @media only screen and (min-width: 769px) {
    .collapsible-trigger-btn--borders
      + .collapsible-content
      .collapsible-content__inner {
      font-size: 13.6px;
    }
  }
  .collapsible-trigger-btn--borders + .collapsible-content--expanded {
    margin-bottom: 30px;
  }
  .collapsible-trigger-btn--borders + .collapsible-content--expanded:last-child {
    margin-bottom: -1px;
  }

  .collapsible-trigger-btn--borders-top {
    border-top: 1px solid #e6e6e6;
  }

  .add-to-cart.btn--secondary {
    border: 1px solid #19232d;
  }

  .icon {
    display: inline-block;
    width: 20px;
    height: 20px;
    vertical-align: middle;
    fill: currentColor;
  }
  .no-svg .icon {
    display: none;
  }

  .icon--full-color {
    fill: initial;
  }

  svg.icon:not(.icon--full-color) circle,
  svg.icon:not(.icon--full-color) ellipse,
  svg.icon:not(.icon--full-color) g,
  svg.icon:not(.icon--full-color) line,
  svg.icon:not(.icon--full-color) path,
  svg.icon:not(.icon--full-color) polygon,
  svg.icon:not(.icon--full-color) polyline,
  svg.icon:not(.icon--full-color) rect,
  symbol.icon:not(.icon--full-color) circle,
  symbol.icon:not(.icon--full-color) ellipse,
  symbol.icon:not(.icon--full-color) g,
  symbol.icon:not(.icon--full-color) line,
  symbol.icon:not(.icon--full-color) path,
  symbol.icon:not(.icon--full-color) polygon,
  symbol.icon:not(.icon--full-color) polyline,
  symbol.icon:not(.icon--full-color) rect {
    fill: inherit;
    stroke: inherit;
  }

  .icon-bag circle,
  .icon-bag ellipse,
  .icon-bag g,
  .icon-bag line,
  .icon-bag path,
  .icon-bag polygon,
  .icon-bag polyline,
  .icon-bag rect,
  .icon-search circle,
  .icon-search ellipse,
  .icon-search g,
  .icon-search line,
  .icon-search path,
  .icon-search polygon,
  .icon-search polyline,
  .icon-search rect,
  .icon-close circle,
  .icon-close ellipse,
  .icon-close g,
  .icon-close line,
  .icon-close path,
  .icon-close polygon,
  .icon-close polyline,
  .icon-close rect,
  .icon-chevron-down circle,
  .icon-chevron-down ellipse,
  .icon-chevron-down g,
  .icon-chevron-down line,
  .icon-chevron-down path,
  .icon-chevron-down polygon,
  .icon-chevron-down polyline,
  .icon-chevron-down rect,
  .icon-user circle,
  .icon-user ellipse,
  .icon-user g,
  .icon-user line,
  .icon-user path,
  .icon-user polygon,
  .icon-user polyline,
  .icon-user rect,
  .icon-hamburger circle,
  .icon-hamburger ellipse,
  .icon-hamburger g,
  .icon-hamburger line,
  .icon-hamburger path,
  .icon-hamburger polygon,
  .icon-hamburger polyline,
  .icon-hamburger rect  {
    fill: none !important;
    stroke-width: 2px;
    stroke: currentColor !important;
    stroke-linecap: miter;
    stroke-linejoin: miter;
  }

  .icon__fallback-text {
    clip: rect(0 0 0 0);
    clip: rect(0, 0, 0, 0);
    overflow: hidden;
    position: absolute;
    height: 1px;
    width: 1px;
  }

  .payment-icons {
    text-align: center;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: default;
    margin-bottom: 15px;
  }
  @media only screen and (min-width: 769px) {
    .payment-icons {
      margin-top: 30px;
      margin-bottom: 0;
    }
  }
  .payment-icons li {
    cursor: default;
    margin: 0 4px 0;
  }
  .payment-icons .icon {
    width: 40px;
    height: 40px;
  }
  .payment-icons .icon__fallback-text {
    text-transform: capitalize;
  }

  .js-drawer-open {
    overflow: hidden;
  }

  .drawer {
    display: none;
    position: fixed;
    overflow: hidden;
    -webkit-overflow-scrolling: touch;
    top: 0;
    bottom: 0;
    padding: 0 15px 15px;
    max-width: 95%;
    z-index: 30;
    color: #19232d;
    background-color: white;
    box-shadow: 0 0 150px rgba(0, 0, 0, 0.1);
    transition: transform 0.25s cubic-bezier(0.165, 0.84, 0.44, 1);
  }
  @media only screen and (min-width: 769px) {
    .drawer {
      padding: 0 30px 30px;
    }
  }
  .drawer a:not(.btn) {
    color: #19232d;
  }
  .drawer a:not(.btn):hover {
    color: #19232d;
  }
  .drawer input {
    border-color: #e6e6e6;
  }
  .drawer .btn {
    background-color: #19232d;
    color: white;
  }

  .drawer--left {
    width: 300px;
    left: -300px;
  }
  .drawer--left.drawer--is-open {
    display: block;
    transform: translateX(300px);
    transition-duration: 0.45s;
  }

  .drawer--right {
    width: 300px;
    right: -300px;
  }
  @media only screen and (min-width: 769px) {
    .drawer--right {
      width: 400px;
      right: -400px;
    }
  }
  .drawer--right.drawer--is-open {
    display: block;
    transform: translateX(-300px);
    transition-duration: 0.45s;
  }
  @media only screen and (min-width: 769px) {
    .drawer--right.drawer--is-open {
      transform: translateX(-400px);
    }
  }

  .drawer__header {
    display: table;
    height: 70px;
    width: 100%;
    padding: 11.53846px 0;
    margin-bottom: 0;
    border-bottom: 1px solid #e6e6e6;
  }
  @media only screen and (min-width: 769px) {
    .drawer__header {
      height: 119px;
    }
  }

  @media only screen and (min-width: 769px) {
    .drawer__header--full {
      padding-left: 30px;
      padding-right: 30px;
    }
  }

  .drawer__fixed-header {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: 70px;
    overflow: visible;
  }
  @media only screen and (min-width: 769px) {
    .drawer__fixed-header {
      height: 119px;
    }
  }

  @media only screen and (min-width: 769px) {
    .drawer__fixed-header:not(.drawer__fixed-header--full) {
      left: 30px;
      right: 30px;
    }
  }
  .drawer__title,
  .drawer__close {
    display: table-cell;
    vertical-align: middle;
  }

  .drawer__title {
    width: 100%;
  }
  @media only screen and (max-width: 768px) {
    .drawer__title {
      padding-left: 15px;
    }
  }

  .drawer__close {
    width: 1%;
    text-align: center;
  }

  .drawer__close-button {
    position: relative;
    height: 100%;
    padding: 0 15px;
    color: inherit;
  }
  .drawer__close-button:active {
    background-color: #f2f2f2;
  }
  .drawer__close-button .icon {
    height: 28px;
    width: 28px;
  }
  @media only screen and (min-width: 769px) {
    .drawer__close-button {
      right: -30px;
    }
  }

  .drawer__close--left {
    text-align: left;
  }
  .drawer__close--left .drawer__close-button {
    right: auto;
    left: -30px;
  }

  .drawer__inner {
    position: absolute;
    top: 70px;
    bottom: 0;
    left: 0;
    right: 0;
    padding: 15px 15px 0;
    overflow: auto;
    -webkit-overflow-scrolling: touch;
  }
  @media only screen and (min-width: 769px) {
    .drawer__inner {
      top: 119px;
      padding-left: 30px;
      padding-right: 30px;
    }
  }
  .drawer--has-fixed-footer .drawer__inner {
    overflow: hidden;
    overflow-y: auto;
  }

  .drawer__inner--has-fixed-footer {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    margin: 0;
    padding: 15px 15px 0;
    bottom: 130px;
    overflow: auto;
    -webkit-overflow-scrolling: touch;
  }
  @media only screen and (min-width: 769px) {
    .drawer__inner--has-fixed-footer {
      padding: 22.22222px 30px 0;
    }
  }
  @media screen and (max-height: 400px) {
    .drawer__inner--has-fixed-footer {
      position: static;
      padding: 0;
    }
  }

  .drawer__footer {
    border-top: 1px solid #e6e6e6;
    padding-top: 15px;
  }
  @media only screen and (min-width: 769px) {
    .drawer__footer {
      padding-top: 22.22222px;
    }
  }

  .drawer__footer--fixed {
    position: absolute;
    bottom: 0;
    left: 15px;
    right: 15px;
    min-height: 130px;
    padding-bottom: 30px;
  }
  @media only screen and (max-width: 768px) {
    .drawer__footer--fixed {
      padding-bottom: 15px;
    }
  }
  @media only screen and (min-width: 769px) {
    .drawer__footer--fixed {
      left: 30px;
      right: 30px;
    }
  }
  @media screen and (max-height: 400px) {
    .drawer__footer--fixed {
      position: static;
    }
  }

  #CartDrawer,
  #NavDrawer,
  #LoginDrawer {
    z-index: 100000000000000;
  }

  @keyframes overlay-on {
    from {
      opacity: 0;
    }

    to {
      opacity: 0.6;
    }
  }

  @keyframes overlay-off {
    from {
      opacity: 0.6;
    }

    to {
      opacity: 0;
    }
  }

  @keyframes shine {
    100% {
      left: -200%;
    }
  }

  .appear-animation {
    opacity: 0;
    transform: translateY(60px);
  }

  .appear-delay-1 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.075s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.15s;
  }

  .appear-delay-2 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.195s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.27s;
  }

  .appear-delay-3 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.255s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.33s;
  }

  .appear-delay-4 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.315s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.39s;
  }

  .appear-delay-5 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.375s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.45s;
  }

  .appear-delay-6 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.435s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.51s;
  }

  .appear-delay-7 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.495s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.57s;
  }

  .appear-delay-8 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.555s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.63s;
  }

  .appear-delay-9 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.615s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.69s;
  }

  .appear-delay-10 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.675s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.75s;
  }

  .appear-delay-11 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.735s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.81s;
  }

  .appear-delay-12 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.795s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.87s;
  }

  .appear-delay-13 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.855s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.93s;
  }

  .appear-delay-14 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.915s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.99s;
  }

  .appear-delay-15 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 0.975s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 1.05s;
  }

  .appear-delay-16 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 1.035s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 1.11s;
  }

  .appear-delay-17 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 1.095s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 1.17s;
  }

  .appear-delay-18 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 1.155s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 1.23s;
  }

  .appear-delay-19 {
    transition: transform 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 1.215s,
      opacity 0.85s cubic-bezier(0.165, 0.84, 0.44, 1) 1.29s;
  }

  .js-drawer-open .appear-animation {
    opacity: 1;
    transform: translateY(0px);
  }

  .js-drawer-closing .appear-animation {
    transition-duration: 0s;
    transition-delay: 0.5s;
  }

  .footer-promotions {
    background-color: #fafafa;
    margin: 0;
    text-align: center;
  }

  @media only screen and (max-width: 768px) {
    .footer-promotions .grid__item {
      margin-bottom: 32px;
    }
    .footer-promotions .grid__item:last-child {
      margin-bottom: 0;
    }
  }
  .footer-promotions .grid--flush-bottom .grid__item {
    display: grid;
    grid-gap: 1rem;
    margin: 25px 0;
    padding: 0;
    grid-template-columns: 1fr 1fr;
    align-items: center;
  }
  .footer-promotions .grid--flush-bottom .grid__item .footer-promotions-icon {
    text-align: right;
  }
  .footer-promotions .grid--flush-bottom .grid__item .footer-promotions-icon img {
    display: inline-block;
    max-width: 64px;
    min-width: 50px;
  }
  .footer-promotions .grid--flush-bottom .grid__item .article__grid-image,
  .footer-promotions .grid--flush-bottom .grid__item p.h3 {
    margin-bottom: 0;
    text-align: left;
  }
  @media only screen and (max-width: 768px) {
    .footer-promotions .grid--flush-bottom .grid__item {
      grid-template-columns: 1fr;
    }
    .footer-promotions .grid--flush-bottom .grid__item .footer-promotions-icon {
      text-align: center;
    }
    .footer-promotions
      .grid--flush-bottom
      .grid__item
      .footer-promotions-icon
      img {
      max-width: 100px;
    }
    .footer-promotions .grid--flush-bottom .grid__item p.h3 {
      text-align: center;
    }
  }

  .site-footer {
    padding-bottom: 30px;
    background-color: #19232d;
    color: white;
  }
  @media only screen and (min-width: 769px) {
    .site-footer {
      padding-top: 60px;
      padding-bottom: 60px;
    }
  }
  .site-footer .footer__collapsible {
    font-size: 13.6px;
  }
  @media only screen and (min-width: 769px) {
    .site-footer select,
    .site-footer input {
      font-size: 13.6px;
    }
  }
  @media only screen and (max-width: 768px) {
    .site-footer {
      text-align: center;
      overflow: hidden;
      padding-bottom: 0;
    }
    .site-footer .grid__item {
      padding-bottom: 5px;
    }
    .site-footer .grid__item:after {
      content: "";
      border-bottom: 1px solid white;
      opacity: 0.12;
      display: block;
    }
    .site-footer .grid__item:first-child {
      padding-top: 7.5px;
    }
    .site-footer .grid__item:last-child:after {
      display: none;
    }
  }

  .site-footer a {
    color: white;
  }

  .site-footer a:hover {
    color: white;
  }

  .footer__small-text {
    font-size: 12px;
    padding: 7.5px 0;
    margin: 0;
    text-align: center;
  }
  ul + .footer__small-text {
    padding-top: 15px;
  }

  .footer__title {
    color: white;
  }
  @media only screen and (min-width: 769px) {
    .footer__title {
      margin-bottom: 20px;
    }
  }
  @media only screen and (max-width: 768px) {
    .footer__title {
      text-align: center;
    }
  }

  .uptodate-footer-grid {
    display: grid;
    grid-template-areas: "logo" "uptodate" "service" "produkte" "blog" "sonstiges" "earnest" "sicherheit" "bezahlen" "social";
  }
  .uptodate-footer-grid .uber-uns {
    grid-area: uber-uns;
    text-align: center;
  }
  .uptodate-footer-grid .uptodate {
    grid-area: uptodate;
  }
  .uptodate-footer-grid .service {
    grid-area: service;
  }
  .uptodate-footer-grid .sonstiges {
    grid-area: sonstiges;
  }
  .uptodate-footer-grid .sicherheit {
    grid-area: sicherheit;
    margin-bottom: 16px;
  }
  .uptodate-footer-grid .earnest-app {
    grid-area: earnest;
    margin: 16px;
  }
  .uptodate-footer-grid .earnest-app a:first-child {
    margin-right: 10px;
  }
  .uptodate-footer-grid .earnest-app a:last-child {
    margin-left: 10px;
  }
  .uptodate-footer-grid .bezahlen {
    grid-area: bezahlen;
  }
  .uptodate-footer-grid .social {
    grid-area: social;
  }
  .uptodate-footer-grid .footer__title {
    font-weight: bold;
    margin-bottom: 16px;
  }
  .uptodate-footer-grid .payment-icons {
    text-align: left;
    margin-top: 0;
  }
  .uptodate-footer-grid .social .footer__title,
  .uptodate-footer-grid .uber-uns .footer__title {
    text-align: center;
  }
  .uptodate-footer-grid .logo {
    grid-area: logo;
    text-align: center;
    margin-top: -6px;
  }

  .copyright {
    border-top: #353f47 1px solid;
  }

  .site-footer {
    padding: 0;
  }

  @media only screen and (min-width: 769px) {
    .uptodate-footer-grid {
      display: grid;
      grid-template-areas: "logo logo logo logo logo" "uptodate service produkte blog sonstiges" "sicherheit earnest social bezahlen bezahlen";
      grid-template-columns: 1fr 1fr 2fr 1fr 1fr;
      row-gap: 2rem;
    }
    .uptodate-footer-grid .sonstiges,
    .uptodate-footer-grid .bezahlen,
    .uptodate-footer-grid .service {
      margin-left: 25%;
      width: 50%;
    }
    .uptodate-footer-grid .service {
      width: 85%;
    }
    .uptodate-footer-grid .earnest-app {
      margin: initial;
      margin-left: 25%;
    }
    .uptodate-footer-grid .earnest-app a:first-child {
      margin-right: initial;
    }
    .uptodate-footer-grid .earnest-app a:last-child {
      margin-left: initial;
    }
    .uptodate-footer-grid .produkte {
      margin: 0 auto;
    }
  }

  .site-footer__linklist {
    margin: 0;
  }
  .site-footer__linklist a {
    display: inline-block;
    padding: 4px 0;
  }

  .footer__logo {
    margin: 15px 0;
  }
  @media only screen and (min-width: 769px) {
    .footer__logo {
      margin: 0 0 20px;
    }
  }
  .footer__logo a {
    display: block;
  }
  .footer__logo img {
    display: inline-block;
    transform: translateZ(0);
    max-height: 100%;
  }

  .footer__social {
    margin: 0;
  }
  form + .footer__social {
    margin-top: 30px;
  }
  .footer__social li {
    display: inline-block;
    margin: 0 15px 15px 0;
  }
  .footer__social a {
    display: block;
  }
  .footer__social .icon {
    width: 22px;
    height: 22px;
  }
  @media only screen and (min-width: 769px) {
    .footer__social .icon {
      width: 24px;
      height: 24px;
    }
  }
  .footer__social .icon.icon--wide {
    width: 40px;
  }

  @media only screen and (max-width: 768px) {
    .footer__collapsible {
      padding: 0 0 15px 0;
    }
  }

  @media only screen and (max-width: 768px) {
    .footer_collapsible--disabled {
      padding-top: 15px;
    }
  }

  .collapsible-content__inner p a:after {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 0;
    width: 0%;
    border-bottom: 2px solid white;
    transition: width 0.5s ease;
  }
  .collapsible-content__inner p a {
    position: relative;
    text-decoration: none;
    border-bottom: 2px solid rgba(255, 255, 255, 0.1);
  }
  .collapsible-content__inner p a:hover:after,
  .collapsible-content__inner p a:focus:after {
    width: 100%;
  }

  .header-layout {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: justify;
    justify-content: space-between;
  }

  .header-layout--center {
    -ms-flex-align: center;
    align-items: center;
  }

  .header-item {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
  }

  .header-item--logo {
    -ms-flex: 0 0 auto;
    flex: 0 0 auto;
  }

  .header-item--icons {
    -ms-flex-pack: end;
    justify-content: flex-end;
    -ms-flex: 0 1 auto;
    flex: 0 1 auto;
  }

  .header-layout--left-center .header-item--logo,
  .header-layout--left-center .header-item--icons {
    -ms-flex: 0 0 200px;
    flex: 0 0 200px;
    max-width: 50%;
  }
  @media only screen and (min-width: 769px) {
    .header-layout--left-center .header-item--logo,
    .header-layout--left-center .header-item--icons {
      min-width: 130px;
    }
  }

  @media only screen and (min-width: 769px) {
    .header-layout[data-logo-align="center"] .header-item--logo {
      margin: 0px;
      margin-left: 10px;
      margin-right: 10px;
    }
  }
  .header-layout[data-logo-align="center"] .header-item--navigation,
  .header-layout[data-logo-align="center"] .header-item--icons {
    -ms-flex: 1 1 150px;
    flex: 1 1 150px;
  }
  @media only screen and (max-width: 768px) {
    .header-layout[data-logo-align="center"] .header-item--navigation,
    .header-layout[data-logo-align="center"] .header-item--icons {
      -ms-flex: 1 1 100%;
      flex: 1 1 100%;
    }
  }

  .site-header__logo-link > img {
    max-height: 8rem;
  }

  .header-layout[data-logo-align="left"] .site-header__logo {
    margin-right: 10px;
  }

  .header-item--logo-split {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: center;
    justify-content: center;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex: 1 1 100%;
    flex: 1 1 100%;
  }
  .header-item--logo-split .header-item:not(.header-item--logo) {
    text-align: center;
    -ms-flex: 1 1 20%;
    flex: 1 1 20%;
    justify-content: flex-start;
  }

  ul.site-nav.site-navigation.small--hide {
    display: flex;
    justify-content: space-around;
    width: 100%;
  }
  @media screen and (max-width: 1300px) {
    ul.site-nav.site-navigation.small--hide {
      flex-wrap: wrap;
    }
  }
  @media screen and (max-width: 1024px) {
    ul.site-nav.site-navigation.small--hide {
      display: none;
    }
  }

  .header-item--split-left {
    -ms-flex-pack: end;
    justify-content: flex-end;
  }

  .header-item--left .site-nav {
    margin-left: -12px;
  }
  @media only screen and (max-width: 768px) {
    .header-item--left .site-nav {
      margin-left: -5px;
    }
  }

  .header-item--icons .site-nav {
    margin-right: -12px;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-top: 21px;
    justify-content: center;
  }
  .header-item--icons .site-nav .status-login-text {
    font-size: 13px;
    text-transform: uppercase;
  }
  @media only screen and (max-width: 768px) {
    .header-item--icons .site-nav {
      margin-right: -5px;
    }
  }

  .site-header {
    position: relative;
    padding: 0px 0;
    background: white;
  }
  @media only screen and (min-width: 769px) {
    .toolbar + .header-sticky-wrapper .site-header {
      border-top: 1px solid rgba(25, 35, 45, 0.1);
    }
  }

  .site-header--stuck {
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    transform: translate3d(0, -100%, 0);
    transition: none;
    z-index: 20;
  }
  .js-drawer-open--search .site-header--stuck {
    z-index: 28;
  }
  @media only screen and (min-width: 769px) {
    .site-header--stuck {
      padding: 10px 0;
    }
  }

  @media screen and (min-width: 700px) and (max-height: 550px) {
    .site-header--stuck {
      position: static;
    }
  }
  .site-header--opening {
    transform: translate3d(0, 0, 0);
    transition: transform 0.4s cubic-bezier(0.165, 0.84, 0.44, 1);
  }

  .site-header__logo {
    position: relative;
    margin: 10px 0;
    display: block;
    font-size: 30px;
    z-index: 6;
  }
  @media only screen and (min-width: 769px) {
    .text-center .site-header__logo {
      padding-right: 0;
      margin: 10px auto;
    }
  }
  .header-layout[data-logo-align="center"] .site-header__logo {
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    margin: 0;
  }
  .site-header__logo a {
    max-width: 100%;
  }
  .site-header__logo a,
  .site-header__logo a:hover {
    text-decoration: none;
  }
  .site-header__logo img {
    display: block;
  }
  .header-layout[data-logo-align="center"] .site-header__logo img {
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 50px;
  }

  .header-section {
    padding: 10px 17px;
  }

  @media only screen and (min-width: 769px) {
    .header-section {
      padding: 40px 17px;
    }
  }

  .site-header__logo-link {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    color: #19232d;
  }
  .site-header__logo-link:hover {
    color: #19232d;
  }
  @media only screen and (max-width: 768px) {
    .site-header__logo-link {
      margin: 0 auto;
    }
  }

  .header-sticky-wrapper {
    position: relative;
  }

  .header-wrapper--sticky {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    z-index: 6;
    background: none;
    background: linear-gradient(
      to bottom,
      rgba(0, 0, 0, 0.3) 0%,
      rgba(0, 0, 0, 0) 100%
    );
  }
  .header-wrapper--sticky .site-header:not(.site-header--stuck) {
    background: none;
  }
  .js-drawer-open--search .header-wrapper--sticky {
    z-index: 28;
  }

  .site-header__search-container {
    position: absolute;
    left: 0;
    right: 0;
    bottom: 200%;
    height: 100%;
    z-index: 28;
    overflow: hidden;
    transition: all 0.3s cubic-bezier(0, 0, 0.38, 1);
  }
  .site-header__search-container.is-active {
    overflow: visible;
    bottom: 0;
    transition: none;
  }

  .site-header__search {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    z-index: 28;
    display: -ms-flexbox;
    display: flex;
    transform: translate3d(0, -110%, 0);
    background-color: white;
    color: #19232d;
  }
  .site-header__search .page-width {
    -ms-flex: 1 1 100%;
    flex: 1 1 100%;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: stretch;
    align-items: stretch;
  }
  @media only screen and (max-width: 768px) {
    .site-header__search .page-width {
      padding: 0;
    }
  }
  .is-active .site-header__search {
    transform: translate3d(0, 0, 0);
  }
  .site-header__search .icon {
    width: 30px;
    height: 30px;
  }

  .site-header__search-form {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    display: -ms-flexbox;
    display: flex;
    align-items: center;
  }
  @media only screen and (min-width: 769px) {
    .site-header__search-form {
      padding: 15px 0;
    }
  }

  .site-header__search-input {
    border: 0;
    width: 100px;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
  }
  .site-header__search-input:focus {
    border: 0;
    outline: 0;
  }

  .site-header__search-btn {
    padding: 0 15px;
  }

  @media only screen and (min-width: 769px) {
    .site-header__search-btn--submit {
      padding: 0 15px 0 0;
    }
    .site-header__search-btn--submit .icon {
      position: relative;
      top: -1px;
      width: 28px;
      height: 28px;
    }
  }
  .predictive-results {
    position: absolute;
    top: 100%;
    left: 0;
    right: 0;
    background-color: white;
    color: #19232d;
    max-height: 70vh;
    max-height: calc(90vh - 100%);
    overflow: auto;
    box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.09);
  }
  @media only screen and (min-width: 769px) {
    .predictive-results {
      max-height: calc(100vh - 100% - 33px);
    }
  }

  .predictive__label {
    border-bottom: 1px solid #e6e6e6;
    padding-bottom: 5px;
    margin-bottom: 20px;
  }

  .predictive-result__layout {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    padding: 10px;
    margin-left: -10px;
    margin-right: -10px;
  }
  .predictive-result__layout > div {
    margin: 0 10px 30px;
  }
  .predictive-result__layout > div:last-child {
    margin-bottom: 0;
  }
  .predictive-result__layout [data-type-products] {
    -ms-flex: 1 1 60%;
    flex: 1 1 60%;
    margin-bottom: 0;
  }
  .predictive-result__layout [data-type-pages] {
    -ms-flex: 1 1 200px;
    flex: 1 1 200px;
  }
  .predictive-result__layout [data-type-articles] {
    -ms-flex: 1 1 60%;
    flex: 1 1 60%;
  }

  .predictive-results__footer {
    padding: 0 0 30px;
  }

  .toolbar__social {
    text-align: right;
  }
  .toolbar__social a {
    display: block;
    padding: 5px;
  }
  .toolbar__social .icon {
    position: relative;
    top: -2px;
    width: 16px;
    height: 16px;
  }

  .section-header {
    margin-bottom: 30px;
    text-align: center;
  }
  @media only screen and (min-width: 769px) {
    .section-header {
      margin-bottom: 50px;
    }
  }
  .section-header select {
    display: inline-block;
    vertical-align: middle;
  }
  .section-header.no-margin {
    margin: 0px;
  }

  .section-header--flush {
    margin-bottom: 0;
  }

  .section-header--with-link {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
  }
  .section-header--with-link select {
    -ms-flex: 0 1 auto;
    flex: 0 1 auto;
  }

  .section-header--hero {
    position: relative;
    -ms-flex: 1 1 100%;
    flex: 1 1 100%;
    color: white;
    margin-bottom: 0;
  }
  .section-header--hero a {
    color: white;
  }

  .section-header__shadow {
    position: relative;
    display: inline-block;
  }
  .section-header__shadow:before {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: radial-gradient(rgba(0, 0, 0, 0.2) 0%, rgba(0, 0, 0, 0) 60%);
    margin: -100px -200px -100px -200px;
    z-index: -1;
  }
  .section-header__shadow .section-header__title {
    position: relative;
  }

  .section-header__title {
    margin-bottom: 0;
    font-size: 32px;
  }
  .section-header--with-link .section-header__title {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
  }
  @media only screen and (min-width: 769px) {
    .section-header--hero .section-header__title {
      font-size: 2.9em;
    }
  }

  .section-header__link {
    -ms-flex: 0 1 auto;
    flex: 0 1 auto;
    margin-top: 15px;
  }
  .section-header--with-link .section-header__link {
    margin-top: 0;
  }

  h2 ~ .section-header__link,
  .h2 ~ .section-header__link {
    margin-top: 2rem;
  }

  .section-header--404 {
    margin-bottom: 0;
    padding: 80px 0;
  }

  .section-header select {
    margin: 10px 0;
  }

  .section-header p {
    margin: 10px 0;
  }

  .site-nav {
    margin: 0;
  }

  .text-center .site-navigation {
    margin: 0 auto;
  }
  .header-layout--left .site-navigation {
    padding-left: 10px;
  }

  .site-nav__icons {
    white-space: nowrap;
  }

  .site-nav__item {
    position: relative;
    display: inline-block;
    margin: 0;
  }
  .site-nav__item li {
    display: block;
  }
  .site-nav__item .icon-chevron-down {
    width: 10px;
    height: 10px;
  }

  .site-nav__link {
    display: inline-block;
    vertical-align: middle;
    text-decoration: none;
    padding: 5px 10px;
    white-space: nowrap;
    color: #19232d;
  }
  .site-header--heading-style .site-nav__link {
    font-family: "Quicksand", sans-serif;
    font-weight: 400;
    letter-spacing: 0em;
    line-height: 1;
    text-transform: uppercase;
  }
  .site-nav__link:hover {
    color: #19232d;
  }
  .site-nav--has-dropdown > .site-nav__link {
    position: relative;
    z-index: 6;
  }
  .site-nav__link .icon-chevron-down {
    margin-left: 5px;
  }
  @media only screen and (max-width: 959px) {
    .site-nav__link {
      padding: 5px;
    }
    .header-layout--center .site-nav__link {
      padding-left: 2px;
      padding-right: 2px;
    }
  }

  .site-nav__link--underline {
    position: relative;
  }
  .site-nav__link--underline:after {
    content: "";
    display: block;
    position: absolute;
    bottom: 0;
    left: 0;
    right: 100%;
    margin: 0 10px;
    border-bottom: 2px solid #19232d;
    transition: right 0.5s;
  }
  .site-nav--has-dropdown .site-nav__link--underline:after {
    border-bottom-color: #19232d;
  }
  .site-nav__item:hover .site-nav__link--underline:after {
    right: 0;
  }

  .site-nav--has-dropdown {
    z-index: 6;
  }
  .site-nav--has-dropdown.is-focused,
  .site-nav--has-dropdown:hover {
    z-index: 7;
  }

  .site-nav--has-dropdown.is-focused > a,
  .site-nav--has-dropdown:hover > a {
    color: #19232d !important;
    background-color: white;
    opacity: 1;
    transition: none;
  }

  .site-nav__link--icon {
    padding-left: 12px;
    padding-right: 12px;
  }
  @media only screen and (max-width: 768px) {
    .site-nav__link--icon {
      padding-left: 5px;
      padding-right: 5px;
    }
    .site-nav__link--icon + .site-nav__link--icon {
      margin-left: -4px;
    }
  }
  .site-nav__link--icon .icon {
    width: 30px;
    height: 30px;
  }

  .site-nav__dropdown {
    position: absolute;
    left: 0;
    margin: 0;
    z-index: 5;
    display: block;
    visibility: hidden;
    background-color: white;
    min-width: 100%;
    padding: 10px 0 5px;
    box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.09);
    transform: translate3d(0px, -12px, 0px);
  }
  .site-nav--has-dropdown:hover .site-nav__dropdown,
  .is-focused > .site-nav__dropdown {
    display: block;
    visibility: visible;
    transform: translate3d(0px, 0px, 0px);
    transition: all 300ms cubic-bezier(0.2, 0.06, 0.05, 0.95);
  }
  .site-nav__dropdown li {
    margin: 0;
  }
  .site-nav__dropdown > li {
    position: relative;
  }
  .site-nav__dropdown > li > a {
    position: relative;
    z-index: 6;
  }
  .site-nav__dropdown a {
    background-color: white;
  }
  .site-nav__dropdown-link--has-children:hover,
  .site-nav__dropdown-link--has-children:focus {
    background-color: #f2f2f2;
  }


  .site-nav__dropdown .grid .grid__item.medium-up--one-fifth {
    width: 14%;
  }

  .mobile-nav {
    margin: -15px -15px 0 -15px;
  }
  @media only screen and (min-width: 769px) {
    .mobile-nav {
      margin-left: -30px;
      margin-right: -30px;
    }
  }
  .mobile-nav li {
    margin-bottom: 0;
    list-style: none;
  }

  .mobile-nav__search {
    padding: 15px;
  }

  .mobile-nav__item {
    position: relative;
    display: block;
  }
  .mobile-nav > .mobile-nav__item {
    background-color: white;
  }
  .mobile-nav__item:after {
    content: "";
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    border-bottom: 1px solid #e6e6e6;
  }

  .mobile-nav__link,
  .mobile-nav__faux-link {
    display: block;
  }

  .mobile-nav__link--top-level {
    font-size: 1.4em;
    font-size: 13px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.85;
    letter-spacing: 3px;
    border-bottom: 1px solid #e6e6e6;
  }
  .mobile-nav--heading-style .mobile-nav__link--top-level {
    font-family: "Quicksand", sans-serif;
    font-weight: 400;
    letter-spacing: 0em;
    line-height: 1;
    text-transform: uppercase;
  }

  .mobile-nav__link,
  .mobile-nav__faux-link,
  .mobile-nav__toggle button,
  .mobile-nav__toggle .faux-button {
    color: #19232d;
    padding: 15px;
    text-decoration: none;
  }
  .mobile-nav__link:active,
  .mobile-nav__faux-link:active,
  .mobile-nav__toggle button:active,
  .mobile-nav__toggle .faux-button:active {
    color: black;
  }
  .mobile-nav__link:active,
  .mobile-nav__faux-link:active,
  .mobile-nav__toggle button:active,
  .mobile-nav__toggle .faux-button:active {
    background-color: #f2f2f2;
  }

  .mobile-nav__child-item {
    display: -ms-flexbox;
    display: flex;
  }
  .mobile-nav__child-item a,
  .mobile-nav__child-item .mobile-nav__link {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
  }
  .mobile-nav__child-item .collapsible-trigger:not(.mobile-nav__link--button) {
    -ms-flex: 0 0 43px;
    flex: 0 0 43px;
  }
  .mobile-nav__child-item .collapsible-trigger__icon {
    padding: 0;
    margin-right: 15px;
  }

  .mobile-nav__item--secondary a {
    padding-top: 10px;
    padding-bottom: 5px;
  }
  .mobile-nav__item--secondary:after {
    display: none;
  }

  .mobile-nav__item:not(.mobile-nav__item--secondary)
    + .mobile-nav__item--secondary {
    margin-top: 10px;
  }

  .mobile-nav__has-sublist,
  .mobile-nav__link--button {
    display: -ms-flexbox;
    display: flex;
  }
  .mobile-nav__has-sublist > *,
  .mobile-nav__link--button > * {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    word-break: break-word;
  }

  .mobile-nav__link--button {
    width: 100%;
    text-align: left;
    padding: 0;
  }

  .mobile-nav__toggle {
    -ms-flex: 0 1 auto;
    flex: 0 1 auto;
  }
  .mobile-nav__toggle .icon {
    width: 16px;
    height: 16px;
  }
  .mobile-nav__toggle button,
  .mobile-nav__toggle .faux-button {
    height: 60%;
    padding: 0 30px;
    margin: 20% 0;
  }
  .mobile-nav__toggle button {
    border-left: 1px solid #e6e6e6;
  }

  .mobile-nav__sublist {
    margin: 0;
  }
  .mobile-nav__sublist .mobile-nav__item:after {
    top: 0;
    bottom: auto;
    border-bottom: none;
  }
  .mobile-nav__sublist .mobile-nav__item:last-child {
    padding-bottom: 15px;
  }
  .mobile-nav__sublist .mobile-nav__link,
  .mobile-nav__sublist .mobile-nav__faux-link {
    font-weight: normal;
    padding: 7.5px 25px 7.5px 15px;
  }

  .mobile-nav__grandchildlist {
    margin: 0;
  }
  .mobile-nav__grandchildlist:before {
    content: "";
    display: block;
    position: absolute;
    width: 1px;
    background: #000;
    left: 17px;
    top: 10px;
    bottom: 10px;
  }
  .mobile-nav__grandchildlist .mobile-nav__item:last-child {
    padding-bottom: 0;
  }
  .mobile-nav__grandchildlist .mobile-nav__link {
    padding-left: 35px;
  }

  .mobile-nav__social {
    list-style: none outside;
    display: -ms-flexbox;
    display: flex !important;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    -ms-flex-pack: center;
    justify-content: center;
    margin: 15px 0 20px 0;
  }
  @media only screen and (min-width: 769px) {
    .mobile-nav__social {
      margin-left: -15px;
      margin-right: -15px;
    }
  }
  .mobile-nav__social a {
    display: block;
    padding: 8px 15px;
  }
  .mobile-nav__social a .icon {
    position: relative;
    top: -1px;
  }

  .mobile-nav__social-item {
    -ms-flex: 0 1 33.33%;
    flex: 0 1 33.33%;
    text-align: center;
    margin: 0 0 -1px;
  }
  .mobile-nav__social-item:nth-child(3n-1) {
    margin-right: -1px;
    margin-left: -1px;
  }

  @media only screen and (min-width: 769px) {
    .site-nav__link--icon .icon {
      width: 28px;
      height: 28px;
    }
    .site-nav__link--icon .icon.icon-user {
      position: relative;
      top: 1px;
    }
  }
  .site-nav__link--icon .icon.icon-user circle {
    fill: inherit !important;
  }
  .site-nav__link--icon .icon.icon-user path {
    stroke: #ffffff !important;
  }

  .cart-link {
    position: relative;
    display: block;
  }

  .cart-link__bubble {
    display: none;
  }

  .cart-link__bubble--visible {
    display: flex;
    position: absolute;
    top: 50%;
    right: 0px;
    width: 15px;
    height: 15px;
    background-color: #76ebb9;
    border: 2px solid white;
    border-radius: 50%;
    justify-content: center;
    align-items: center;
  }
  .cart-link__bubble--visible .cart-link__bubble--count {
    font-size: 9px;
  }
  @media only screen and (min-width: 769px) {
    .cart-link__bubble--visible .cart-link__bubble--count {
      display: none;
    }
  }

  .megamenu {
    padding: 39px 0;
    line-height: 1.8;
    transform: none;
    opacity: 0;
    transition: all 300ms cubic-bezier(0.2, 0.06, 0.05, 0.95);
    transition-delay: 0.3s;
  }
  .site-nav--has-dropdown:hover .megamenu,
  .is-focused > .megamenu {
    opacity: 1;
    transition-delay: 0s;
  }
  .site-nav--has-dropdown:hover .megamenu .appear-animation,
  .is-focused > .megamenu .appear-animation {
    opacity: 1;
    transform: none;
  }

  .site-nav--is-megamenu.site-nav__item {
    position: static;
  }

  .megamenu__colection-image {
    display: block;
    background-repeat: no-repeat;
    background-position: top center;
    background-size: cover;
    height: 0;
    padding-bottom: 46%;
    margin-bottom: 20px;
  }

  .text-center .megamenu .grid {
    text-align: center;
  }
  .text-center .megamenu .grid .grid__item {
    float: none;
    display: inline-block;
    vertical-align: top;
    text-align: left;
  }

  .drawer {
    z-index: 10000;
  }

  .collapsible-trigger {
    color: inherit;
    position: relative;
  }

  .collapsible-trigger__icon {
    display: block;
    position: absolute;
    right: 0;
    top: 50%;
    width: 10px;
    height: 10px;
    transform: translateY(-50%);
  }
  @media only screen and (min-width: 769px) {
    .collapsible-trigger__icon {
      width: 12px;
      height: 12px;
    }
  }
  .mobile-nav__has-sublist .collapsible-trigger__icon {
    right: 25px;
  }
  .collapsible-trigger__icon .icon {
    display: block;
    width: 10px;
    height: 10px;
    transition: all 0.1s ease-in;
  }
  @media only screen and (min-width: 769px) {
    .collapsible-trigger__icon .icon {
      width: 12px;
      height: 12px;
    }
  }

  .collapsible-trigger__icon--circle {
    border: 1px solid #e6e6e6;
    width: 28px;
    height: 28px;
    border-radius: 28px;
    text-align: center;
  }
  .collapsible-trigger__icon--circle .icon {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  .collapsible-trigger.is-open .collapsible-trigger__icon > .icon-chevron-down {
    transform: scaleY(-1);
  }

  .collapsible-trigger.is-open
    .collapsible-trigger__icon--circle
    > .icon-chevron-down {
    transform: translate(-50%, -50%) scaleY(-1);
  }

  .collapsible-content {
    transition: opacity 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94),
      height 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94);
  }
  .collapsible-content.is-open {
    visibility: visible;
    opacity: 1;
    transition: opacity 1s cubic-bezier(0.25, 0.46, 0.45, 0.94),
      height 0.35s cubic-bezier(0.25, 0.46, 0.45, 0.94);
  }

  .collapsible-content--all {
    visibility: hidden;
    overflow: hidden;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    opacity: 0;
    height: 0;
  }
  .collapsible-content--all .collapsible-content__inner {
    transform: translateY(40px);
  }
  .collapsible-content--all .collapsible-content__inner--no-translate {
    transform: translateY(0);
  }

  @media only screen and (max-width: 768px) {
    .collapsible-content--small {
      visibility: hidden;
      -webkit-backface-visibility: hidden;
      backface-visibility: hidden;
      opacity: 0;
      height: 0;
    }
    .collapsible-content--small .collapsible-content__inner {
      transform: translateY(40px);
    }
    .collapsible-content--small .collapsible-content__inner--no-translate {
      transform: translateY(0);
    }
  }
  .collapsible-content__inner {
    transition: transform 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94);
  }
  .is-open .collapsible-content__inner {
    transform: translateY(0);
    transition: transform 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94);
  }

  .collapsible-trigger[aria-expanded="true"] .collapsible-label__closed {
    display: none;
  }

  .collapsible-label__open {
    display: none;
  }
  .collapsible-trigger[aria-expanded="true"] .collapsible-label__open {
    display: inline-block;
  }

  .collapsible-content--sidebar {
    visibility: hidden;
    overflow: hidden;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    opacity: 0;
    height: 0;
  }
  @media only screen and (min-width: 769px) {
    .collapsible-content--sidebar.is-open {
      overflow: initial;
      visibility: visible;
      opacity: 1;
      height: auto;
    }
  }

  .no-bullets {
    list-style: none outside;
    margin-left: 0;
  }

  .inline-list {
    padding: 0;
    margin: 0;
  }
  .inline-list li {
    display: inline-block;
    margin-bottom: 0;
    vertical-align: middle;
  }

  .index-section--footer {
    padding-top: 40px;
  }

  .hero {
    position: relative;
    overflow: hidden;
  }

  @media only screen and (min-width: 769px) {
    .site-header__logo {
      text-align: left;
    }
  }

  .site-header__logo a,
  .header-logo a {
    color: #19232d;
  }

  .site-header {
    box-shadow: 0 0 1px rgba(0, 0, 0, 0.2);
  }

  .site-nav__dropdown-link {
    display: block;
    padding: 8px 15px;
    white-space: nowrap;
  }
  .megamenu .site-nav__dropdown-link {
    padding: 4px 0;
    white-space: normal;
  }

  .article__grid-image {
    display: block;
    text-align: center;
    margin-bottom: 17px;
  }
  @media only screen and (min-width: 769px) {
    .article__grid-image {
      margin-bottom: 20px;
    }
  }
  .article__grid-image img {
    display: block;
  }

  .announcement-bar {
    font-size: 12px;
    position: relative;
    text-align: center;
    padding: 10px 0;
  }
  @media only screen and (min-width: 769px) {
    .announcement-bar {
      font-size: 13.6px;
    }
  }

  .announcement-slider__slide {
    display: none;
    position: relative;
    overflow: hidden;
    padding: 0 5px;
  }
  .announcement-slider__slide:first-child {
    display: block;
  }

  .announcement-text {
    font-family: Quicksand;
    font-size: 14px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    display: block;
    cursor: auto;
  }

  @media only screen and (min-width: 769px) {
    .announcement-slider--compact .announcement-text {
      display: inline;
    }
  }

  .app-store-button {
    width: 120px;
    height: 40px;
    display: inline-block;
  }

  .index-section--footer {
    padding-top: 0px;
  }

  .only-mobile {
    display: none;
  }
  @media screen and (max-width: 1024px) {
    .only-mobile {
      display: block;
    }
  }

  .only-desktop {
    display: block;
  }
  @media screen and (max-width: 1024px) {
    .only-desktop {
      display: none;
    }
  }

  .drawer .earnest-link a {
    display: flex;
    align-items: center;
  }
  .drawer .earnest-link a img {
    padding-right: 10px;
  }
  @media screen and (max-width: 1024px) {
    .drawer .earnest-link {
      display: flex;
      justify-content: space-between;
    }
    .drawer .earnest-link .earnest-wrap {
      display: flex;
      align-items: center;
    }
    .drawer .earnest-link .earnest-wrap img {
      padding-right: 10px;
    }
    .drawer .earnest-link .badge-wrap {
      display: flex;
      padding-right: 15px;
    }
    .drawer .earnest-link .badge-wrap a:first-child {
      padding-right: 10px;
    }
    .drawer .earnest-link .badge-wrap img {
      padding-right: 0px;
    }
  }

  .pointer-none {
    pointer-events: none;
  }

  .drawer__cart {
    text-align: center;
  }
  .drawer__cart img.empty-cart-icon {
    width: 56px;
    margin: 15px auto;
  }
  .drawer__cart p.appear-animation {
    color: #959595;
  }

  .customer-login-wrap {
    text-align: center;
    padding: 30px 0px;
    border-top: 1px solid #e6e6e6;
  }
  .customer-login-wrap.no-top-border {
    border-top: none;
    padding-top: 16px;
  }
  .customer-login-wrap p.h3 {
    font-size: 18px;
    text-transform: unset;
    font-weight: 600;
    line-height: 30px;
    padding-bottom: 10px;
  }
  .customer-login-wrap p.register {
    font-family: Quicksand;
    font-size: 22px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.36;
    letter-spacing: normal;
    text-align: left;
    color: #19232d;
  }
  .customer-login-wrap .advantages {
    display: grid;
    grid-template-columns: 72px auto;
    grid-row-gap: 32px;
    grid-column-gap: 22px;
    margin-bottom: 49px;
  }
  .customer-login-wrap .advantages img {
    width: 72px;
    height: 72px;
  }
  .customer-login-wrap .advantages div {
    display: flex;
    height: 72px;
    font-family: Quicksand;
    font-size: 18px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.5;
    letter-spacing: normal;
    text-align: left;
    color: #19232d;
    align-items: center;
  }
  .customer-login-wrap .btn.light {
    background: none;
    color: #19232d;
    border: 1px solid #e6e6e6;
  }
  .customer-login-wrap p.line-text {
    font-size: 12px;
    width: 100%;
    text-align: center;
    border-bottom: 1px solid #e6e6e6;
    line-height: 0.1em;
    margin: 30px 0;
  }
  .customer-login-wrap p.line-text span {
    background: #fff;
    padding: 0px 10px;
    letter-spacing: 3px;
    text-transform: uppercase;
  }
  @media only screen and (max-width: 959px) {
    .customer-login-wrap p.line-text {
      margin: 15px 0px;
    }
    .customer-login-wrap p.register {
      margin-bottom: 0px;
    }
    .customer-login-wrap .advantages {
      display: grid;
      grid-template-columns: 36px auto;
      grid-row-gap: 4px;
      grid-column-gap: 16px;
      margin-bottom: 17px;
    }
    .customer-login-wrap .advantages img {
      width: 36px;
      height: 36px;
    }
    .customer-login-wrap .advantages div {
      display: flex;
      height: 36px;
      font-family: Quicksand;
      font-size: 12px;
      font-weight: 500;
      font-stretch: normal;
      font-style: normal;
      line-height: 2.25;
      letter-spacing: normal;
      text-align: left;
      color: #19232d;
      align-items: center;
    }
  }

  @media only screen and (max-width: 959px) {
    .mobile-nav__item .desktop-only {
      display: none;
    }
  }
`;
