/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useEffect, useState } from 'react';

import {
  ConsentOuterWrapper,
  VideoPlaceholderWrapper,
  YoutubeWallWrapper,
  WallText,
  CustomSwitchWrapper,
  CustomSwitchBlock,
  SwitchTitle,
  CustomSwitch,
  PpLink,
} from './ConsentWrapper.styled';

declare global {
  interface Window {
    UC_UI: any;
  }
}

const usercentricsLoaded = (): boolean =>
  // Usercentrics SDK will not be loaded during SSR or in development mode
  typeof window?.UC_UI?.getServicesBaseInfo === 'function';

type ConsentObject = {
  name: string;
  consent: {
    status: boolean;
  };
  id: string;
};

const getConsentObject = (vendorName: string): ConsentObject | undefined => {
  if (!usercentricsLoaded()) {
    return undefined;
  }

  const consents = window.UC_UI.getServicesBaseInfo() as ConsentObject[];

  return consents.find(({ name }) => name === vendorName);
};

const getConsentStatus = (vendorName: string): boolean => {
  const { consent } = getConsentObject(vendorName) || { consent: { status: false } };

  return consent.status;
};

const SCROLL_POS_STORE_KEY = 'scrollpos';

const updateConsentStatus = (vendorName: string, newStatus: boolean) => {
  if (!usercentricsLoaded()) {
    return;
  }

  const { consent, id } = getConsentObject(vendorName) || {};

  if (consent === undefined || id === undefined || consent.status === newStatus) {
    return;
  }

  // Updating the consent status via UserCentrics will trigger a reload as of March 2021.
  // Store the scroll position so we can restore it when we re-render after page refresh.
  sessionStorage.setItem(SCROLL_POS_STORE_KEY, window.scrollY.toString());
  if (newStatus) {
    window.UC_UI.acceptService(id);
  } else {
    window.UC_UI.rejectService(id);
  }
};

type ConsentToggleProps = {
  checked: boolean;
  onChange: (checked: boolean) => void;
};

const ConsentToggle: React.FC<ConsentToggleProps> = ({ checked, onChange }: ConsentToggleProps) => (
  <CustomSwitchWrapper isChecked={checked}>
    <CustomSwitchBlock isChecked={checked}>
      <SwitchTitle>Externer Inhalt</SwitchTitle>
      <CustomSwitch>
        <input
          type="checkbox"
          checked={checked}
          onChange={(event) => onChange(event.target.checked)}
          data-testid="consent-toggle"
        />
        <span />
      </CustomSwitch>
    </CustomSwitchBlock>
    {checked && (
      <PpLink>
        Mehr dazu in unserer{' '}
        <a href="https://www.uptodate.de/pages/privacy-policy" target="_blank" rel="noopener noreferrer">
          Datenschutzerklärung
        </a>
      </PpLink>
    )}
  </CustomSwitchWrapper>
);

const LegalText: React.FC = () => (
  <YoutubeWallWrapper>
    <WallText>
      <h4>Empfohlener Inhalt (YouTube Video)</h4>
      <p>
        Wir benötigen Deine Einwilligungen, um diesen von YouTube bereitgestellten Inhalt anzuzeigen: Wenn Du das Video
        ansiehst, werden Cookies und ähnliche Technologien eingesetzt und Deine personenbezogenen Daten auch von YouTube
        zu Marketingzwecken verarbeitet. Indem Du den Inhalt lädst, willigst Du zugleich ein, dass Deine
        personenbezogenen Daten auch in Drittländern mit unzureichenden Datenschutzniveaus (insb. USA) verarbeitet
        werden können und deshalb insb. das Risiko besteht, dass Deine Daten durch staatliche Stellen des jeweiligen
        Drittlandes zu (geheimdienstlichen) Kontroll- und Überwachungszwecken verarbeitet werden können, ohne dass Dir
        dagegen stets hinreichende Rechtsbehelfe zustehen. Weitere Informationen, auch zu Deinen Einstellungs-,
        Widerrufs- und Widerspruchsmöglichkeiten und Deinen weiteren Rechten erhältst Du in unserer{' '}
        <a href="https://www.uptodate.de/pages/privacy-policy" target="_blank" rel="noopener noreferrer">
          Datenschutzerklärung
        </a>{' '}
        sowie der{' '}
        <a href="https://policies.google.com/privacy?hl=de" target="_blank" rel="noopener noreferrer">
          Datenschutzerklärung von YouTube.
        </a>
      </p>
    </WallText>
  </YoutubeWallWrapper>
);

export type ConsentWrapperProps = {
  children?: React.ReactNode;
};

const useRestoredScrollPosition = () => {
  useEffect(() => {
    const scrollPos = parseInt(sessionStorage.getItem(SCROLL_POS_STORE_KEY) || '0', 10);

    if (scrollPos) {
      sessionStorage.removeItem(SCROLL_POS_STORE_KEY);

      setTimeout(() => {
        window.scrollTo(0, scrollPos);
      }, 500);
    }
  });
};

const YOUTUBE_VENDOR_NAME = 'YouTube Video';

export const ConsentWrapper: React.FC<ConsentWrapperProps> = ({ children }: ConsentWrapperProps) => {
  const [hasConsent, setHasConsent] = useState(false);

  const handleUserCentricsInitialized = () => {
    setHasConsent(getConsentStatus(YOUTUBE_VENDOR_NAME));
  };

  useEffect(() => {
    window?.addEventListener('UC_UI_INITIALIZED', handleUserCentricsInitialized);

    return () => {
      window?.removeEventListener('UC_UI_INITIALIZED', handleUserCentricsInitialized);
    };
  }, []);

  useEffect(() => {
    updateConsentStatus(YOUTUBE_VENDOR_NAME, hasConsent);
  }, [hasConsent]);

  // If the user changes their consent choice, the window is reloaded (via Google Tag Manager).
  // Since the partner pages are rendered on the clientside, the browser's built-in scroll
  // restoration will not work. We need to restore the scroll position manually if needed.
  useRestoredScrollPosition();

  return (
    <ConsentOuterWrapper hasConsent={hasConsent}>
      <VideoPlaceholderWrapper>{!hasConsent ? <LegalText /> : children}</VideoPlaceholderWrapper>
      <ConsentToggle checked={hasConsent} onChange={(checked: boolean) => setHasConsent(checked)} />
    </ConsentOuterWrapper>
  );
};
