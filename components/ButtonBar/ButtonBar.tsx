import styled from 'styled-components';

export const ButtonBar = styled.div`
  margin: 20px 0;

  a,
  button {
    margin-right: 20px;
  }

  a:last-child,
  button:last-child {
    margin-right: 0;
  }

  @media screen and (max-width: 768px) {
    a,
    button {
      margin-bottom: 20px;
    }

    a:last-child,
    button:last-child {
      margin-bottom: 0;
    }
  }
`;
