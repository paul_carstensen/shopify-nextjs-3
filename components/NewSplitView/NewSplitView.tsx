import React from 'react';
import Image from 'next/image';
import styled from 'styled-components';
import { RoundedButtonLinkDark } from '../common/Buttons';

export interface NewSplitViewProps {
  children: React.ReactNode;
  title: string;
  imageUrl: any;
  ctaTitle?: string;
  ctaText?: string;
  ctaUrl?: string;
  ctaTrackingId?: string;
  ctaNewWindow?: boolean;
  onCtaButtonClick?: () => any;
  imagePosition?: 'left' | 'right';
  isMobile?: boolean;
}

interface SplitViewInnerContainerProps {
  imagePosition: 'left' | 'right';
}

const SplitViewInnerContainer = styled.div<SplitViewInnerContainerProps>`
  display: flex;
  align-items: center;
  flex-direction: ${(props) => (props.imagePosition === 'right' ? 'row' : 'row-reverse')};
  @media screen and (max-width: 768px) {
    flex-direction: ${(props) => (props.imagePosition === 'right' ? 'column' : 'column-reverse')};
  }
`;

const ImageWrapper = styled.div`
  width: 450px;
  height: 320px;
  position: relative;

  @media screen and (max-width: 768px) {
    width: 336px;
    height: 300px;
  }
`;

const CtaWrapper = styled.div`
  margin: 0 0 20px 0;
  display: flex;

  @media screen and (max-width: 768px) {
    margin-top: 15px;
  }
`;

const Content = styled.div`
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  text-align: left;
  flex: 2;
  align-self: stretch;
  padding: 0 30px;

  @media screen and (max-width: 768px) {
    text-align: center;
    padding: 0;
  }
`;

const Title = styled.div`
  font-size: 22px;
  line-height: 1.36;
  font-weight: 600;
  text-align: left;
  font-family: Poppins;
  @media screen and (max-width: 768px) {
    text-align: center;
    padding-bottom: 10px;
  }
`;

export const NewSplitView: React.FC<NewSplitViewProps> = ({
  children,
  title,
  imageUrl,
  ctaText,
  ctaUrl,
  ctaTrackingId,
  imagePosition,
  isMobile,
}) => {
  const renderBtn = () => (
    <RoundedButtonLinkDark
      href={ctaUrl}
      target="_blank"
      rel="noopener noreferrer"
      role="button"
      data-tracking-id={ctaTrackingId}
    >
      {ctaText}
    </RoundedButtonLinkDark>
  );

  const shouldRenderBtn = ctaText && ctaUrl;

  return (
    <SplitViewInnerContainer imagePosition={imagePosition || 'right'}>
      <Content>
        {shouldRenderBtn && <CtaWrapper>{renderBtn()}</CtaWrapper>}
        {!isMobile ? <Title>{title}</Title> : ''}
        {children}
      </Content>
      <ImageWrapper>
        <Image alt="" src={imageUrl} layout="fill" objectFit="cover" objectPosition="center" />
      </ImageWrapper>
    </SplitViewInnerContainer>
  );
};
