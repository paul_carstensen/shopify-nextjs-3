/**
 * Teaser component.
 *
 * This component renders a teaser banner to promote a product or website. Teasers
 * are currently shown on the home page. If and how a teaser is rendered is
 * user-configurable via the home page config (see `teasers` property).
 *
 * The teaser data object(s) found in the configuration are validated; if any
 * of the attributes are invalid, the respective teaser will not be rendered.
 *
 */
import React, { CSSProperties } from 'react';
import * as Yup from 'yup';
import styled, { css } from 'styled-components';
import { FontFamilyPoppins } from '@u2dv/marketplace_ui_kit/dist/styles';

type StyledLinkProps = {
  backgroundImageUrl: string;
  tintBackground?: boolean;
};

const StyledLink = styled.a<StyledLinkProps>`
  height: 80px;
  width: calc(100% - 2 * 7px);
  max-width: 884px;
  border-radius: 15px;

  /* Box centering */
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto;

  /* Text styling */
  text-align: center;
  padding: 0 20px;
  color: #fff;
  font-family: ${FontFamilyPoppins};
  font-weight: bold;
  font-size: 20px;
  line-height: 1.44;
  hyphens: auto;

  &,
  &:hover {
    color: #fff;
  }

  /* Image positioning */
  background: url(${({ backgroundImageUrl }) => backgroundImageUrl}) no-repeat left center;
  background-size: 100% auto;

  /* Optional background tinting */
  ${({ tintBackground }) =>
    tintBackground &&
    css`
      background-color: rgba(25, 35, 45, 0.4);
      background-blend-mode: multiply;
    `}
`;

type TeaserData = {
  imageUrl: string;
  linkUrl: string;
  text?: string;
  trackingId?: string;
  tintBackground?: boolean;
};

export type TeaserProps = TeaserData & React.AllHTMLAttributes<HTMLAnchorElement>;

export const Teaser: React.FC<TeaserProps> = ({
  imageUrl,
  linkUrl,
  text,
  trackingId,
  tintBackground,
  className,
  style,
}) => (
  <StyledLink
    className={className}
    style={style}
    href={linkUrl}
    data-tracking-id={trackingId}
    backgroundImageUrl={imageUrl}
    tintBackground={tintBackground}
  >
    {text}
  </StyledLink>
);

const teaserSchema = Yup.object().shape({
  imageUrl: Yup.string().url().required(),
  linkUrl: Yup.string().url().required(), // require absolute URL for simpler validation
  text: Yup.string(),
  trackingId: Yup.string().matches(/^ga-/),
  tintBackground: Yup.boolean(),
});

export const isValidTeaserData = (teaserData: TeaserData | undefined): boolean => {
  try {
    teaserSchema.validateSync(teaserData);
    return true;
  } catch (err) {
    if (err instanceof Yup.ValidationError === false) {
      throw err;
    }

    return false;
  }
};

export const renderTeaser = (teaserData: TeaserData | undefined, style?: CSSProperties): React.ReactElement | null => {
  if (!teaserData || !isValidTeaserData(teaserData)) {
    return null;
  }

  return <Teaser {...teaserData} style={style} />;
};
