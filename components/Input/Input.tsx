import React from 'react';
import styled from 'styled-components';

const Label = styled.label`
  text-transform: uppercase;
  letter-spacing: 0.3em;
  font-size: 0.75em;
  display: block;
  margin-bottom: 10px;

  &[for] {
    cursor: pointer;
  }

  & span::after {
    content: '${(props) => (props['aria-required'] ? '*' : '')}';
    color: red;
  }

  &.error {
    color: #d02e2e;
  }
`;

const InputField = styled.input`
  border: 1px solid #e6e6e6;
  max-width: 100%;
  padding: 8px 10px;
  border-radius: 0;

  &.error {
    border-color: #d02e2e;
    background-color: #fff6f6;
    color: #d02e2e;
  }
`;

export type InputProps = {
  id: string;
  label: string;
  type?: 'text' | 'number';
  value: string;
  required?: boolean;
  onChange?: (event?: any) => void;
  onBlur?: (event?: any) => void;
};

export const Input: React.FC<InputProps> = ({
  id,
  label,
  type = 'text',
  value,
  onChange,
  onBlur,
  required = false,
  ...props
}: InputProps) => (
  <>
    <Label htmlFor={id} aria-required={required}>
      <span>{label}</span>
      <InputField id={id} name={id} type={type} value={value} onChange={onChange} onBlur={onBlur} {...props} />
    </Label>
  </>
);
