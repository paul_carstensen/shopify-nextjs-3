/**
 * @jest-environment jsdom
 */

import { fireEvent, render, screen } from '@testing-library/react';
import { categories, sortedProducts, products, interestKeys, Product, categoryKeys } from 'helpers/homePageConfig';
import { ProductList } from './ProductList';

// we have to test an implementation detail of the `Chip` component here
// if the Chip component gets written to support aria attributes we can use
// the standard `.toBeChecked()` expectation: https://github.com/testing-library/jest-dom#tobechecked
// until then we override the toBeChecked matcher with some custom logic
expect.extend({
  toBeChecked(received: HTMLElement) {
    return {
      pass: received.previousSibling?.nodeName === 'DIV',
      message: () => `Expected chip "${received.textContent}" to be checked`,
    };
  },
});

describe('ProductList', () => {
  const productDescription = (p: Product) => p.badge + p.title + p.description;

  it('should toggle checked categories', () => {
    render(<ProductList userPreference={{ categories: ['health'], interest: 'apps' }} />);

    const health = () => screen.getByText(categories.health);
    const sustainability = () => screen.getByText(categories.sustainability);

    // check that chips are initially rendered as expected
    expect(health()).toBeChecked();
    expect(sustainability()).not.toBeChecked();

    // click sustainability
    fireEvent.click(sustainability());
    expect(health()).toBeChecked();
    expect(sustainability()).toBeChecked();

    // click sustainability a second time
    fireEvent.click(sustainability());
    expect(health()).toBeChecked();
    expect(sustainability()).not.toBeChecked();

    // click on health
    fireEvent.click(health());
    expect(health()).toBeChecked(); // health is still selected because there must be at least one selection
    expect(sustainability()).not.toBeChecked();

    // click sustainability a third time
    fireEvent.click(sustainability());
    expect(health()).toBeChecked(); // health is still selected
    expect(sustainability()).toBeChecked();
  });

  it('should render products matching selected category after clicking', () => {
    const healthProducts = Object.values(products).filter((p) => p.category === 'health');
    const sustainabilityProducts = Object.values(products).filter((p) => p.category === 'sustainability');

    render(<ProductList userPreference={{ categories: ['health'], interest: 'apps' }} />);

    const renderedProductDescriptions = () =>
      screen // eslint-disable-line implicit-arrow-linebreak -- linter style conflicts with automatic formatting here
        .getAllByRole('link')
        .map((l) => l.textContent)
        .sort()
        .join('\n');

    const healthProductDescriptions = healthProducts.map(productDescription).sort().join('\n');

    expect(renderedProductDescriptions()).toBe(healthProductDescriptions);

    // select sustainability
    fireEvent.click(screen.getByText(categories.sustainability));
    // deselect health
    fireEvent.click(screen.getByText(categories.health));

    const sustainabilityProductDescriptions = sustainabilityProducts.map(productDescription).sort().join('\n');

    expect(renderedProductDescriptions()).toBe(sustainabilityProductDescriptions);
  });

  it.each(categoryKeys)('should render only products matching category "%s"', (category) => {
    render(<ProductList userPreference={{ categories: [category], interest: 'articles' }} />);

    const renderedProductDescriptions = screen
      .getAllByRole('link')
      .map((l) => l.textContent)
      .sort()
      .join('\n');

    const categoryProductDescriptions = Object.values(products)
      .filter((p) => p.category === category)
      .map(productDescription)
      .sort()
      .join('\n');

    expect(renderedProductDescriptions).toBe(categoryProductDescriptions);
  });

  it.each(interestKeys)('should render products sorted by interest "%s"', (interest) => {
    render(<ProductList userPreference={{ categories: ['health', 'sustainability'], interest }} />);

    const links = screen.getAllByRole('link');
    const renderedProductDescriptions = links.map((l) => l.textContent).join('\n');
    const expectedProducts = sortedProducts[interest].map((p) => products[p]);
    const expectedProductDescriptions = expectedProducts.map(productDescription).join('\n');

    expect(renderedProductDescriptions).toBe(expectedProductDescriptions);
  });
});
