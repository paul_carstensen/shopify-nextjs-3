import { useEffect, useState } from 'react';
import { Chip, ListItem } from '@u2dv/marketplace_ui_kit/dist/components';
import { FontFamilyPoppins } from '@u2dv/marketplace_ui_kit/dist/styles';
import styled from 'styled-components';
import { categories, categoryKeys, CategoryKey } from 'helpers/homePageConfig';
import { chips, products, sortedProducts, productListTitle } from 'helpers/homePageConfig';
import { saveUserPreferences, UserPreference } from './userPreferences';

const TitleContainer = styled.div`
  text-align: center;
`;

const StyledChipsContainer = styled.div`
  display: flex;
  justify-content: center;
  gap: 8px;
  margin-bottom: 2rem;
  font-family: 'Quicksand';
`;

const Title = styled.h2`
  font-family: ${FontFamilyPoppins};
  font-size: 24px;
  font-weight: bold;
`;

const ProductListContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  a {
    width: 30%;
  }
  p,
  h3 {
    text-align: center;
  }
  @media only screen and (max-width: 768px) {
    a {
      width: 100%;
    }
  }
`;

export type ProductListProps = {
  userPreference: UserPreference;
};

export const ProductList = ({ userPreference: initialUserPreference }: ProductListProps) => {
  const [userPreference, setUserPreference] = useState(initialUserPreference);

  const activeChips = Object.fromEntries(
    chips.map(({ category }) => [category, userPreference.categories.includes(category)])
  );

  const filteredAndSortedProducts = sortedProducts[userPreference.interest]
    .map((key) => products[key])
    .filter((p) => userPreference.categories.includes(p.category));

  useEffect(() => {
    saveUserPreferences(userPreference);
  }, [userPreference]);

  const toggleCategory = (category: CategoryKey) => {
    const selectedCategories = categoryKeys.filter((c) => (c === category ? !activeChips[c] : activeChips[c]));
    const newUserPreference = { ...userPreference, categories: selectedCategories };
    if (newUserPreference.categories.length > 0) {
      setUserPreference(newUserPreference);
    }
  };

  return (
    <>
      <TitleContainer>
        <Title>{productListTitle}</Title>
      </TitleContainer>
      <StyledChipsContainer>
        {chips.map(({ category, color, dataTrackingId }) => (
          <Chip
            key={category}
            name={categories[category]}
            active={activeChips[category]}
            removable={activeChips[category]}
            onPress={() => toggleCategory(category)}
            activeColor={color}
            data-tracking-id={dataTrackingId}
          >
            {categories[category]}
          </Chip>
        ))}
      </StyledChipsContainer>
      <ProductListContainer>
        {filteredAndSortedProducts.map(({ title, color, description, dataTrackingId, url, imageUrl, badge }) => (
          <ListItem
            key={`product-${title}`}
            bgColor={color}
            badge={badge}
            text={description}
            title={title}
            trackingId={dataTrackingId}
            url={url}
            urlImage={imageUrl}
          />
        ))}
      </ProductListContainer>
    </>
  );
};
