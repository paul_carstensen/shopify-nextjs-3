import { CategoryKey, InterestKey, categoryKeys, interestKeys } from 'helpers/homePageConfig';
import * as yup from 'yup';

const localStorageKey = 'home_page_user_preferences';

export type UserPreference = {
  categories: CategoryKey[];
  interest: InterestKey;
};

const userPreferenceSchema = yup
  .object({
    categories: yup.array(yup.string().oneOf(categoryKeys)).min(1).required(),
    interest: yup.string().oneOf(interestKeys).required(),
  })
  .required();

export const isUserPreference = (up?: unknown): up is UserPreference => userPreferenceSchema.isValidSync(up);

const loadUserPreferenceFromLocalStorage = (): Partial<UserPreference> => {
  try {
    const mayBeUserPreference = JSON.parse(localStorage.getItem(localStorageKey) ?? '');
    return isUserPreference(mayBeUserPreference) ? mayBeUserPreference : {};
  } catch {
    return {};
  }
};

const loadUserPreferenceFromQueryParams = (): UserPreference | undefined => {
  const params = new URLSearchParams(document.location.search.substring(1));
  const mayBeUserPreference = userPreferenceSchema.cast({
    categories: params.getAll('category') as CategoryKey[],
    interest: (params.get('interest') ?? 'apps') as InterestKey,
  });
  return isUserPreference(mayBeUserPreference) ? mayBeUserPreference : undefined;
};

export const loadUserPreference = (): Partial<UserPreference> =>
  loadUserPreferenceFromQueryParams() ?? loadUserPreferenceFromLocalStorage();

export const saveUserPreferences = (userPreferences: UserPreference) => {
  localStorage.setItem(localStorageKey, JSON.stringify(userPreferences));
};
