/* eslint-disable camelcase */
export type PageBlock = {
  page_id: number;
  position: number;
  type: string;
  data: any;
  page_handle: string;
};
/* eslint-enable camelcase */

export enum PartnerPageBlockType {
  HERO = 'HERO',
  ADVANTAGES = 'ADVANTAGES',
  PROCESS = 'PROCESS',
  OFFERS = 'OFFERS',
  TESTIMONIAL = 'TESTIMONIAL',
  PRICING = 'PRICING',
  FAQ = 'FAQ',
  GALLERY = 'GALLERY',
  USE_CASE = 'USE_CASE',
  VIDEO = 'VIDEO',
  CALL_TO_ACTION = 'CALL_TO_ACTION',
  REGISTRATION = 'REGISTRATION',
  BUYING = 'BUYING',
  BUYING_SKU = 'BUYING_SKU',
  CUSTOM_HTML = 'CUSTOM_HTML',
  CARESHIP_WIDGET = 'CARESHIP_WIDGET',
  ONE_CLICK_RETURN = 'ONE_CLICK_RETURN',
}

export interface BlockContent {
  component: PartnerPageBlockType;
  data: any;
  pageHandle?: string;
}
