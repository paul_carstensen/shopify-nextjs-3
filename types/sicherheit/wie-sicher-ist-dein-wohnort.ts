export type AmountString = 'niedrig' | 'mittel' | 'hoch';

export type LocalRiskPotential = {
  locationName: string;
  relativeRisk: AmountString;
}

export type CaseAmountSummary = {
  locationName: string;
  numCases: number;
  timeFrame: string;
}

export type CrimeStats = {
  crimeType: string;
  numCases: number;
  relativeRisk: AmountString;
  clearancePercentage: string;
}

export type CrimeInfo = {
  postalCode: string;
  cityRelativeRisk: LocalRiskPotential;
  districtRelativeRisk?: LocalRiskPotential;
  caseAmountSummary: CaseAmountSummary;
  crimeStatsPerType: CrimeStats[];
};
