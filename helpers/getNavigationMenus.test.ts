import { mocked } from 'ts-jest/utils';
import fetch from 'cross-fetch';
import cache from '../cache';
import { getNavigationMenus } from './getNavigationMenus';

jest.mock('../cache');
jest.mock('cross-fetch');

describe('getNavigationMenus', () => {
  beforeEach(() => {
    mocked(cache.get).mockReset();
    mocked(fetch).mockReset();
    mocked(cache.set).mockReset();
  });

  it('uses the cache to get the menu items', async () => {
    const cachedMenus = {};
    mocked(cache.get).mockReturnValueOnce(cachedMenus);

    const menus = await getNavigationMenus();

    expect(menus).toBe(cachedMenus);
    expect(cache.get).toHaveBeenCalled();
    expect(fetch).not.toHaveBeenCalled();
    expect(cache.set).not.toHaveBeenCalled();
  });

  it('fetches the items over HTTP if necessary', async () => {
    const fetchedMenus = {};
    const mockResponse = {
      ok: true,
      json: () => ({
        data: { linklists: fetchedMenus },
      }),
    };
    mocked(fetch).mockResolvedValueOnce(mockResponse as any);

    const menus = await getNavigationMenus();

    expect(menus).toBe(fetchedMenus);
    expect(cache.get).toHaveBeenCalled();
    expect(fetch).toHaveBeenCalled();
    expect(cache.set).toHaveBeenCalledWith('navigationMenuItems', menus, 3600);
  });

  it('logs an error if fetching the menu items fails (HTTP error) and returns empty menus', async () => {
    const emptyMenus = {
      contentMain: [],
      contentSide: [],
      contentFooter: [],
    };
    const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();
    const mockResponse = {
      ok: false,
      status: 500,
      statusText: 'Internal Server Error',
      text: () => '<h1>Trouble at Shopify</h1>',
    };
    mocked(fetch).mockResolvedValueOnce(mockResponse as any);

    const menus = await getNavigationMenus();
    expect(menus).toEqual(emptyMenus);
    const expectedError = new Error('HTTP 500 Internal Server Error: <h1>Trouble at Shopify</h1>');
    expect(consoleErrorSpy).toHaveBeenCalledWith('Failed to fetch navigation menus:', expectedError);

    expect(cache.get).toHaveBeenCalled();
    expect(fetch).toHaveBeenCalled();
    expect(cache.set).not.toHaveBeenCalled();
  });

  it('logs an error if fetching the menu items fails (API error) and returns empty menus', async () => {
    const emptyMenus = {
      contentMain: [],
      contentSide: [],
      contentFooter: [],
    };
    const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();
    const mockResponse = {
      ok: true,
      json: () => ({
        error: 'unknown API error',
      }),
    };
    mocked(fetch).mockResolvedValueOnce(mockResponse as any);

    const menus = await getNavigationMenus();
    expect(menus).toEqual(emptyMenus);
    const expectedError = new Error('unknown API error');
    expect(consoleErrorSpy).toHaveBeenCalledWith('Failed to fetch navigation menus:', expectedError);

    expect(cache.get).toHaveBeenCalled();
    expect(fetch).toHaveBeenCalled();
    expect(cache.set).not.toHaveBeenCalled();
  });
});
