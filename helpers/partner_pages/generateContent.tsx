/* eslint-disable react/no-array-index-key */
import jsonschema from 'jsonschema';
import { BlockContent } from '../../types/partnerPages';
import { jsonSchemaMappings, componentContainer } from '../../components/partner_pages/blocks';

const generateContent = (pageContent: BlockContent[] = []) => {
  const validator = new jsonschema.Validator();

  const components = pageContent
    .filter(
      // @ts-expect-error TODO: jsonSchemaMappings with enums as keys must be rewritten to adhere strict typescript settings
      (content) => validator.validate(content.data || {}, jsonSchemaMappings[content.component]).errors.length === 0
    )
    .map((content, index) => {
      // @ts-expect-error TODO: componentContainer with enums as keys must be rewritten to adhere strict typescript settings
      const DesiredComponent = componentContainer[content.component];
      return (
        <DesiredComponent
          key={index}
          component={content.component}
          data={content.data || {}}
          pageHandle={content.pageHandle}
        />
      );
    });

  return <>{components}</>;
};

export default generateContent;
