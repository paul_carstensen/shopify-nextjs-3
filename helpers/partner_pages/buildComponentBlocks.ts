import { BlockContent, PartnerPageBlockType, PageBlock } from '../../types/partnerPages';

const buildComponentBlocks = (blocks: PageBlock[]): BlockContent[] =>
  blocks.map(({ data, type, page_handle: pageHandle }) => ({
    // @ts-expect-error TODO: PartnerPageBlockType with enums as keys must be rewritten in a separate PR to adhere strict typescript settings
    component: PartnerPageBlockType[type],
    data,
    pageHandle,
  }));

export default buildComponentBlocks;
