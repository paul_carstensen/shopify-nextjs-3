import { colors } from '@u2dv/marketplace_ui_kit/dist/styles';
import { TeaserProps } from '../components/Teaser';
import earnest from '../assets/images/homepage/earnest-new.jpg';
import smartricity from '../assets/images/homepage/smartricity-new.jpg';
import saveWater from '../assets/images/homepage/save-water-new.jpg';
import sustainabilityForBeginners from '../assets/images/homepage/sustainability-for-beginners-new.jpg';
import sustainabilityShop from '../assets/images/homepage/sustainability-shop-new.jpg';
import wellbi from '../assets/images/homepage/wellbi-new.jpg';
import healthTips from '../assets/images/homepage/tipps-new.jpg';
import radon from '../assets/images/homepage/radon-new.jpg';
import sleepBetter from '../assets/images/homepage/sleep-better-new.jpg';
import eatHealthy from '../assets/images/homepage/eat-healthy-new.jpg';
import healthShop from '../assets/images/homepage/health-shop-new.jpg';
import imageFirstQuestion from '../assets/images/homepage/questions/Questionaire_1.jpg';
import imageSecondQuestion from '../assets/images/homepage/questions/Questionaire_2.jpg';

const SUSTAINABILITY_PRODUCT_COLOR = colors.duckEggBlue;
const HEALTH_PRODUCT_COLOR = colors.powderBlue;
const SUSTAINABILITY_COLOR = colors.duckEggBlue;
const HEALTH_COLOR = colors.powderBlue;
const SUSTAINABILITY_BADGE = '🌳';
const HEALTH_BADGE = '🍏';

export const categories = {
  sustainability: `Nachhaltigkeit ${SUSTAINABILITY_BADGE}`,
  health: `Gesundheit ${HEALTH_BADGE}`,
};
export type CategoryKey = keyof typeof categories;
export const categoryKeys = Object.keys(categories) as CategoryKey[];

export const interests = {
  apps: 'Apps für den Alltag 📱',
  articles: 'Artikel zum Lesen 📚',
  products: 'Lifestyle-Produkte ✨',
};
export type InterestKey = keyof typeof interests;
export const interestKeys = Object.keys(interests) as InterestKey[];

export type Product = {
  title: string;
  description: string;
  category: CategoryKey;
  color: string;
  badge?: string;
  imageUrl: string;
  dataTrackingId: string;
  url: string;
};

export type ProductKey =
  | 'earnest'
  | 'welbi'
  | 'smartricity'
  | 'radon'
  | 'beginner'
  | 'water'
  | 'sleep'
  | 'luck'
  | 'food'
  | 'kfw'
  | 'healthShop'
  | 'sustainableShop';

export const products: Record<ProductKey, Product> = {
  earnest: {
    title: 'Unsere Earnest App',
    description: 'Dein Begleiter für mehr Nachhaltigkeit im Alltag',
    category: 'sustainability',
    color: SUSTAINABILITY_PRODUCT_COLOR,
    badge: SUSTAINABILITY_BADGE,
    url: 'https://www.meetearnest.de/?utm_medium=hometile',
    imageUrl: earnest.src,
    dataTrackingId: 'ga-newhome01-vc-productteasertile-earnest',
  },
  welbi: {
    title: 'Wellbi - Dein Wohlfühlcoach',
    description: '5 Lebensbereiche, die Dich glücklich machen',
    category: 'health',
    color: HEALTH_PRODUCT_COLOR,
    badge: HEALTH_BADGE,
    url: 'https://www.wellbi.fit/app-splash2?utm_medium=hometile',
    imageUrl: wellbi.src,
    dataTrackingId: 'ga-newhome01-vc-productteasertile-wellbi',
  },
  smartricity: {
    title: 'Mit Smartricity Energie sparen',
    description: 'Finde Stromsünder zuhause und ersetze sie',
    category: 'sustainability',
    color: SUSTAINABILITY_PRODUCT_COLOR,
    badge: SUSTAINABILITY_BADGE,
    url: '/pages/nachhaltige-analysen?utm_medium=hometile',
    imageUrl: smartricity.src,
    dataTrackingId: 'ga-newhome01-vc-productteasertile-smartricity',
  },
  radon: {
    title: 'Radon - wie sicher bist Du?',
    description: 'Checke das Radonrisiko in Deiner Umgebung',
    category: 'health',
    color: HEALTH_PRODUCT_COLOR,
    badge: HEALTH_BADGE,
    url: '/nachhaltigkeit/radon?utm_medium=hometile',
    imageUrl: radon.src,
    dataTrackingId: 'ga-newhome01-vc-productteasertile-radon',
  },
  beginner: {
    title: 'Step by step',
    description: 'Nachhaltigkeit für Beginner - fang an',
    category: 'sustainability',
    color: SUSTAINABILITY_PRODUCT_COLOR,
    badge: SUSTAINABILITY_BADGE,
    url: '/nachhaltigkeit/nachhaltigkeit-fuer-anfaenger?utm_medium=hometile',
    imageUrl: sustainabilityForBeginners.src,
    dataTrackingId: 'ga-newhome01-vc-productteasertile-beginner_guide',
  },
  water: {
    title: 'Verschwendung den Hahn abdrehen',
    description: '3 Tipps, um sofort Wasser zu sparen',
    category: 'sustainability',
    color: SUSTAINABILITY_PRODUCT_COLOR,
    badge: SUSTAINABILITY_BADGE,
    url: '/pages/3-einfache-tipps-um-sofort-wasser-zu-sparen?utm_medium=hometile',
    imageUrl: saveWater.src,
    dataTrackingId: 'ga-newhome01-vc-productteasertile-savewater',
  },
  sleep: {
    title: 'Traumhafte Nächte',
    description: 'Hol Dir Tipps für erholsame Nächte',
    category: 'health',
    color: HEALTH_PRODUCT_COLOR,
    badge: HEALTH_BADGE,
    url: '/pages/besser-schlafen?utm_medium=hometile',
    imageUrl: sleepBetter.src,
    dataTrackingId: 'ga-newhome01-vc-productteasertile-better_sleep',
  },
  luck: {
    title: 'Viel Glück',
    description: 'Diese 5 Tipps machen glücklich',
    category: 'health',
    color: HEALTH_PRODUCT_COLOR,
    badge: HEALTH_BADGE,
    url: '/blogs/dein-uptodate-expertenblog/viel-gluck-diese-5-tipps-machen-glucklich?utm_medium=hometile',
    imageUrl: healthTips.src,
    dataTrackingId: 'ga-newhome01-vc-productteasertile-health_tips',
  },
  food: {
    title: 'Für ein gutes Bauchgefühl',
    description: 'Ernähre Dich gesund & fühl Dich wohl in Deiner Haut',
    category: 'health',
    color: HEALTH_PRODUCT_COLOR,
    badge: HEALTH_BADGE,
    url: '/pages/gesund-ernahren?utm_medium=hometile',
    imageUrl: eatHealthy.src,
    dataTrackingId: 'ga-newhome01-vc-productteasertile-healthy_food',
  },
  kfw: {
    title: 'Hol Dir Deine Förderung',
    description: 'Jetzt die richtige Ladestation finden und KfW-Förderung sichern',
    category: 'sustainability',
    color: SUSTAINABILITY_PRODUCT_COLOR,
    badge: SUSTAINABILITY_BADGE,
    url: '/blog/e-mobility?utm_medium=hometile',
    imageUrl: 'https://cdn.shopify.com/s/files/1/0268/4819/8771/files/Home_kfW-E-Mobilitaet_2.jpg?v=1633022150',
    dataTrackingId: 'ga-newhome01-vc-productteasertile-kfw',
  },
  healthShop: {
    title: 'Produkte für mehr Gesundheit',
    description: 'Smarte Produkte, die Dich unterstützen',
    category: 'health',
    color: HEALTH_PRODUCT_COLOR,
    badge: HEALTH_BADGE,
    url: '/pages/gesundheit?utm_medium=hometile',
    imageUrl: healthShop.src,
    dataTrackingId: 'ga-newhome01-vc-productteasertile-healthy_shop_products',
  },
  sustainableShop: {
    title: 'Smarte Produkte für mehr Nachhaltigkeit',
    description: 'Finde hier, was Energie & Geld spart',
    category: 'sustainability',
    color: SUSTAINABILITY_PRODUCT_COLOR,
    badge: SUSTAINABILITY_BADGE,
    url: '/pages/nachhaltigkeit?utm_medium=hometile',
    imageUrl: sustainabilityShop.src,
    dataTrackingId: 'ga-newhome01-vc-productteasertile-sustainable_shop_products',
  },
};
export const productKeys = Object.keys(products) as ProductKey[];

export const sortedProducts: Record<InterestKey, ProductKey[]> = {
  apps: [
    'earnest',
    'welbi',
    'smartricity',
    'radon',
    'beginner',
    'water',
    'sleep',
    'luck',
    'food',
    'kfw',
    'healthShop',
    'sustainableShop',
  ],
  articles: [
    'sleep',
    'water',
    'food',
    'luck',
    'beginner',
    'kfw',
    'earnest',
    'welbi',
    'radon',
    'smartricity',
    'healthShop',
    'sustainableShop',
  ],
  products: [
    'healthShop',
    'sustainableShop',
    'earnest',
    'welbi',
    'beginner',
    'radon',
    'smartricity',
    'water',
    'sleep',
    'luck',
    'food',
    'kfw',
  ],
};

export type Chip = {
  category: CategoryKey;
  color: string;
  dataTrackingId: string;
};

export const chips: Chip[] = [
  {
    category: 'sustainability',
    color: SUSTAINABILITY_COLOR,
    dataTrackingId: 'ga-newhome01-vc-productteaserfilter-sustainability',
  },
  {
    category: 'health',
    color: HEALTH_COLOR,
    dataTrackingId: 'ga-newhome01-vc-productteaserfilter-health',
  },
];

export const intro = {
  title: 'Besser leben mit uptodate',
  description: (
    <>
      Bei uptodate helfen wir Dir, besser zu leben und zu wohnen. Zum Beispiel durch innovative Lösungen für ein
      nachhaltigeres und gesünderes Verhalten.
      <br />
      Wir informieren, wir schaffen Bewusstsein und wir machen es Dir einfach, selbst aktiv zu werden.
    </>
  ),
  dataTrackingId: 'ga-newhome01-vc-explanation-more',
  explanations: [
    {
      description: (
        <>
          Wir halten den Schlüssel zu unserer Zukunft selbst in der Hand.
          <br />
          Von Tipps &amp; Tools, um gemeinsam die Umwelt zu schonen, bis hin zu innovativen Angeboten, um ein gesundes,
          ausgewogenes und sorgenfreieres Leben zu führen - bei uptodate bist Du an der richtigen Stelle.
          <br />
          Bei uns stehst Du mit Deinen individuellen Bedürfnissen im Mittelpunkt. Lass Dich von uns inspirieren!
        </>
      ),
    },
  ],
};

export const questions = [
  {
    title: 'Welches Thema ist Dir wichtiger?',
    options: [
      {
        key: 'sustainability',
        title: categories.sustainability,
        dataTrackingId: 'ga-newhome01-vc-question-0101_sustainability',
      },
      {
        key: 'health',
        title: categories.health,
        dataTrackingId: 'ga-newhome01-vc-question-0102_health',
      },
    ],
    bgImage: imageFirstQuestion.src,
  },
  {
    title: 'Wofür interessierst Du Dich am meisten?',
    options: [
      {
        key: 'apps',
        title: interests.apps,
        dataTrackingId: 'ga-newhome01-vc-question-0201_apps',
      },
      {
        key: 'articles',
        title: interests.articles,
        dataTrackingId: 'ga-newhome01-vc-question-0202_read',
      },
      {
        key: 'products',
        title: interests.products,
        dataTrackingId: 'ga-newhome01-vc-question-0203_prducts',
      },
    ],
    bgImage: imageSecondQuestion.src,
  },
];

export type SlotId = 'slot1';

export const teasers: Partial<Record<SlotId, TeaserProps>> = {
  // slot1: {
  //   imageUrl: 'https://cdn.shopify.com/s/files/1/0268/4819/8771/files/1111_ClimatePartner_31_1080x.jpg?v=1619510483',
  //   linkUrl: 'https://www.wellbi.fit/?utm_source=u2d_home_banner_slot1',
  //   text: 'Neue App – Wellbi',
  //   trackingId: 'ga-teaser-wellbi-click', // must start with `ga-teaser-`
  //   tintBackground: true,
  // },
};

export const productListTitle = 'Angebote für Dich';
