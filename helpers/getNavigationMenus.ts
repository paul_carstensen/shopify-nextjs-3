import fetch from 'cross-fetch';
import cache, { keys } from '../cache';
import config from '../config';
import { Navigation, NavigationItem } from '../types';

export type NavigationMenusResult = {
  contentMain: NavigationItem[];
  contentSide: NavigationItem[];
  contentFooter: Navigation[];
};

const fetchNavigationMenus = async (): Promise<NavigationMenusResult> => {
  const url = `${config().menu.apiUrl}?${+new Date()}`; // circumvent Shopify CDN cache
  const response = await fetch(url);
  if (!response.ok) {
    const errorText = (await response.text()) || '<no response text>';
    throw new Error(`HTTP ${response.status} ${response.statusText}: ${errorText}`);
  }

  const { data, error } = await response.json();
  if (error) {
    throw new Error(error);
  }

  return data.linklists;
};

/**
 * Get the navigation menus (link lists) for the site header and footer.
 *
 * As a data source, a hidden Shopify page is used which renders the link lists
 * as JSON. As of August 2021, there is no official API endpoint for this data.
 *
 */
export const getNavigationMenus = async (): Promise<NavigationMenusResult> => {
  let menus: NavigationMenusResult | undefined = cache.get(keys.NAVIGATION_MENU_ITEMS);
  if (!menus) {
    try {
      menus = await fetchNavigationMenus();
      cache.set(keys.NAVIGATION_MENU_ITEMS, menus, 3600);
    } catch (err) {
      console.error('Failed to fetch navigation menus:', err); // eslint-disable-line no-console
      menus = {
        contentMain: [],
        contentSide: [],
        contentFooter: [],
      };
    }
  }

  return menus;
};
