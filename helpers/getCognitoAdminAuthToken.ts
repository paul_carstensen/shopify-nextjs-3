import AWS from 'aws-sdk';

export type CognitoAdminAuthOptions = {
  region: string;
  clientId: string;
  userPoolId: string;
  username: string;
  password: string;
};

type GetTokenOptions = {
  useCached?: boolean;
};

let cachedToken: string | null = null; // cached in process memory

export const getCognitoAdminAuthToken = async (
  { region, clientId, userPoolId, username, password }: CognitoAdminAuthOptions,
  { useCached = false }: GetTokenOptions = {}
): Promise<string> => {
  if (cachedToken === null || useCached === false) {
    console.info('Fetching new token from Cognito…'); // eslint-disable-line no-console
    const cognito = new AWS.CognitoIdentityServiceProvider({ region });
    const result = await cognito
      .adminInitiateAuth({
        AuthFlow: 'ADMIN_USER_PASSWORD_AUTH',
        ClientId: clientId,
        UserPoolId: userPoolId,
        AuthParameters: {
          USERNAME: username,
          PASSWORD: password,
        },
      })
      .promise();

    const newToken = result.AuthenticationResult?.IdToken;
    if (!newToken) {
      throw new Error('no token received');
    }

    cachedToken = newToken;
  }

  return cachedToken;
};
