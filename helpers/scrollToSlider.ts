import { isMobile } from 'react-device-detect';

export const DEFAULT_SCROLL_CONTAINER_ID = 'slider-page-scroll-container';

/**
 * Scroll to the given slider DOM element.
 *
 * On a desktop device, scroll inside the given (or default) scroll container.
 * On a mobile device, scroll inside the window.
 *
 */
export const scrollToSlider = (selector: string, scrollContainerId: string = DEFAULT_SCROLL_CONTAINER_ID): void => {
  const el = document.querySelector(selector);
  if (!el) {
    return;
  }

  const header = document.querySelector('.header-section');
  if (!header) {
    return;
  }

  if (!isMobile && scrollContainerId) {
    const scrollContainer = document.getElementById(scrollContainerId);
    if (!scrollContainer) {
      return;
    }

    scrollContainer.scrollTo({
      top: el.getBoundingClientRect().top + scrollContainer.scrollTop - header.getBoundingClientRect().height,
      behavior: 'smooth',
    });
  } else {
    window.scrollTo({
      top: el.getBoundingClientRect().top + window.pageYOffset - header.getBoundingClientRect().height,
      behavior: 'smooth',
    });
  }
};
