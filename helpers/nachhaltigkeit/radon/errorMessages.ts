export const getErrorMessage = (e: unknown): string => {
  const error = e as { response?: { status?: unknown } };
  switch (error?.response?.status) {
    case 400:
      return 'Eine Deiner Eingaben ist nicht gültig. Bitte versuche es erneut.';
    case 404:
      return 'Wir konnten für Deine Adresse keine Daten finden.';
    default:
      return 'Da ist etwas schief gelaufen. Bitte versuche es erneut.';
  }
};
