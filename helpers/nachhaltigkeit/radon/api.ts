import axios, { AxiosResponse } from 'axios';
import { AddressData, ArabeskData } from '../../../types/nachhaltigkeit/radon';
import config from '../../../config';

export const postAddressData = async ({
  email,
  streetName,
  houseNumber,
  postalCode,
}: AddressData): Promise<AxiosResponse<any>> =>
  axios({
    method: 'POST',
    url: `${config().gimmick.apiUrl}/radon_level`,
    data: {
      email,
      street_name: streetName,
      house_number: houseNumber,
      postal_code: postalCode,
    },
  });

export const postArabeskData = async (
  { email, streetName, houseNumber, postalCode }: AddressData,
  {
    arabeskWaterFilter,
    arabeskWaterFilterMaintenance,
    arabeskWaterRarelyUsedSources,
    arabeskWaterSiliconeGroutOk,
    arabeskSmokeAlarms,
    arabeskCircuitBreakers,
    arabeskPropertyOwner,
    arabeskMovingOutSoon,
  }: ArabeskData
): Promise<AxiosResponse<any>> =>
  axios({
    method: 'POST',
    url: `${config().gimmick.apiUrl}/arabesk`,
    data: {
      email,
      street_name: streetName,
      house_number: houseNumber,
      postal_code: postalCode,
      arabesk_water_filter: arabeskWaterFilter,
      arabesk_water_filter_maintenance: arabeskWaterFilterMaintenance,
      arabesk_water_rarely_used_sources: arabeskWaterRarelyUsedSources,
      arabesk_water_silicone_grout_ok: arabeskWaterSiliconeGroutOk,
      arabesk_smoke_alarms: arabeskSmokeAlarms,
      arabesk_circuit_breakers: arabeskCircuitBreakers,
      arabesk_property_owner: arabeskPropertyOwner,
      arabesk_moving_out_soon: arabeskMovingOutSoon,
    },
  });
