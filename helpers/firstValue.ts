/**
 * Get the first item of the given argument if it is an array,
 * or the argument itself otherwise.
 *
 * This is useful for working with the Next.js `ParsedUrlQuery` type
 * where each value can be either an array or a string.
 */
export const firstValue = <T>(param: T | T[]): T => (Array.isArray(param) ? param[0] : param);
