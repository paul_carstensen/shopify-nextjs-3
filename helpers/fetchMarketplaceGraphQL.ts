import { fetchGraphQL, GraphQLRequestOptions } from '@u2dv/marketplace_common/dist/fetchGraphQL';
import { fetchGraphQLAuthenticated } from './fetchGraphQLAuthenticated';

export const fetchMarketplaceGraphQL = async (config: any, query: string, options: GraphQLRequestOptions = {}) => {
  const {
    url: apiUrl,
    cognitoClientId: clientId,
    cognitoUserPoolId: userPoolId,
    cognitoUsername: username,
    cognitoPassword: password,
  } = config.graphql;

  if (username && password) {
    return fetchGraphQLAuthenticated(apiUrl, query, options, {
      region: 'eu-central-1',
      clientId,
      userPoolId,
      username,
      password,
    });
  }

  return fetchGraphQL(apiUrl, query, options);
};
